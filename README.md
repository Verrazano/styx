# README #

### What is this repository for? ###

* Styx is a simple game engine I'm working on that uses SFML/Box2D/Angelscript/enet.

### How do I get set up? ###

* Currently you need SFML, Box2D and SFGUI in the future I will add Angelscript and enet maybe something else.

library flags:
-lBox2D -sfgui-d -lsfml-audio -lsfml-graphics -lsfml-window -lsfml-system

### Contribution guidelines ###

* Make a pull request if you think something should be added. For right now it is very unlikely that I will add anything since the library is in the very early stages of development.

### Who do I talk to? ###

Harrison Miller (Verrazano) hdmscratched@gmail.com