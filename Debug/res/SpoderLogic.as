float speed = 2.3;

void onInit(Entity@ this)
{
	this.setBool("goingLeft", false);

}

void onTick(Entity@ this)
{
	Body@ body = this.getBody();
	bool goingLeft = this.getBool("goingLeft");
	Vec2 vel = body.getVelocity();
	Vec2 pos = body.getPosition();
	Engine@ engine = this.getEngine();
	SpriteLayer@ layer = this.getSprite().getLayer("sprite");
	if(goingLeft)
	{
		layer.flipHorizontal(false);
		vel.x = -speed;
		body.setVelocity(vel);
		pos.x -= speed/2;
		pos.y -= 0.8;
		if(!engine.isOtherEntityAt(pos, this))
		{
			goingLeft = !goingLeft;
			this.setBool("goingLeft", goingLeft);

		}

	}
	else
	{
		layer.flipHorizontal(true);
		vel.x = speed;
		body.setVelocity(vel);
		pos.x += speed/2;
		pos.y -= 0.8;
		if(!engine.isOtherEntityAt(pos, this))
		{
			goingLeft = !goingLeft;
			this.setBool("goingLeft", goingLeft);

		}


	}

}
