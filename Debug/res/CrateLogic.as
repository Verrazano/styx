bool loaded = false;

void onInit(Entity@ this)
{
	if(!loaded)
	{
		loaded = true;
		this.getEngine().addParticleType("dust", "smoke.png", Vec2(62, 58));

	}

}

void onDeath(Entity@ this)
{
	Engine@ engine = this.getEngine();
	Vec2 pos = this.getBody().getPosition();

	int d = rand()%12 + 6;
	for(int i = 0; i < d; i++)
	{
		float r = rand()%700;
		r /= 100;

		this.emit("dust", Vec2(1.3*sin(r), 1.3*cos(r)), (rand()%2)+0.5, 0.75);

	}

	int choice = rand()%6;
	if(choice == 0)
	{
		Entity@ spoder = engine.spawn("spoder", pos);
		if(spoder !is null)
		{
			Vec2 loc((rand()%200)-100, (rand()%200)+100);
			spoder.getBody().applyForce(loc);

		}
		return;

	}


	int mushrooms = (rand()%3) + 1;
	for(int i = 0; i < mushrooms; i++)
	{
		Entity@ mushroom = engine.spawn("mushroom", pos);
		if(mushroom !is null)
		{
			mushroom.getBody().applyForce(Vec2((rand()%200)-100, (rand()%200)+100));

		}

	}

}

void onCollide(Entity@ this, Entity@ other)
{
	float speed = this.getBody().getSpeed() + other.getBody().getSpeed();
	if(speed > 5.6)
	{
		float ratio = speed/5.6;
		if(ratio > 2.5)
			ratio = 2.5;
		this.getEngine().play("knock.wav", 2.2 - (0.75*ratio));

	}

}
