#include "BodyDef.h"

namespace styx
{

//Sets the default values for everything
BodyDef::BodyDef(World& world) :
		isDynamic(true),
		linearDamping(0.0f),
		angularDamping(0.0f),
		isBullet(false),
		fixedRotation(true),
		sensor(false),
		bouncyness(0.1f),
		friction(0.6f),
		gravityScale(1.0f),
		maxSpeed(150.0f),
		m_world(world),
		m_mass(0.0f),
		m_useMass(false)
{
}

void BodyDef::addCirc(float radius, float offsetX, float offsetY)
{
	b2CircleShape circ;
	circ.m_radius = radius;
	circ.m_p.Set(offsetX, offsetY);
	m_circles.push_back(circ);

}

void BodyDef::addRect(float width, float height, float offsetX, float offsetY, float angle)
{
	b2PolygonShape rect;

	//Box2D takes the width and height of a rectangle halved.
	rect.SetAsBox(width/2.0f, height/2.0f, b2Vec2(offsetX, offsetY), angle);
	m_polygons.push_back(rect);

}

void BodyDef::addPoly(std::vector<Vec2>& points, float offsetX, float offsetY)
{
	b2PolygonShape poly;

	//Convert the vector of Vec2 to an array of b2Vec2
	b2Vec2 p[points.size()];
	for(unsigned int i = 0; i < points.size(); i++)
	{
		p[i] = points[i].to<b2Vec2>();

	}

	poly.Set(p, points.size());
	m_polygons.push_back(poly);

}

float BodyDef::getMass()
{
	return m_mass;

}

void BodyDef::setMass(float kg)
{
	//If this is called we want to let the Body know to use our mass.
	m_useMass = true;
	m_mass = kg;

}

}
