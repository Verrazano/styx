#include "Body.h"
#include <math.h>
#include "../Util/Math/Conversion.h"

namespace styx
{

Body::Body(World& world) :
		m_world(world),
		m_body(NULL),
		m_sensor(false),
		m_bouncyness(0.1f),
		m_friction(0.6f),
		m_mass(0.0f),
		m_maxSpeed(150),
		m_data(NULL),
		m_onCollide([](Body&){}),
		m_onEndCollide([](Body&){}),
		m_shouldCollide([](Body&){return true;})
{
	//Creates the b2Body in the world.
	m_world.create(this);
	m_body->SetUserData(this);

}

Body::Body(BodyDef& def) :
		m_world(def.m_world),
		m_body(NULL),
		m_sensor(def.sensor),
		m_bouncyness(def.bouncyness),
		m_friction(def.friction),
		m_maxSpeed(def.maxSpeed),
		m_data(NULL),
		m_onCollide([](Body&){}),
		m_onEndCollide([](Body&){}),
		m_shouldCollide([](Body&){return true;})
{
	//Creates the b2Body in the world.
	m_world.create(this);
	m_body->SetUserData(this);

	//Adds all shapes from the BodyDef to this Body.
	for(auto circ : def.m_circles)
	{
		b2FixtureDef fixture;
		fixture.friction = m_friction;
		fixture.restitution = m_bouncyness;
		fixture.isSensor = m_sensor;
		fixture.density = m_sensor ? 0.0f : 1.0f;
		fixture.shape = &circ;
		fixture.userData = this;
		m_body->CreateFixture(&fixture);

	}

	for(auto poly : def.m_polygons)
	{
		b2FixtureDef fixture;
		fixture.friction = m_friction;
		fixture.restitution = m_bouncyness;
		fixture.isSensor = m_sensor;
		fixture.density = m_sensor ? 0.0f : 1.0f;
		fixture.shape = &poly;
		fixture.userData = this;
		m_body->CreateFixture(&fixture);

	}

	//Sets all variables of the BodyDef.
	setDynamic(def.isDynamic);
	setLinearDamping(def.linearDamping);
	setAngularDamping(def.angularDamping);
	setBullet(def.isBullet);
	setFixedRotation(def.fixedRotation);
	setGravityScale(def.gravityScale);

	if(def.m_useMass)
	{
		setMass(def.m_mass);

	}

}

Body::~Body()
{
	//Removes the b2Body from the world.
	m_world.destroy(this);

}

Vec2 Body::getPosition()
{
	return m_body->GetTransform().p;

}

void Body::setPosition(Vec2 pos)
{
	m_body->SetTransform(pos.to<b2Vec2>(), m_body->GetAngle());

}

void Body::setPosition(float x, float y)
{
	setPosition(Vec2(x, y));

}

void Body::move(Vec2 offset)
{
	setPosition(getPosition() + offset);

}

void Body::move(float offsetX, float offsetY)
{
	move(Vec2(offsetX, offsetY));

}

Vec2 Body::getVelocity()
{
	return m_body->GetLinearVelocity();

}

void Body::setVelocity(Vec2 vel)
{
	m_body->SetLinearVelocity(vel.to<b2Vec2>());

}

void Body::setVelocity(float x, float y)
{
	setVelocity(Vec2(x, y));

}

void Body::changeVelocity(Vec2 vel)
{
	setVelocity(getVelocity() + vel);

}

void Body::changeVelocity(float x, float y)
{
	changeVelocity(Vec2(x, y));

}

float Body::getSpeed()
{
	return getVelocity().length();

}

bool Body::isMoving(float threshold)
{
	return getSpeed() >= threshold;

}

bool Body::isAwake()
{
	return m_body->IsAwake();

}

float Body::getAngle()
{
	return -m_body->GetAngle()*180.0f/M_PI;

}

void Body::setAngle(float degrees)
{
	m_body->SetTransform(m_body->GetPosition(), -degrees/180.0f*M_PI);

}

void Body::rotate(float degrees)
{
	m_body->SetTransform(m_body->GetPosition(), m_body->GetAngle() + (-degrees/180.0f*M_PI));

}

float Body::getAngularVelocity()
{
	return toDegrees(m_body->GetAngularVelocity());

}

void Body::setAngularVelocity(float degreesASecond)
{
	m_body->SetAngularVelocity(toRadians(degreesASecond));

}

void Body::applyForce(Vec2 force)
{
	m_body->ApplyForceToCenter(force.to<b2Vec2>(), true);

}

void Body::applyForce(float forceX, float forceY)
{
	applyForce(Vec2(forceX, forceY));

}

void Body::applyForceTowards(float degrees, float force)
{
	float radians = -degrees/180.0f*M_PI;
	applyForce(Vec2(force*cosf(radians), force*sinf(radians)));

}

bool Body::isDynamic()
{
	return m_body->GetType() == b2_dynamicBody;

}

void Body::setDynamic(bool dynamic)
{
	m_body->SetType(dynamic ? b2_dynamicBody : b2_staticBody);

}

float Body::getLinearDamping()
{
	return m_body->GetLinearDamping();

}

void Body::setLinearDamping(float damping)
{
	m_body->SetLinearDamping(damping);

}

float Body::getAngularDamping()
{
	return m_body->GetAngularDamping();

}

void Body::setAngularDamping(float damping)
{
	m_body->SetAngularDamping(damping);

}

bool Body::isBullet()
{
	return m_body->IsBullet();

}

void Body::setBullet(bool bullet)
{
	m_body->SetBullet(bullet);

}

bool Body::isFixedRotation()
{
	return m_body->IsFixedRotation();

}

void Body::setFixedRotation(bool fixedRotation)
{
	m_body->SetFixedRotation(fixedRotation);

}

bool Body::isSensor()
{
	return m_sensor;

}

void Body::setSensor(bool sensor)
{
	/*Sensor data is stored in b2Fixture,
	 * so we must iterate through each one
	 * and set it's m_sensor value.
	 */
	if(m_sensor != sensor)
	{
		m_sensor = sensor;
		b2Fixture* fixture = NULL;
		for(fixture = m_body->GetFixtureList();
				fixture != NULL;
				fixture = fixture->GetNext())
		{
			fixture->SetSensor(m_sensor);

		}

	}

}

float Body::getBouncyness()
{
	return m_bouncyness;

}

void Body::setBouncyness(float bouncyness)
{
	/*restitution (bouncyness) is stored in b2Fixture,
	 * so we must iterate through each one
	 * and set it's restitution value.
	 */
	if(m_bouncyness != bouncyness)
	{
		m_bouncyness = bouncyness;
		b2Fixture* fixture = NULL;
		for(fixture = m_body->GetFixtureList();
				fixture != NULL;
				fixture = fixture->GetNext())
		{
			fixture->SetRestitution(m_bouncyness);

		}

	}

}

float Body::getFriction()
{
	return m_friction;

}

void Body::setFriction(float friction)
{
	/*friction is stored in b2Fixture,
	 * so we must iterate through each one
	 * and set it's friction value.
	 */
	if(m_friction != friction)
	{
		m_friction = friction;
		b2Fixture* fixture = NULL;
		for(fixture = m_body->GetFixtureList();
				fixture != NULL;
				fixture = fixture->GetNext())
		{
			fixture->SetFriction(m_friction);

		}

	}

}

float Body::getGravityScale()
{
	return m_body->GetGravityScale();

}

void Body::setGravityScale(float scale)
{
	m_body->SetGravityScale(scale);

}

float Body::getMass()
{
	return m_mass;

}

void Body::setMass(float kg)
{
	m_mass = kg;
	b2MassData data;
	m_body->GetMassData(&data);
	data.mass = m_mass;
	m_body->SetMassData(&data);

}

float Body::getMaxSpeed()
{
	return m_maxSpeed;

}

void Body::setMaxSpeed(float speed)
{
	m_maxSpeed = speed;

}

void Body::updateSpeed()
{
	Vec2 vel = getVelocity();
	float speed = vel.length();
	if(speed > m_maxSpeed)
	{
		setVelocity(vel*(m_maxSpeed/speed));

	}

}

void Body::resetCollisions()
{
	/*Iterates through each fixture
	 * and calls Refilter on it.
	 * This should cause shouldCollide hook to be called again.
	 */
	b2Fixture* fixture = NULL;
	for(fixture = m_body->GetFixtureList();
			fixture != NULL;
			fixture = fixture->GetNext())
	{
		fixture->Refilter();

	}

}

void Body::addCirc(float radius, float offsetX, float offsetY)
{
	b2CircleShape circ;
	circ.m_radius = radius;
	circ.m_p.Set(offsetX, offsetY);
	b2FixtureDef fixture;
	fixture.friction = m_friction;
	fixture.restitution = m_bouncyness;
	fixture.isSensor = m_sensor;
	fixture.density = m_sensor ? 0.0f : 1.0f;
	fixture.shape = &circ;
	fixture.userData = this;
	m_body->CreateFixture(&fixture);
	m_mass = m_body->GetMass();

}

void Body::addRect(float width, float height, float offsetX, float offsetY, float angle)
{
	b2PolygonShape rect;
	rect.SetAsBox(width/2.0f, height/2.0f, b2Vec2(offsetX, offsetY), angle);
	b2FixtureDef fixture;
	fixture.friction = m_friction;
	fixture.restitution = m_bouncyness;
	fixture.isSensor = m_sensor;
	fixture.density = m_sensor ? 0.0f : 1.0f;
	fixture.shape = &rect;
	fixture.userData = this;
	m_body->CreateFixture(&fixture);
	m_mass = m_body->GetMass();

}

void Body::addPoly(std::vector<Vec2>& points, float offsetX, float offsetY)
{
	b2PolygonShape poly;

	//Convert the vector of Vec2 to an array of b2Vec2
	b2Vec2 p[points.size()];
	for(unsigned int i = 0; i < points.size(); i++)
	{
		p[i] = points[i].to<b2Vec2>();

	}

	poly.Set(p, points.size());
	b2FixtureDef fixture;
	fixture.friction = m_friction;
	fixture.restitution = m_bouncyness;
	fixture.isSensor = m_sensor;
	fixture.density = m_sensor ? 0.0f : 1.0f;
	fixture.shape = &poly;
	fixture.userData = this;
	m_body->CreateFixture(&fixture);
	m_mass = m_body->GetMass();

}

void Body::setOnCollide(std::function<void(Body&)> onCollide)
{
	m_onCollide = onCollide;

}

void Body::setOnEndCollide(std::function<void(Body&)> onEndCollide)
{
	m_onEndCollide = onEndCollide;

}

void Body::onCollide(Body& other)
{
	return m_onCollide(other);

}

void Body::onEndCollide(Body& other)
{
	return m_onEndCollide(other);

}

bool Body::shouldCollide(Body& other)
{
	return m_shouldCollide(other);

}

void Body::setShouldCollide(std::function<bool(Body&)> shouldCollide)
{
	m_shouldCollide = shouldCollide;

}

//This is empty because it should never be(able to be) called
Body::Body(Body& copy) :
		m_world(copy.m_world)
{
}

}
