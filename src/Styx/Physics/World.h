#ifndef WORLD_H_
#define WORLD_H_

/*
The MIT License (MIT)

Copyright (c) 2015 Harrison D. Miller

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

author: Harrison D. Miller
psuedonym: Verrazano
contact: hdmscratched@gmail.com
*/

#include <stdint.h>
#include <functional>
#include <Box2D/Box2D.h>
#include "../Util/Math/Vec2.h"

namespace styx
{

class Body;
class Query;

/**World class.
 * Nice, neat wrapper for Box2D world.
 * Handles contact filtering, listening and world querying.
 */
class World : public b2ContactListener, public b2ContactFilter
{
public:
	/**World constructor.
	 * Initializes gravity to (0, -9.8) and everything to the
	 * default values used in the second constructor.
	 */
	World();

	/**World constructor.
	 * @param gravity The gravity of this world.
	 * @param timeStep The amount of time that passes between update steps.
	 * @param velIter The number of velocity iterations for the world to perform.
	 * @param posIter The number of position iterations for the world to perform.
	 */
	World(Vec2 gravity, float timeStep = 1.0f/60.0f, int32_t velIter = 8, int32_t posIter = 6);

	/**Returns the gravity of this world.
	 * @return The gravity of this world.
	 */
	Vec2 getGravity();

	/**Sets the new gravity for this world.
	 * @param gravity The new gravity for this world.
	 */
	void setGravity(Vec2 gravity);

	/**Returns the timeStep for this world.
	 * @return The timeStep for this world.
	 */
	float getTimeStep();

	/**Sets the new timeStep for this world.
	 * @param timeStep the new timeStep for this world.
	 */
	void setTimeStep(float timeStep);

	/**Returns the velIter for this world.
	 * @return The velIter for this world.
	 */
	int32_t getVelIter();

	/**Sets the new velIter for this world.
	 * @param velIter the new velIter for this world.
	 */
	void setVelIter(int32_t velIter);

	/**Returns the posIter for this world.
	 * @return the posIter for this world.
	 */
	int32_t getPosIter();

	/**Sets the new posIter for this world.
	 * @param posIter the new posIter for this world.
	 */
	void setPosIter(int32_t posIter);

	/**Performs an update step for the physics world.
	 * During this step if two bodies collide their callbacks are called.
	 * Make sure not to delete any bodies during this step, because it will
	 * cause a crash.
	 */
	void step();

	/**Sets the b2Draw for the internal b2World.
	 * World is not responsible for the lifetime of its b2Draw.
	 * @param debugDraw The b2Draw to set for the internal b2World.
	 */
	void setDebugDraw(b2Draw* debugDraw);

	/**Calls the appropriate functions of the b2Draw
	 * in order to draw all physics bodies to the screen.
	 */
	void drawDebug();

	/**Queries the world for bodies in side of specified area.
	 * @param pos The position of the box to check inside of.
	 * @param size The size of the box to check inside of.
	 * @param callback Every time a body is found the callback is called.
	 * Return true from the callback to continue the query or false to halt
	 * it and return from this function.
	 */
	void query(Vec2 pos, Vec2 size, std::function<bool(Body&)> callback);

private:
	/**Creates a new physics body in the world.
	 * Called whenever a new Body is created.
	 * @param body The Body to create in the world.
	 */
	void create(Body* body);

	/**Destroys a physics body of this world.
	 * Called whenever a Body is deconstructed.
	 * @param body The body to remove/destroy in the world.
	 */
	void destroy(Body* body);

	/**Hooks for b2ContactListener.
	 */
	void BeginContact(b2Contact* contact);
	void EndContact(b2Contact* contact);

	/**Hook for b2ContactFilter.
	 */
	bool ShouldCollide(b2Fixture* fixtureA, b2Fixture* fixtureB);

	b2World m_world; ///< The internal b2World stored by this class.

	float m_timeStep; ///< The timeStep to use during the step function.
	int32_t m_velIter; ///< The velIter to use during the step function.
	int32_t m_posIter; ///< The posIter to use during the step function.

	friend Body;
	friend Query;

};

}

#endif /* WORLD_H_ */
