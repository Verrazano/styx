#ifndef AABBQUERY_H_
#define AABBQUERY_H_

/*
The MIT License (MIT)

Copyright (c) 2015 Harrison D. Miller

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

author: Harrison D. Miller
psuedonym: Verrazano
contact: hdmscratched@gmail.com
*/

#include "World.h"
#include <functional>
#include "../Util/Math/Vec2.h"

namespace styx
{

/**Query class.
 * Generic implementation of the b2QueryCallback using std::function.
 * This class is private because all instances of it are created by calling
 * the query function on a World object.
 */
class Query : public b2QueryCallback
{
private:
	/**Query constructor.
	 * The construction of this object automatically starts the query on the World.
	 * @param pos The position of the box to check inside of.
	 * @param size The size of the box to check inside of.
	 * @param callback Every time a body is found the callback is called.
	 * Return true from the callback to continue the query or false to halt
	 * it and return from this function.
	 */
	Query(World& world, Vec2 pos, Vec2 size,
			std::function<bool(Body&)> callback);

	/**Hook for b2QueryCallback
	 */
	bool ReportFixture(b2Fixture* fixture);

	std::function<bool(Body&)> m_callback; ///< The callback function used inside of ReportFixture.

	friend World;

};

}

#endif /* AABBQUERY_H_ */
