#include "World.h"
#include "Body.h"
#include "Query.h"

namespace styx
{

World::World() :
		m_world(b2Vec2(0, -9.8)),
		m_timeStep(1.0f/60.0f),
		m_velIter(8),
		m_posIter(3)
{
	m_world.SetContactListener(this);
	m_world.SetContactFilter(this);

}

World::World(Vec2 gravity, float timeStep, int32_t velIter, int32_t posIter) :
		m_world(gravity.to<b2Vec2>()),
		m_timeStep(timeStep),
		m_velIter(velIter),
		m_posIter(posIter)
{
	m_world.SetContactListener(this);
	m_world.SetContactFilter(this);

}

Vec2 World::getGravity()
{
	return m_world.GetGravity();

}

void World::setGravity(Vec2 gravity)
{
	m_world.SetGravity(gravity.to<b2Vec2>());

}

float World::getTimeStep()
{
	return m_timeStep;

}

void World::setTimeStep(float timeStep)
{
	m_timeStep = timeStep;

}

int32_t World::getVelIter()
{
	return m_velIter;

}

void World::setVelIter(int32_t velIter)
{
	m_velIter = velIter;

}

int32_t World::getPosIter()
{
	return m_posIter;

}

void World::setPosIter(int32_t posIter)
{
	m_posIter = posIter;

}

void World::step()
{
	m_world.Step(m_timeStep, m_velIter, m_posIter);

}

void World::setDebugDraw(b2Draw* debugDraw)
{
	m_world.SetDebugDraw(debugDraw);

}

void World::drawDebug()
{
	m_world.DrawDebugData();

}

void World::query(Vec2 pos, Vec2 size, std::function<bool(Body&)> callback)
{
	Query((*this), pos, size, callback);

}

void World::create(Body* body)
{
	b2BodyDef def;
	body->m_body = m_world.CreateBody(&def);

}

void World::destroy(Body* body)
{
	m_world.DestroyBody(body->m_body);
	body->m_body = NULL;

}

void World::BeginContact(b2Contact* contact)
{
	b2Fixture* fixtureA = contact->GetFixtureA();
	b2Fixture* fixtureB = contact->GetFixtureB();

	Body* bodyA = static_cast<Body*>(fixtureA->GetUserData());
	Body* bodyB = static_cast<Body*>(fixtureB->GetUserData());

	bodyA->onCollide((*bodyB));
	bodyB->onCollide((*bodyA));

}

void World::EndContact(b2Contact* contact)
{
	b2Fixture* fixtureA = contact->GetFixtureA();
	b2Fixture* fixtureB = contact->GetFixtureB();

	Body* bodyA = static_cast<Body*>(fixtureA->GetUserData());
	Body* bodyB = static_cast<Body*>(fixtureB->GetUserData());

	bodyA->onEndCollide((*bodyB));
	bodyB->onEndCollide((*bodyA));

}

bool World::ShouldCollide(b2Fixture* fixtureA, b2Fixture* fixtureB)
{
	Body* bodyA = static_cast<Body*>(fixtureA->GetUserData());
	Body* bodyB = static_cast<Body*>(fixtureB->GetUserData());

	return bodyA->shouldCollide((*bodyB)) && bodyB->shouldCollide((*bodyA));

}

}
