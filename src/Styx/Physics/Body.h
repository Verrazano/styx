#ifndef BODY_H_
#define BODY_H_

#include "World.h"
#include "BodyDef.h"
#include <functional>
#include <Box2D/Box2D.h>
#include "../Util/Math/Vec2.h"

namespace styx
{

/**Body class.
 * Nice, neat wrapper for b2Body.
 * Includes functions delegated to b2Fixtures (annoying) and hooks into
 * b2ContactListener and b2ContactFilter. Allows for adding bodies during simulation.
 */
class Body
{
public:
	/**Body constructor.
	 * @param world The World to create this Body in.
	 * Creates a Body with default values and no fixtures (shapes).
	 */
	Body(World& world);

	/**Body constructor.
	 * @param def The BodyDef to get values and fixtures (shapes) from.
	 * Creates the Body in the World of the BodyDef.
	 */
	Body(BodyDef& def);

	/**Body deconstructor.
	 * Removes the body from the World.
	 * Don't do this during the step function.
	 */
	~Body();

	/**Returns the World this Body was created in.
	 * @return The world this Body was created in.
	 */
	World& getWorld();

	/**Returns the position of this Body.
	 * @return The position of this Body.
	 */
	Vec2 getPosition();

	/**Sets the position of this Body.
	 * @param pos The new position of this Body.
	 */
	void setPosition(Vec2 pos);

	/**Sets the position of this Body.
	 * @param x the new x-position of this Body.
	 * @param y the new y-position of this Body.
	 */
	void setPosition(float x, float y);

	/**Moves the Body by an amount.
	 * @param offset The amount to move the Body by.
	 */
	void move(Vec2 offset);

	/**Moves the Body by an amount.
	 * @param offsetX The amount to move the Body by on the x-axis.
	 * @param offsetY The amount to move the Body by on the y-axis.
	 */
	void move(float offsetX, float offsetY);

	/**Returns the velocity of this Body.
	 * @return The velocity of this Body.
	 */
	Vec2 getVelocity();

	/**Sets the velocity of this Body.
	 * @param vel The new velocity of this Body.
	 */
	void setVelocity(Vec2 vel);

	/**Sets the velocity of this Body.
	 * @param x The new x velocity of this Body.
	 * @param y The new y velocity of this Body.
	 */
	void setVelocity(float x, float y);

	/**Increases/decreases the velocity of this Body.
	 * @param vel The amount to change the velocity of this Body by.
	 */
	void changeVelocity(Vec2 vel);

	/**Increases/decrease the velocity of this Body.
	 * @param x The amount to change the x-component of velocity of this Body by.
	 * @param y The amount to change the y-component of velocity of this Body by.
	 */
	void changeVelocity(float x, float y);

	/**Returns the magnitude of the velocity of this Body.
	 * @return The magnitude of the velocity of this Body.
	 */
	float getSpeed();

	/**Returns true if the speed of this object is above or equal to threshold.
	 * @param threshold The value to check the speed against.
	 * @return True if the speed of this object is above or equal to threshold.
	 */
	bool isMoving(float threshold = 0.1);

	/**Returns true if the Body is awake and active in the physics simulation.
	 * @return True if the Body is awake and active in the physics simulation.
	 */
	bool isAwake();

	/**Returns the angle of this Body.
	 * @return The angle of this Body.
	 */
	float getAngle();

	/**Sets the new angle of this Body.
	 * @param degrees the new angle for this Body.
	 */
	void setAngle(float degrees);

	/**Rotates the Body by degrees.
	 * @param degrees The amount of degrees to rotate this Body by.
	 */
	void rotate(float degrees);

	/**Returns the angular velocity of this Body.
	 * @return the angular velocity of this Body.
	 */
	float getAngularVelocity();

	/**Sets the new angularVelocity of this Body.
	 * @param degreesASecond the new angularVelocity of this Body in degrees a second.
	 */
	void setAngularVelocity(float degreesASecond);

	/**Applies a force in Newtons to the Body.
	 * @param force The force to apply to the Body.
	 */
	void applyForce(Vec2 force);

	/**Applies a force in Newtons to the Body.
	 * @param forceX The x-component of the force to apply to The Body.
	 * @param forceY The y-component of the force to apply to The Body.
	 */
	void applyForce(float forceX, float forceY);

	/**Applies a force at an angle to the Body.
	 * @param degrees The angle to apply the force at.
	 * @param force The force in Newtons to apply to the Body.
	 */
	void applyForceTowards(float degrees, float force);

	/**Returns true if this Body is effected by forces.
	 * @return true if this Body is effected by forces.
	 */
	bool isDynamic();

	/**Set whether this Body is dynamic or not.
	 * @param dynamic whether this Body is dynamic or not.
	 */
	void setDynamic(bool dynamic = true);

	/**Returns the linear damping of this Body.
	 * @return The linear damping of this Body.
	 */
	float getLinearDamping();

	/**Sets the new linear damping for this Body.
	 * @param damping The new linear damping for this Body.
	 */
	void setLinearDamping(float damping);

	/**Returns the angular damping of this Body.
	 * @return The angular damping of this Body.
	 */
	float getAngularDamping();

	/**Sets the new angular damping for this Body.
	 * @param damping The new angular damping for this Body.
	 */
	void setAngularDamping(float damping);

	/**Returns true if this Body uses continuous collision checking.
	 * @return True if this Body uses continuous collision checking.
	 */
	bool isBullet();

	/**Sets whether or not this Body uses continuous collision checking.
	 * @param bullet Whether or not this Body uses continuous collision checking.
	 */
	void setBullet(bool bullet = false);

	/**Returns true if this Body has fixed rotation.
	 * @return True if this Body has fixed rotation.
	 */
	bool isFixedRotation();

	/**Sets whether or not this Body has fixed rotation.
	 * @param fixedRotation Whether or not this Body has fixed rotation.
	 */
	void setFixedRotation(bool fixedRotation = true);

	/**Returns true if this Body is a sensor.
	 * @return True if this Body is a sensor.
	 */
	bool isSensor();

	/**Sets whether or not this Body is a sensor.
	 * @param sensor Whether or not this Body is a sensor.
	 */
	void setSensor(bool sensor = false);

	/**Returns the bouncyness of this Body.
	 * @return The bouncyness of this Body.
	 */
	float getBouncyness();

	/**Sets the new bouncyness for this Body.
	 * @param bouncyness The new bouncyness for this Body.
	 */
	void setBouncyness(float bouncyness);

	/**Returns the friction of this Body.
	 * @return The friction of this Body.
	 */
	float getFriction();

	/**Sets the new friction for this Body.
	 * @param friction The new friction for this Body.
	 */
	void setFriction(float friction);

	/**Returns the gravity scale of this Body.
	 * @return The gravity scale of this Body.
	 */
	float getGravityScale();
	void setGravityScale(float scale);

	/**Returns the mass in kg of this Body.
	 * @return The mass in kg of this Body.
	 */
	float getMass();
	void setMass(float kg);

	/**Returns the maximum speed of this Body.
	 * @return the maximum speed of this Body.
	 */
	float getMaxSpeed();

	/**Sets the new maximum speed for this Body.
	 * @param speed The new maximum speed for this Body.
	 */
	void setMaxSpeed(float speed);

	/**Limits the speed of this Body.
	 * Checks the if the speed of this Body is greater than the max.
	 * If it is, then the functions sets it to the max using the unit vector.
	 */
	void updateSpeed();

	/**Resets all collisions with other Bodies.
	 * If shouldCollide had already been triggered it will be recalled now.
	 */
	void resetCollisions();

	/**Adds a circle shape to the Body.
	 * @param radius The radius of the circle.
	 * @param offsetX The offset on the x-axis from the center of the Body.
	 * @param offsetY The offset on the y-axis from the center of the Body.
	 */
	void addCirc(float radius, float offsetX = 0.0f, float offsetY = 0.0f);

	/**Adds a rectangle shape to the Body.
	 * @param width The full width of the rectangle.
	 * @param height The full height of the rectangle.
	 * @param offsetX The offset on the x-axis from the center of the Body.
	 * @param offsetY The offset on the y-axis from the center of the Body.
	 * @param angle The angle that the rectangle is rotated by.
	 */
	void addRect(float width, float height, float offsetX = 0.0f, float offsetY = 0.0f, float angle = 0.0f);

	/**Adds a polygon to the Body.
	 * Unless you configure Box2D differently the polygon must have a maximum of 8 points.
	 * The points must be specified in CCW order.
	 * @param points The points of the polygon specified in CCW order.
	 * @param offsetX The offset on the x-axis from the center of the Body.
	 * @param offsetY The offset on the y-axis from the center of the Body.
	 */
	void addPoly(std::vector<Vec2>& points, float offsetX = 0.0f, float offsetY = 0.0f);

	/**Returns the user data.
	 * @return The user data.
	 */
	template <typename T>
	T* getData();

	/**Sets the user data.
	 * Body is not responsible for the lifetime of data.
	 * @param data the user data to store.
	 */
	template <typename T>
	void setData(T* data);

	/**Sets the callback for the onCollide hook.
	 * @param onCollide the callback function to be used in onCollide.
	 * The Body reference passed in is the other Body this one is colliding with.
	 */
	void setOnCollide(std::function<void(Body&)> onCollide);

	/**Sets the callback for the onEndCollide hook.
	 * @param onEndCollide the callback function to be used in onEndCollide.
	 * The Body reference passed in is the other Body this one finished colliding with.
	 */
	void setOnEndCollide(std::function<void(Body&)> onEndCollide);

	/**Sets the callback for the shouldCollide hook.
	 * @param shouldCollide the callback function to be used in shouldCollide.
	 * The return of the callback determines if this Body wants to collide with the
	 * other Body. Both bodies must return true from their callbacks to collide.
	 * By default if this is unset it will return true.
	 */
	void setShouldCollide(std::function<bool(Body&)> shouldCollide);

private:
	/**The onCollide/onEndCollide hooks called by the World.
	 */
	void onCollide(Body& other);
	void onEndCollide(Body& other);

	/**The shoudlCollide hook called by the World.
	 */
	bool shouldCollide(Body& other);

	/**Body copy constructor.
	 * private so that Bodies can not be copied.
	 */
	Body(Body& copy);

	World& m_world; ///< The world this Body is a part of.
	b2Body* m_body; ///< The internal b2Body.

	bool m_sensor; ///< Whether or not this Body is a sensor.
	float m_bouncyness; ///< The bouncyness of this Body.
	float m_friction; ///< The friction of this Body.
	float m_mass; ///< The mass in kg of this Body.
	float m_maxSpeed; ///< The maximum speed of this Body.

	void* m_data; ///< The user data for this Body. (We don't store it in b2Body because we store (this) there instead).

	std::function<void(Body&)> m_onCollide; ///< The onCollide callback.
	std::function<void(Body&)> m_onEndCollide; ///< The onEndCollide callback.
	std::function<bool(Body&)> m_shouldCollide; ///< The shouldCollide callback.

	friend World;

};

#include "Body.inl" //Implementation for getData/setData.

}

#endif /* BODY_H_ */
