#ifndef BODYDEF_H_
#define BODYDEF_H_

/*
The MIT License (MIT)

Copyright (c) 2015 Harrison D. Miller

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

author: Harrison D. Miller
psuedonym: Verrazano
contact: hdmscratched@gmail.com
*/

#include "World.h"
#include <Box2D/Box2D.h>
#include "../Util/Math/Vec2.h"

namespace styx
{

/**BodyDef class.
 * Used to create multiple instances of the same type of Body.
 */
class BodyDef
{
public:
	/**BodyDef constructor.
	 * @param world The World to be passed to the Body when it's created.
	 */
	BodyDef(World& world);

	/**Adds a circle shape to the Body.
	 * @param radius The radius of the circle.
	 * @param offsetX The offset on the x-axis from the center of the Body.
	 * @param offsetY The offset on the y-axis from the center of the Body.
	 */
	void addCirc(float radius, float offsetX = 0.0f, float offsetY = 0.0f);

	/**Adds a rectangle shape to the Body.
	 * @param width The full width of the rectangle.
	 * @param height The full height of the rectangle.
	 * @param offsetX The offset on the x-axis from the center of the Body.
	 * @param offsetY The offset on the y-axis from the center of the Body.
	 * @param angle The angle that the rectangle is rotated by.
	 */
	void addRect(float width, float height, float offsetX = 0.0f, float offsetY = 0.0f, float angle = 0.0f);

	/**Adds a polygon to the Body.
	 * Unless you configure Box2D differently the polygon must have a maximum of 8 points.
	 * The points must be specified in CCW order.
	 * @param points The points of the polygon specified in CCW order.
	 * @param offsetX The offset on the x-axis from the center of the Body.
	 * @param offsetY The offset on the y-axis from the center of the Body.
	 */
	void addPoly(std::vector<Vec2>& points, float offsetX = 0.0f, float offsetY = 0.0f);

	bool isDynamic; ///< Is this body effected by forces? Default value of true.
	float linearDamping; ///< How quickly does the linear motion decay. Default value of 0.0f.
	float angularDamping; ///< How quickly does the angular motion decay. Default value of 0.0f.
	bool isBullet; ///< Does this object need continuous collision detection? Default value of false.
	bool fixedRotation; ///< Can this object rotate freely? Default value of true.
	bool sensor; ///< Is this object a sensor? Default value of false.
	float bouncyness; ///< How much does this Body bounce. Default value of 0.1f.
	float friction; ///< The friction of this Body. Default value of 0.6f.
	float gravityScale; ///< The gravity scale this Body experiences. Default value of 1.0f.
	float maxSpeed; ///< The maximum speed this Body can achieve. Default value of 150.0f.

	/**Returns the mass in kg of this Body.
	 * @return the mass in kg of this Body.
	 */
	float getMass();

	/**Sets the new mass of this Body in kg.
	 * @param kg The new mass of this Body.
	 */
	void setMass(float kg);

private:
	World& m_world; ///< The World to be used when constructing a Body using this BodyDef.

	std::vector<b2CircleShape> m_circles; ///< The stored circle shapes.
	std::vector<b2PolygonShape> m_polygons; ///< The stored rectangle/polygon shapes.

	float m_mass; ///< The mass of the Body.
	bool m_useMass; ///< Determines if we use m_mass to set the mass of the Body.

	friend Body;

};

}

#endif /* BODYDEF_H_ */
