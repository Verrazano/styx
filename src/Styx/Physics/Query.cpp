#include "Query.h"
#include "../Game/Entity/Entity.h"
#include <math.h>

namespace styx
{

Query::Query(World& world, Vec2 pos, Vec2 size,
		std::function<bool(Body&)> callback) :
		m_callback(callback)
{
	b2AABB aabb;
	Vec2 pos2 = pos + size;

	/*Using fmin/fmax to determine the upper and lower bounds
	 * insures that even if the user passes a negative size
	 * (meaning the position is the lower right/left corner)
	 * the bounds will still be correct.
	 */
	aabb.lowerBound = b2Vec2(fmin(pos.x, pos2.x), fmin(pos.y, pos2.y));
	aabb.upperBound = b2Vec2(fmax(pos.x, pos2.x), fmax(pos.y, pos2.y));

	//Starts the query on the internal b2World of world.
	world.m_world.QueryAABB(this, aabb);

}

bool Query::ReportFixture(b2Fixture* fixture)
{
	Body& body = *static_cast<Body*>(fixture->GetBody()->GetUserData());
	return m_callback(body);

}

}
