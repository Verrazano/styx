template <typename T>
T* Body::getData()
{
	return static_cast<T*>(m_data);
	
}

template <typename T>
void Body::setData(T* data)
{
	m_data = data;
	
}