#include "MemberBase.h"

MemberBase::MemberBase(std::string name, size_t offset)
{
	m_name = name;
	m_offset = offset;

}

MemberBase::~MemberBase()
{
}

std::string MemberBase::write(char* base)
{
	std::string line = getType() + " " + getName() + " = " + getValue(base);
	return line;

}

std::string MemberBase::getName()
{
	return m_name;

}

size_t MemberBase::getOffset()
{
	return m_offset;

}
