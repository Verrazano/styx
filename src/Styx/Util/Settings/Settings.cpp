#include "Settings.h"
#include "Member.h"
#include "../Config/Config.h"
#include "VectorMember.h"

Settings::Settings()
{
}

Settings::~Settings()
{
	while(!m_members.empty())
	{
		delete (*m_members.begin());
		m_members.erase(m_members.begin());

	}

}

bool Settings::read(std::string file)
{
	if(file == "")
	{
		return false;

	}

	Config settings;
	settings.parse(file);
	read(settings);

}

bool Settings::read(Config& settings)
{
	std::list<MemberBase*>::iterator it;
	for(it = m_members.begin(); it != m_members.end(); it++)
	{
		MemberBase* member = (*it);
		std::string type = settings.getItem(member->getName()).getType();

		if(type == member->getType())
		{
			if(type == "int")
			{
				int32_t t = settings.getItem(member->getName()).valueAsInt();
				member->set((char*)this, (void*)&t);

			}
			else if(type == "uint")
			{
				uint32_t t = settings.getItem(member->getName()).valueAsUInt();
				member->set((char*)this, (void*)&t);

			}
			else if(type == "float")
			{
				float t = settings.getItem(member->getName()).valueAsFloat();
				member->set((char*)this, (void*)&t);

			}
			else if(type == "bool")
			{
				bool t = settings.getItem(member->getName()).valueAsBool();
				member->set((char*)this, (void*)&t);

			}
			else if(type == "string")
			{
				std::string t = settings.getItem(member->getName()).getValue();
				member->set((char*)this, (void*)&t);

			}
			else if(type == "string@")
			{
				std::vector<std::string> t;
				ConfigItem item = settings.getItem(member->getName());
				for(unsigned int i = 0; i < item.getNumOfParams(); i++)
				{
					t.push_back(item.getParam(i));
				}
				member->set((char*)this, (void*)&t);

			}

		}

	}

	return true;

}

bool Settings::write(std::string path)
{
	if(path == "")
		return false;

	Config settings;

	std::list<MemberBase*>::iterator it;
	for(it = m_members.begin(); it != m_members.end(); it++)
	{
		MemberBase* member = (*it);
		settings.addItem(member->write((char*)this));

	}

	return settings.write(path);

}

void Settings::add(std::string name, size_t offset, int32_t value)
{
	m_members.push_back(new Member<int32_t>(name, offset, value, "int"));

}

void Settings::add(std::string name, size_t offset, uint32_t value)
{
	m_members.push_back(new Member<uint32_t>(name, offset, value, "uint"));

}

void Settings::add(std::string name, size_t offset, float value)
{
	m_members.push_back(new Member<float>(name, offset, value, "float"));

}

void Settings::add(std::string name, size_t offset, bool value)
{
	m_members.push_back(new BoolMember(name, offset, value, "bool"));

}

void Settings::add(std::string name, size_t offset, std::string value)
{
	m_members.push_back(new Member<std::string>(name, offset, value, "string"));

}

void Settings::add(std::string name, size_t offset, std::vector<std::string> value)
{
	m_members.push_back(new VectorMember(name, offset, value));

}
