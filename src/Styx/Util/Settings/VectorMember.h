#ifndef VECTORMEMBER_H_
#define VECTORMEMBER_H_

#include "MemberBase.h"
#include <vector>

class VectorMember : public MemberBase
{
public:
	VectorMember(std::string name, size_t offset, std::vector<std::string> value);
	virtual ~VectorMember();

	virtual std::string getType();
	virtual std::string getValue(char* base);

	void set(char* base, void* value);

private:
	std::vector<std::string> m_value;

};

#endif /* VECTORMEMBER_H_ */
