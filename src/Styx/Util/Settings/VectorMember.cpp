#include "VectorMember.h"

VectorMember::VectorMember(std::string name, size_t offset, std::vector<std::string> value) :
	MemberBase(name, offset)
{
	m_value = value;

}

VectorMember::~VectorMember()
{

}

std::string VectorMember::getType()
{
	return "string@";

}

std::string VectorMember::getValue(char* base)
{
    std::vector<std::string>* member = NULL;
    member = (std::vector<std::string>*)(base + getOffset());
    m_value = (*member);
    std::string value;
    for(int i = 0; i < m_value.size(); i++)
    {
    	if(i != m_value.size()-1)
    	{
    		value += m_value[i]+", ";

    	}
    	else
    	{
    		value += m_value[i];

    	}

    }
    return value;

}

void VectorMember::set(char* base, void* value)
{
    std::vector<std::string>* member = NULL;
    member = (std::vector<std::string>*)(base + getOffset());
    (*member) = *(std::vector<std::string>*)value;

}
