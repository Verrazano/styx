template <typename T>
Member<T>::Member(std::string name, size_t offset, T value, std::string type) :
    MemberBase(name, offset)
{
    m_value = value;
    m_type = type;

}

template <typename T>
Member<T>::~Member()
{
}

template <typename T>
std::string Member<T>::getType()
{
    return m_type;

}

template <typename T>
std::string Member<T>::getValue(char* base)
{
    T* member = NULL;
    member = (T*)(base + getOffset());
    m_value = (*member);
    std::stringstream ss;
    ss << m_value;
    std::string s;
    ss >> s;
    return s;

}

template <typename T>
void Member<T>::set(char* base, void* value)
{
    T* member = NULL;
    member = (T*)(base + getOffset());
    (*member) = *(T*)value;

}