#ifndef VARMANAGER_H_
#define VARMANAGER_H_

#include <map>
#include <list>
#include <string>
#include <stdint.h>

class VarManager
{
public:
	VarManager();
	~VarManager();

	void addTag(std::string tag);
	bool hasTag(std::string tag);
	void remTag(std::string tag);

	void setUInt(std::string name, uint32_t val);
	uint32_t getUInt(std::string name);

	void setInt(std::string name, int32_t val);
	int32_t getInt(std::string name);

	void setFloat(std::string name, float val);
	float getFloat(std::string name);

	void setString(std::string name, std::string val);
	std::string getString(std::string name);

	std::map<std::string, uint32_t>& getUInts();
	std::map<std::string, int32_t>& getInts();
	std::map<std::string, float>& getFloats();
	std::map<std::string, std::string>& getStrings();

private:
	std::list<std::string> m_tags;
	std::map<std::string, uint32_t> m_uints;
	std::map<std::string, int32_t> m_ints;
	std::map<std::string, float> m_floats;
	std::map<std::string, std::string> m_strings;

};

#endif /* VARMANAGER_H_ */
