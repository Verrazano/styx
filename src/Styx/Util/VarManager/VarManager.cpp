#include "VarManager.h"

VarManager::VarManager()
{
}

VarManager::~VarManager()
{
}

void VarManager::addTag(std::string tag)
{
	m_tags.remove(tag);
	m_tags.push_back(tag);

}

bool VarManager::hasTag(std::string tag)
{
	std::list<std::string>::iterator it;
	for(it = m_tags.begin(); it != m_tags.end(); ++it)
	{
		if((*it) == tag)
		{
			return true;

		}

	}

	return false;

}

void VarManager::remTag(std::string tag)
{
	m_tags.remove(tag);

}

void VarManager::setUInt(std::string name, uint32_t val)
{
	m_uints[name] = val;

}

uint32_t VarManager::getUInt(std::string name)
{
	return m_uints[name];

}

void VarManager::setInt(std::string name, int32_t val)
{
	m_ints[name] = val;

}

int32_t VarManager::getInt(std::string name)
{
	return m_ints[name];

}

void VarManager::setFloat(std::string name, float val)
{
	m_floats[name] = val;

}

float VarManager::getFloat(std::string name)
{
	return m_floats[name];

}

void VarManager::setString(std::string name, std::string val)
{
	m_strings[name] = val;

}

std::string VarManager::getString(std::string name)
{
	return m_strings[name];

}

std::map<std::string, uint32_t>& VarManager::getUInts(){return m_uints;}
std::map<std::string, int32_t>& VarManager::getInts(){return m_ints;}
std::map<std::string, float>& VarManager::getFloats(){return m_floats;}
std::map<std::string, std::string>& VarManager::getStrings(){return m_strings;}
