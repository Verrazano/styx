#include "Conversion.h"
#include <math.h>

namespace styx
{

float pixelsInAMeter = 32.0f;

float toMeters(float pixels)
{
	return pixels/pixelsInAMeter;

}

float toPixels(float meters)
{
	return meters*pixelsInAMeter;

}

Vec2 toWorld(Vec2 vec)
{
	return Vec2(toMeters(vec.x), -toMeters(vec.y));

}

sf::Vector2f toScreen(Vec2 vec)
{
	return sf::Vector2f(toPixels(vec.x), -toPixels(vec.y));

}

float toRadians(float degrees)
{
	return degrees*M_PI/180.0f;

}

float toDegrees(float radians)
{
	return radians/M_PI*180.0f;

}

}
