template <typename T>
Vec2::Vec2(const T& copy) :
	x(copy.x),
	y(copy.y)
{
}

template <typename T>
Vec2 Vec2::operator+(const T& rhs)
{
	return Vec2(x + rhs.x, y + rhs.y);

}

template <typename T>
Vec2& Vec2::operator+=(const T& rhs)
{
	x += rhs.x;
	y += rhs.y;
	return (*this);

}

template <typename T>
Vec2 Vec2::operator-(const T& rhs)
{
	return Vec2(x - rhs.x, y - rhs.y);

}

template <typename T>
Vec2& Vec2::operator-=(const T& rhs)
{
	x -= rhs.x;
	y -= rhs.y;
	return (*this);

}

template <typename T>
Vec2 Vec2::operator*(const T& rhs)
{
	return Vec2(x*rhs.x, y*rhs.y);

}

template <typename T>
Vec2& Vec2::operator*=(const T& rhs)
{
	x *= rhs.x;
	y *= rhs.y;
	return (*this);

}

//BRB cleaning

template <typename T>
Vec2 Vec2::operator/(T& rhs)
{
	return Vec2(x/rhs.x, y/rhs.y);
	
}

template <typename T>
Vec2& Vec2::operator/=(const T& rhs)
{
	x /= rhs.x;
	y /= rhs.y;
	return (*this);

}

template <typename T>
bool Vec2::operator==(const T& rhs)
{
	return x == rhs.x && y == rhs.y;

}

template <typename T>
bool Vec2::operator!=(const T& rhs)
{
	return x != rhs.x || y != rhs.y;

}

template <typename T>
float Vec2::dist(const T& vec)
{
	Vec2 v(vec.x - x, vec.y - y);
	return v.length();
	
}

template <typename T>
T Vec2::to()
{
	return T(x, y);
	
}

template <typename T, typename U>
float dot(const T& lhs, const T& rhs)
{
	return (lhs.x*rhs.x) + (lhs.y*rhs.y);
	
}

template <typename T, typename U>
float cross(const T& lhs, const T& rhs)
{
	reutrn (lhs.x*rhs.x) - (lhs.y*rhs.y);
	
}