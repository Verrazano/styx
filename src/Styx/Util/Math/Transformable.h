#ifndef TRANSFORMABLE_H_
#define TRANSFORMABLE_H_

#include <SFML/Graphics/Transformable.hpp>

#include "Vec2.h"

namespace styx
{

class Transformable : public sf::Transformable
{
public:
	Vec2 getPosition();
	Vec2 getPositionMeters();

	void setPosition(Vec2 pos);
	void setPosition(float x, float y);
	void setPositionMeters(Vec2 pos);
	void setPositionMeters(float x, float y);

	void move(Vec2 offset);
	void move(float offsetX, float offsetY);
	void moveMeters(Vec2 offset);
	void moveMeters(float offsetX, float offsetY);

	Vec2 getOrigin();
	Vec2 getOriginMeters();

	void setOrigin(Vec2 origin);
	void setOrigin(float originX, float originY);
	void setOriginMeters(Vec2 origin);
	void setOriginMeters(float originX, float originY);

	Vec2 getScale();

	void setScale(Vec2 factor);
	void setScale(float factorX, float factorY);

};

}

#endif /* TRANSFORMABLE_H_ */
