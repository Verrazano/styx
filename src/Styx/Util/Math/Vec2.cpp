#include "Vec2.h"
#include <math.h>
#include <sstream>
#include "../Strings/Strings.h"

namespace styx
{

Vec2::Vec2() :
	x(0),
	y(0)
{
}

Vec2::Vec2(float x, float y) :
	x(x),
	y(y)
{
}

Vec2 Vec2::operator-()
{
	return Vec2(-x, -y);

}

Vec2 Vec2::operator*(float rhs)
{
	return Vec2(x*rhs, y*rhs);

}

Vec2 Vec2::operator/(float rhs)
{
	return Vec2(x/rhs, y/rhs);

}

float Vec2::length()
{
	return sqrtf((x*x) + (y*y));

}

Vec2& Vec2::normalized()
{
	float l = length();
	if(l == 0.0f)
	{
		return (*this);

	}

	x /= l;
	y /= l;

	return (*this);

}

Vec2& Vec2::truncate()
{
	x = (int)x;
	y = (int)y;
	return (*this);

}

std::string Vec2::toString()
{

	return "(" + styx::toString(x) + ", " + styx::toString(y) + ")";

}

}
