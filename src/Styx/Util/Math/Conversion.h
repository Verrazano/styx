#ifndef CONVERSION_H_
#define CONVERSION_H_

#include <SFML/System/Vector2.hpp>
#include "Vec2.h"

namespace styx
{

extern float pixelsInAMeter;

float toMeters(float pixels);
float toPixels(float meters);

Vec2 toWorld(Vec2 vec);
sf::Vector2f toScreen(Vec2 vec);

float toRadians(float degrees);
float toDegrees(float radians);

}

#endif /* CONVERSION_H_ */
