#include "Transformable.h"
#include "Conversion.h"

namespace styx
{

Vec2 Transformable::getPosition()
{
	return sf::Transformable::getPosition();

}

Vec2 Transformable::getPositionMeters()
{
	return toWorld(sf::Transformable::getPosition());

}

void Transformable::setPosition(Vec2 pos)
{
	sf::Transformable::setPosition(pos.to<sf::Vector2f>());

}

void Transformable::setPosition(float x, float y)
{
	setPosition(Vec2(x, y));

}

void Transformable::setPositionMeters(Vec2 pos)
{
	sf::Transformable::setPosition(toScreen(pos));

}

void Transformable::setPositionMeters(float x, float y)
{
	setPositionMeters(Vec2(x, y));

}

void Transformable::move(Vec2 offset)
{
	sf::Transformable::move(offset.to<sf::Vector2f>());

}

void Transformable::move(float offsetX, float offsetY)
{
	move(Vec2(offsetX, offsetY));

}

void Transformable::moveMeters(Vec2 offset)
{
	sf::Transformable::move(toScreen(offset));

}

void Transformable::moveMeters(float offsetX, float offsetY)
{
	moveMeters(offsetX, offsetY);

}

Vec2 Transformable::getOrigin()
{
	return sf::Transformable::getOrigin();

}

Vec2 Transformable::getOriginMeters()
{
	return toWorld(sf::Transformable::getOrigin());

}

void Transformable::setOrigin(Vec2 origin)
{
	sf::Transformable::setOrigin(origin.to<sf::Vector2f>());

}

void Transformable::setOrigin(float originX, float originY)
{
	setOrigin(Vec2(originX, originY));

}

void Transformable::setOriginMeters(Vec2 origin)
{
	sf::Transformable::setOrigin(toScreen(origin));

}

void Transformable::setOriginMeters(float originX, float originY)
{
	setOriginMeters(Vec2(originX, originY));

}

Vec2 Transformable::getScale()
{
	return sf::Transformable::getScale();

}

void Transformable::setScale(Vec2 factor)
{
	sf::Transformable::setScale(factor.to<sf::Vector2f>());

}

void Transformable::setScale(float factorX, float factorY)
{
	setScale(Vec2(factorX, factorY));

}


}
