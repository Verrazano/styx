#ifndef VEC2_H_
#define VEC2_H_

#include <math.h>
#include <string>

namespace styx
{

class Vec2
{
public:
	Vec2();

	template <typename T>
	Vec2(const T& copy);

	Vec2(float x, float y);

	Vec2 operator-();

	template <typename T>
	Vec2 operator+(const T& rhs);

	template <typename T>
	Vec2& operator+=(const T& rhs);

	template <typename T>
	Vec2 operator-(const T& rhs);

	template <typename T>
	Vec2& operator-=(const T& rhs);

	template <typename T>
	Vec2 operator*(const T& rhs);

	Vec2 operator*(float rhs);

	template <typename T>
	Vec2& operator*=(const T& rhs);

	Vec2 operator/(float rhs);

	template <typename T>
	Vec2 operator/(T& rhs); //screw const correctness right now

	template <typename T>
	Vec2& operator/=(const T& rhs);

	template <typename T>
	bool operator==(const T& rhs);

	template <typename T>
	bool operator!=(const T& rhs);

	float length();

	Vec2& normalized();

	template <typename T>
	float dist(const T& vec);

	template <typename T>
	T to();

	Vec2& truncate();

	std::string toString();

	float x;
	float y;

};

template <typename T, typename U>
float dot(const T& lhs, const T& rhs);

template <typename T, typename U>
float cross(const T& lhs, const T& rhs);

#include "Vec2.inl"

}

#endif /* VEC2_H_ */
