#include "mousetoview.h"

sf::Vector2f mouseToView(sf::View& v, sf::RenderWindow& win)
{
	sf::Vector2f m(sf::Mouse::getPosition(win));
	sf::Vector2f p;
	float vx = v.getCenter().x - v.getSize().x/2;
	float vy = v.getCenter().y - v.getSize().y/2;

	float fx = (win.getSize().x/v.getSize().x);
	float fy = (win.getSize().y/v.getSize().y);

	p.x = m.x/fx + vx;
	p.y = m.y/fy + vy;

	return p;

}

sf::Vector2f pointToView(sf::View& v, sf::RenderWindow& win, sf::Vector2f point)
{
	sf::Vector2f p;
	float vx = v.getCenter().x - v.getSize().x/2;
	float vy = v.getCenter().y - v.getSize().y/2;

	float fx = (win.getSize().x/v.getSize().x);
	float fy = (win.getSize().y/v.getSize().y);

	p.x = point.x/fx + vx;
	p.y = point.y/fy + vy;

	return p;

}
