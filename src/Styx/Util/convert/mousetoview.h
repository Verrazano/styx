#ifndef MOUSETOVIEW_H_
#define MOUSETOVIEW_H_

#include <SFML/Graphics.hpp>

sf::Vector2f mouseToView(sf::View& v, sf::RenderWindow& win);
sf::Vector2f pointToView(sf::View& v, sf::RenderWindow& win, sf::Vector2f point);

#endif /* MOUSETOVIEW_H_ */
