#include "ID.h"

ID::ID()
{
	m_id = uuidMake();
	char str[128];
	uuidToStr(m_id, str, 128);
	m_idString = std::string(str);

}

ID::ID(ID& copy)
{
	m_id = copy.m_id;
	m_idString = copy.m_idString;

}

ID::ID(std::string idString)
{
	m_idString = idString;
	uuidFromStr(&m_id, idString.c_str());

}

ID::~ID()
{
}

bool ID::operator==(ID& rhs)
{
	return m_id.n0 == rhs.m_id.n0 && m_id.n1 == rhs.m_id.n1;

}

bool ID::operator==(uuid_t id)
{
	return m_id.n0 == id.n0 && m_id.n1 == id.n1;

}

bool ID::operator==(std::string idString)
{
	uuid_t id;
	uuidFromStr(&id, idString.c_str());
	return m_id.n0 == id.n0 && m_id.n1 == id.n1;

}

bool ID::operator!=(ID& rhs)
{
	return m_id.n0 != rhs.m_id.n0 && m_id.n1 != rhs.m_id.n1;

}

bool ID::operator!=(uuid_t id)
{
	return  m_id.n0 != id.n0 && m_id.n1 != id.n1;

}

bool ID::operator!=(std::string idString)
{
	uuid_t id;
	uuidFromStr(&id, idString.c_str());
	return m_id.n0 != id.n0 && m_id.n1 != id.n1;

}

uuid_t ID::getUUID()
{
	return m_id;

}

std::string ID::getIDString()
{
	return m_idString;

}
