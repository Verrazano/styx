/* Version 4 UUID Library */

#include "uuidlib.h"
#include <string.h>
#include <time.h>
#include <cstdlib>
#include <cstdio>
#include <cctype>

/* Generates and returns a new UUID. The random number
   generator should be seeded with srand() before calling
   this function. */
uuid_t
uuidMake()
{
  uuid_t uuid;

  /* Masks for the 0x04 and (8, 9, A, B) */
  uint64_t onemask0 = 0xffffffffffff4fff;
  uint64_t onemask1 = 0xbfffffffffffffff;
  uint64_t zeromask0 = 0x0000000000004000;
  uint64_t zeromask1 = 0x8000000000000000;

  /* Generate a random UUID */
  uuid.n0 = rand();
  uuid.n0 |= (uint64_t)((uint16_t)rand()) << 16;
  uuid.n0 |= (uint64_t)((uint16_t)rand()) << 32;
  uuid.n0 |= (uint64_t)((uint16_t)rand()) << 48;

  uuid.n1 = rand();
  uuid.n1 |= (uint64_t)((uint16_t)rand()) << 16;
  uuid.n1 |= (uint64_t)((uint16_t)rand()) << 32;
  uuid.n1 |= (uint64_t)((uint16_t)rand()) << 48;

  /* Mask it */
  uuid.n0 &= onemask0;
  uuid.n1 &= onemask1;

  uuid.n0 |= zeromask0;
  uuid.n1 |= zeromask1;

  return uuid;
}

/* Internal function
   Returns a nonzero value if the specified string contains
   only valid hexidecimal characters. Otherwise, a value of
   zero is returned. */
int
uuid_isxstr(char *str)
{
  while(str != '\0') {
    if(isxdigit(*str) == 0)
      return 0;

    str++;
  }

  return 1;
}

/* Internal function
   Reads a segment of /count/ length at an offset of /offset/
   from /buf/, converts it to a uint64_t, and stores it
   into /num/. This function returns zero upon success, or
   -1 upon failure. */
int
uuidGetStrSegment(const char *str, int offset, int count, uint64_t *num)
{
  uint64_t tmp;
  char *endptr;

  tmp = strtoll(str+offset, &endptr, 16);

  if(endptr != str+offset+count)
    return -1;

  *num = tmp;

  return 0;
}

/* Decodes a string containing a UUID in the standard format
   to a uuid_t. Upon successful completion, a value of
   zero is returned. Otherwise, -1 is returned. */
int
uuidFromStr(uuid_t *uuid, const char *str)
{
  uint64_t n0;
  uint64_t n1;
  uint64_t tmp;

  /* Test uuid for NULL */
  if(uuid == NULL)
    return -1;

  /* A UUID must be exactly 36 characters long */
  if(strlen(str) != 36)
    return -1;

  /* Read the first half */
  n0 = 0;

  n0 = n0 << 32;
  if(uuidGetStrSegment(str, 0, 8, &tmp) != 0)
    return -1;
  n0 |= tmp & 0xffffffff;

  n0 = n0 << 16;
  if(uuidGetStrSegment(str, 9, 4, &tmp) != 0)
    return -1;
  n0 |= tmp & 0xffff;

  n0 = n0 << 16;
  if(uuidGetStrSegment(str, 14, 4, &tmp) != 0)
    return -1;
  n0 |= tmp & 0xffff;

  /* Read the second half */
  n1 = 0;

  n1 = n1 << 16;
  if(uuidGetStrSegment(str, 19, 4, &tmp) != 0)
    return -1;
  n1 |= tmp & 0xffff;

  n1 = n1 << 48;
  if(uuidGetStrSegment(str, 24, 12, &tmp) != 0)
    return -1;
  n1 |= tmp & 0xffffffffffff;

  /* Write out the result */
  uuid->n0 = n0;
  uuid->n1 = n1;

  return 0;
}


/* Encodes a uuid_t to a string in the standard format. At
   most size characters of the string will be written to
   str . If successful, the function will return the number
   of characters written to str. A negative value will
   be returned upon error. */
int
uuidToStr(uuid_t uuid, char *str, int size)
{
  uint32_t p1;
  uint16_t p2;
  uint16_t p3;
  uint16_t p4;
  uint64_t p5;

  /* Test str for NULL */
  if(str == NULL) return -1;

  /* Construct the segments of the UUID */
  p1 = (uuid.n0 >> 32) & 0xffffffff;
  p2 = (uuid.n0 >> 16) & 0xffff;
  p3 = (uuid.n0 >> 0) & 0xffff;
  p4 = (uuid.n1 >> 48) & 0xffff;
  p5 = (uuid.n1 >> 0) & 0xffffffffffff;

  /* Print them */
  return snprintf(str, size, "%08x-%04x-%04x-%04x-%012lx",
    p1, p2, p3, p4, p5);
}

/* Tests the equality of two uuid_t s. Returns one if
   first and second are equal, and zero in all other
   cases. */
int
uuidIsEqual(uuid_t first, uuid_t second)
{
  return first.n0 == second.n0 && first.n1 == second.n1;
}

