#include "Strings.h"

namespace styx
{

bool fromString(std::string str)
{
	return str == "true";

}

std::string toString(bool val)
{
	return val ? "true" : "false";

}

}
