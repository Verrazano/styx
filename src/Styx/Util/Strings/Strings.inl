template <typename T>
T fromString(std::string str)
{
	std::stringstream ss;
	ss << str;
	T val;
	ss >> val;
	return val;
	
}

template <typename T>
std::string toString(T val)
{
	std::stringstream ss;
	ss << val;
	std::string str;
	ss >> str;
	return str;
	
}

/*bool fromString(std::string str)
{
	return str == "true";
	
}

std::string toString(bool val)
{
	return val ? "true" : "false";

}*/