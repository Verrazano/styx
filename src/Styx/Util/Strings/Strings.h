#ifndef STRINGS_H_
#define STRINGS_H_

#include <string>
#include <sstream>

namespace styx
{

template <typename T>
T fromString(std::string str);

template <typename T>
std::string toString(T val);

bool fromString(std::string str);

std::string toString(bool val);

#include "Strings.inl"

}

#endif /* STRINGS_H_ */
