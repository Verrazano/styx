#ifndef RESOURCEMANAGER_H_
#define RESOURCEMANAGER_H_

#include <list>
#include <vector>

#include "FileSystem.h"

#include "Resource.h"
#include "ResourceFactory.h"

class ResourceManager
{
public:
	ResourceManager();

	~ResourceManager();

	void search(std::string path);

	void reset();

	bool hasPath(std::string name);

	std::string fullPath(std::string name);

	void addFactory(ResourceFactory factory);

	bool add(std::string path);

	void rem(std::string name);

	bool has(std::string name);

	template <class T>
	T* get(std::string name)
	{
		Resource* r = getResource(name);
		if(r != NULL)
		{
			void* data = r->getData();
			return reinterpret_cast<T*>(data);

		}

		return NULL;

	}

	Resource* getResource(std::string name);

	FileSystem& getSystem();

	unsigned int getNumOfPaths();
	std::string getPath(unsigned int index);

private:
	FileSystem m_files;
	std::vector<ResourceFactory> m_factories;
	std::list<Resource*> m_resources;

};

#endif /* RESOURCEMANAGER_H_ */
