#include "ResourceManager.h"

ResourceManager::ResourceManager()
{
}

ResourceManager::~ResourceManager()
{
	reset();

}

void ResourceManager::search(std::string path)
{
	m_files.search(path);
	for(unsigned int i = 0; i < m_files.getNumOfPaths(); i++)
	{
		add(m_files.getPath(i));

	}

}

void ResourceManager::reset()
{
	m_files.reset();
	while(!m_resources.empty())
	{
		delete (*m_resources.begin());
		m_resources.erase(m_resources.begin());

	}

	m_factories.clear();

}

bool ResourceManager::hasPath(std::string name)
{
	return has(name);

}

std::string ResourceManager::fullPath(std::string name)
{
	return m_files.fullPath(name);

}

void ResourceManager::addFactory(ResourceFactory factory)
{
	m_factories.push_back(factory);

}

bool ResourceManager::add(std::string path)
{
	Resource* resource;
	for(unsigned int i = 0; i < m_factories.size(); i++)
	{
		std::string ext = m_factories[i].getExtension();
		if(FileSystem::hasExtension(path, ext))
		{
			resource = m_factories[i].create(path);
			if(resource == NULL)
			{
				continue;

			}

			rem(path); //incase there is a resource with the same path
			m_resources.push_back(resource);
			return true;

		}

	}

	return false;

}

void ResourceManager::rem(std::string name)
{
	m_resources.remove(getResource(name));

}

bool ResourceManager::has(std::string name)
{
	return getResource(name) != NULL;

}
Resource* ResourceManager::getResource(std::string name)
{
	std::string path = m_files.fullPath(name);

	std::list<Resource*>::iterator it;
	for(it = m_resources.begin(); it != m_resources.end(); it++)
	{
		if((*it)->getPath() == path)
		{
			return (*it);

		}

	}

	return NULL;

}

FileSystem& ResourceManager::getSystem()
{
	return m_files;

}

unsigned int ResourceManager::getNumOfPaths()
{
	return m_files.getNumOfPaths();

}

std::string ResourceManager::getPath(unsigned int index)
{
	return m_files.getPath(index);

}
