template<class T, typename... Args>
bool Engine::addBehavior(Args&&... args)
{
	static_assert(std::is_base_of<EntityBehavior, T>::value, "T does not inherit EntityBehaivor");
	T* ptr = new T(args...);
	EntityBehavior::Ptr sptr = EntityBehavior::Ptr(ptr);
	return behaviors.insert(sptr).second;

}

template<class T, typename... Args>
bool Engine::addGameModeBehavior(Args&&... args)
{
	static_assert(std::is_base_of<GameModeBehavior, T>::value, "T does not inherit GameModeBehaivor");
	T* ptr = new T(args...);
	GameModeBehavior::Ptr sptr = GameModeBehavior::Ptr(ptr);
	return gameModeBehaviors.insert(sptr).second;

}