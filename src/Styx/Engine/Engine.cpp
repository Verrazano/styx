#include "Engine.h"
#include "../Game/Entity/EntityType/EntityType.h"
#include "../Game/Entity/Entity.h"
#include "../Game/Entity/EntityBehavior.h"
#include "../Util/Util.h"
#include "../Util/convert/mousetoview.h"
#include "../Graphics/UI/EntityTracker.h"
#include "../Game/GameMode/GameMode.h"
#include <algorithm>
#include <iostream>
#include <time.h>
#include "../Scripting/Scripting.h"
#include <type_traits>

namespace styx
{

Engine::Engine(EngineInfo info) :
		files(&soundManager),
		console(desktop),
		m_info(info)
{
	defer = false;
	orderingRequested = true;
	std::string build = "Build 30";
	frameCount = 0;

	sf::ContextSettings settings;
	settings.antialiasingLevel = m_info.antialiasingLevel;
	if(m_info.fullScreen)
	{
		sf::VideoMode fullscreenMode = sf::VideoMode::getFullscreenModes()[0];
		m_window.create(fullscreenMode, "Styx - " + build, sf::Style::Fullscreen, settings);

	}
	else
	{
		m_window.create(sf::VideoMode(m_info.windowWidth, m_info.windowHeight), "Styx - " + build, sf::Style::Close | sf::Style::Resize, settings);

	}

	m_camera.setSize(m_info.viewWidth, m_info.viewHeight);
	m_camera.setGUIViewSize(m_info.viewWidth, m_info.viewHeight);
	hq2xTexture.create(m_info.windowWidth, m_info.windowHeight);
	hq2xTexture.setSmooth(false);
	hq2xRect.setSize(sf::Vector2f(m_info.viewWidth, m_info.viewHeight));

	hq2xRect.setOutlineColor(sf::Color::Blue);
	hq2xRect.setOutlineThickness(5.0);

	hq2xRect.setTexture(&hq2xTexture);
	m_window.setFramerateLimit(m_info.frameRate);
	world.setGravity(Vec2(m_info.gravityX, m_info.gravityY));
	debugDraw = Box2DDebugDrawer(&m_window);
	world.setDebugDraw(&debugDraw);

	console.addCommand(styx::Console::Command("spawn",
			[this](std::vector<std::string> params)
			{
				if(params.size() != 3)
				{
					return false;

				}

				if(!hasEntityType(params[0]))
				{
					console.out("No entity type called: " + params[0] + ".");
					return true;

				}

				float x = fromString<float>(params[1]);
				float y = fromString<float>(params[2]);

				addEntity(params[0], sf::Vector2f(x, y));
				return true;

			}, "spawn [name] [x] [y]\t-\tSpawns an entity by name."));

	console.addCommand(styx::Console::Command("debug",
			[this](std::vector<std::string> params)
			{
				if(params.size() == 1)
				{
					int level = fromString<int>(params[0]);
					setDebug(level);
					return true;

				}

				setDebug(true);
				return true;

			}, "debug [*state]\t-\tToggle engine debug."));

	out("initing engine");
	out("precaching resources");

	files.search(m_info.resourcePath);

	defaultFont = files.get<sf::Font>(m_info.fontName);

	hq2x.loadFromFile(files.fullPath("hq2x.frag"), sf::Shader::Fragment);
	hq2x.setParameter("screenSize", m_info.windowWidth, m_info.windowHeight);
	//hq2x.setParameter("ratio", sf::Vector2f(1.0, 1.0));

	consolePressed = false;
	fpsText = sf::Text("fps:\nms:", *defaultFont);
	fpsText.setPosition(10, 10);
	fpsText.setColor(sf::Color::Red);
	fpsAvg = 0;

	gameMode = NULL;
	gameStarted = false;
	srand(time(NULL));

	out(std::string("Angelscript header version: ") + ANGELSCRIPT_VERSION_STRING);
	out(std::string("Angelscript lib version: ") + asGetLibraryVersion());
	m_scripts = asCreateScriptEngine(ANGELSCRIPT_VERSION);
	registerEngine(m_scripts);
	m_ctx = m_scripts->CreateContext();

	for(unsigned int i = 0; i < files.getNumOfPaths(); i++)
	{
		if(FileSystem::getExtension(files.getPath(i)) == "cfg")
		{
			addEntityType(Config::load(files.getPath(i)));

		}

		out("precaching: \"" + files.getPath(i) + "\"");

	}

}

Engine::~Engine()
{
	trackers.clear();

	clearEntities();

	types.clear();

	behaviors.clear();

}

std::string Engine::addEntity(Entity* entity)
{
	entity->onInit();
	entities.push_back(entity);
	if(gameMode != NULL)
	{
		gameMode->onEntityCreated(entity);

	}
	return entity->getIDString();

}

Entity* Engine::retEntity(Entity* entity)
{
	addEntity(entity);
	return entity;

}

Entity* Engine::getEntity(std::string id)
{
	for(auto entity : entities)
	{
		if(entity->getIDString() == id)
		{
			return entity;

		}

	}

	return NULL;

}
bool Engine::hasEntity(std::string id)
{
	return getEntity(id) != NULL;

}

void Engine::remEntity(std::string id)
{
	std::vector<Entity*>::iterator it;
	for(it = entities.begin(); it != entities.end(); ++it)
	{
		Entity* entity = *it;
		if(entity->getIDString() == id)
		{
			entities.erase(it);
			delete entity;
			return;

		}

	}

}

Entity* Engine::takeEntity(std::string id)
{
	std::vector<Entity*>::iterator it;
	for(it = entities.begin(); it != entities.end(); ++it)
	{
		Entity* entity = *it;
		if(entity->getIDString() == id)
		{
			entities.erase(it);
			return entity;

		}

	}

	return NULL;

}

std::vector<Entity*>& Engine::getEntities()
{
	return entities;

}

void Engine::getEntitiesWithName(std::string name, std::vector<Entity*>& list)
{
	for(unsigned int i = 0; i < entities.size(); i++)
	{
		if(entities[i]->getName() == name)
		{
			list.push_back(entities[i]);

		}

	}

}


bool Engine::isRunning()
{
	return m_window.isOpen();

}

World& Engine::getWorld()
{
	return world;

}

EngineResourceManager& Engine::getFiles()
{
	return files;

}

void Engine::addBehaviorTo(Entity* entity, std::string title)
{
	entity->addBehavior(getBehavior(title));

}

void Engine::remBehavior(std::string title)
{
	for(auto behavior : behaviors)
	{
		if(behavior->getTitle() == title)
		{
			behaviors.erase(behavior);
			return;

		}

	}

}

EntityBehavior::Ptr Engine::getBehavior(std::string title)
{
	for(auto behavior : behaviors)
	{
		if(behavior->getTitle() == title)
		{
			return behavior;

		}

	}

	ScriptEntityBehavior* behavior = new ScriptEntityBehavior(this, title);
	EntityBehavior::Ptr sptr = EntityBehavior::Ptr(behavior);
	if(behavior->isValid())
	{
		behaviors.insert(sptr);
		return sptr;

	}

	out("could not load script: " + title);

	return NULL;

}

bool Engine::hasBehavior(std::string title)
{
	return getBehavior(title) != NULL;

}

void Engine::remGameModeBehavior(std::string title)
{
	for(auto behavior : behaviors)
	{
		if(behavior->getTitle() == title)
		{
			behaviors.erase(behavior);
			return;

		}

	}

}

GameModeBehavior::Ptr Engine::getGameModeBehavior(std::string title)
{
	for(auto behavior : gameModeBehaviors)
	{
		if(behavior->getTitle() == title)
		{
			return behavior;

		}

	}

	return NULL;

}

bool Engine::hasGameModeBehavior(std::string title)
{
	return getGameModeBehavior(title) != NULL;

}

void Engine::addEntityType(std::string file)
{
	EntityType::Ptr type = EntityType::create(this, file);
	if(!hasEntityType(type->getName()))
	{
		types.push_back(type);
		return;

	}

}

EntityType::Ptr Engine::getEntityType(std::string name)
{
	for(auto type : types)
	{
		if(type->getName() == name)
		{
			return type;

		}

	}

	return NULL;

}

bool Engine::hasEntityType(std::string name)
{
	return name != "" && getEntityType(name) != NULL;

}

std::string Engine::addEntity(std::string name, Vec2 pos)
{
	EntityType::Ptr type = getEntityType(name);
	if(type != NULL)
	{
		return addEntity(new Entity(type, pos));

	}

	return "";

}

Entity* Engine::retEntity(std::string name, Vec2 pos)
{
	EntityType::Ptr type = getEntityType(name);
	if(type != NULL)
	{
		return retEntity(new Entity(type, pos));

	}

	return NULL;

}

Entity* Engine::entityAt(Vec2 pos)
{
	Entity* ret = NULL;
	world.query(pos, Vec2(0.1, 0.1),
			[&ret](Body& body)
			{
				ret = body.getData<Entity>();
				return false;

			});
	return ret;

}

bool Engine::isEntityAt(Vec2 pos)
{
	bool ret = false;
	world.query(pos, Vec2(0.1, 0.1),
			[&ret](Body& body)
			{
				ret = true;
				return false;

			});
	return ret;

}

bool Engine::isOtherEntityAt(Vec2 pos, Entity* self)
{
	bool ret = false;
	world.query(pos, Vec2(0.1, 0.1),
			[&ret, self](Body& body)
			{
				Entity* e = body.getData<Entity>();
				if(e != self){ret = true; return false;}
				return true;

			});
	return ret;

}

std::vector<Entity*> Engine::getEntitiesInBox(Vec2 pos, Vec2 size)
{
	std::vector<Entity*> ret;
	world.query(pos, size,
			[&ret](Body& body)
			{
				ret.push_back(body.getData<Entity>());
				return true;

			});
	return ret;

}

std::vector<Entity*> Engine::getEntitiesInBoxWithName(Vec2 pos, Vec2 size, std::string name)
{
	std::vector<Entity*> ret;
	world.query(pos, size,
			[&ret, &name](Body& body)
			{
				Entity* e = body.getData<Entity>();
				if(e->getName() == name)
					ret.push_back(e);

				return true;

			});
	return ret;

}

std::vector<Entity*> Engine::getEntitiesInDist(Vec2 pos, float dist)
{
	std::vector<Entity*> ret;
	for(unsigned int i = 0; i < entities.size(); i++)
	{
		if(entities[i]->distanceTo(pos) <= dist)
		{
			ret.push_back(entities[i]);

		}

	}

	return ret;

}

std::vector<Entity*> Engine::getEntitiesInDistWithName(Vec2 pos, float dist, std::string name)
{
	std::vector<Entity*> ret;
	for(unsigned int i = 0; i < entities.size(); i++)
	{
		if(entities[i]->distanceTo(pos) <= dist && entities[i]->getName() == name)
		{
			ret.push_back(entities[i]);

		}

	}

	return ret;

}

void Engine::out(std::string msg)
{
	std::cout << msg << "\n";
	console.out(msg);

}

void Engine::setDebug(int d)
{
	m_info.debugLevel = d;
	if(m_info.debugLevel < 1)
	{
		trackers.clear();

	}

}

int Engine::getDebug()
{
	return m_info.debugLevel;

}

void Engine::setHq2x(bool state)
{
	m_info.hq2xOn = state;

}

bool Engine::isHq2xOn()
{
	return m_info.hq2xOn;

}

void Engine::updateZ()
{
	if(defer)
	{
		orderingRequested = true;
		return;

	}

	std::sort(entities.begin(), entities.end(), [](Entity* lhs, Entity* rhs){return lhs->getSprite().getZ() < rhs->getSprite().getZ();});

}

void Engine::addTracker(Entity* target)
{
	if(!hasTracker(target) && trackers.size() != m_info.maxTrackers)
	{
		trackers.push_back(EntityTracker::create(desktop, target));

	}


}

EntityTracker::Ptr Engine::getTracker(Entity* target)
{
	for(auto tracker : trackers)
	{
		if(target == tracker->getTarget())
		{
			return tracker;

		}

	}

	return NULL;

}

bool Engine::hasTracker(Entity* target)
{
	return getTracker(target) != NULL;

}

void Engine::remTracker(Entity* target)
{
	std::vector<EntityTracker::Ptr>::iterator it;
	for(it = trackers.begin(); it != trackers.end(); ++it)
	{
		EntityTracker::Ptr tracker = *it;
		if(tracker->getTarget() == target)
		{
			trackers.erase(it);
			return;

		}

	}

}

unsigned int Engine::getMaxTrackers()
{
	return m_info.maxTrackers;

}

void Engine::setMaxTrackers(unsigned int max)
{
	m_info.maxTrackers = max;

}

void Engine::setGameMode(std::string file)
{
	if(gameMode != NULL)
	{
		delete gameMode;

	}

	gameMode = new GameMode(this, file);

}

GameMode* Engine::getGameMode()
{
	return gameMode;

}

void Engine::clearEntities()
{
	while(!entities.empty())
	{
		Entity* entity = *entities.begin();
		entities.erase(entities.begin());
		delete entity;

	}

}

void Engine::draw(Vec2 pos, Vec2 size, sf::Color color)
{
	sf::RectangleShape shape(size.to<sf::Vector2f>());
	shape.setPosition(pos.to<sf::Vector2f>());
	shape.setFillColor(color);
	m_window.draw(shape);

}

void Engine::draw(Vec2 pos, Vec2 size, std::string img)
{
	sf::RectangleShape shape(size.to<sf::Vector2f>());
	shape.setPosition(pos.to<sf::Vector2f>());
	shape.setTexture(files.get<sf::Texture>(img));
	m_window.draw(shape);

}

void Engine::draw(Vec2 pos, float radius, sf::Color color)
{
	sf::CircleShape shape(radius);
	shape.setPosition(pos.to<sf::Vector2f>());
	shape.setOrigin(radius, radius);
	shape.setFillColor(color);
	m_window.draw(shape);

}

void Engine::draw(Vec2 pos, float radius, std::string img)
{
	sf::CircleShape shape(radius);
	shape.setPosition(pos.to<sf::Vector2f>());
	shape.setOrigin(radius, radius);
	shape.setTexture(files.get<sf::Texture>(img));
	m_window.draw(shape);

}

void Engine::draw(Vec2 pos, std::string text, sf::Color color, unsigned int size)
{
	sf::Text t(text, *defaultFont, size);
	t.setPosition(pos.to<sf::Vector2f>());
	t.setColor(color);
	m_window.draw(t);

}

void Engine::draw(Vec2 pos, std::string text, std::string font, sf::Color color, unsigned int size)
{
	sf::Font* f = files.get<sf::Font>(font);
	if(f == NULL)
	{
		f = defaultFont;

	}

	sf::Text t(text, *f, size);
	t.setPosition(pos.to<sf::Vector2f>());
	t.setColor(color);
	m_window.draw(t);

}

unsigned int Engine::getFrameCount()
{
	return frameCount;

}

ParticleManager& Engine::getParticles()
{
	return particles;

}

void Engine::addParticleType(std::string name, std::string imgName, Vec2 frameSize)
{
	sf::Texture* texture = files.get<sf::Texture>(imgName);
	if(texture == NULL)
		return;

	particles.add(name, texture, frameSize.to<sf::Vector2u>(), 500);

}

void Engine::addParticleType(std::string name, std::string imgName, Vec2 framePos, Vec2 frameSize)
{
	sf::Texture* texture = files.get<sf::Texture>(imgName);
	if(texture == NULL)
		return;

	sf::IntRect rect;
	rect.left = framePos.x;
	rect.top = framePos.y;
	rect.width = frameSize.x;
	rect.height = frameSize.y;

	particles.add(name, texture, rect, 500);

}

void Engine::addParticleType(std::string name, sf::Color color, Vec2 size)
{
	particles.add(name, color, 0, size.to<sf::Vector2u>(), 500);

}

//in meters
void Engine::emit(std::string name, Vec2 pos, Vec2 vel, float lifetime, float scale)
{
	particles.emit(name, toScreen(pos), toScreen(vel), lifetime, scale);

}

Camera& Engine::getCamera()
{
	return m_camera;

}

void Engine::play(std::string soundName, float pitch, float volume)
{
	std::string path = files.fullPath(soundName);
	soundManager.play(path, pitch, volume);

}

SoundManager* Engine::getSoundManager()
{
	return &soundManager;

}

asIScriptEngine* Engine::getScripts()
{
	return m_scripts;

}

asIScriptContext* Engine::getContext()
{
	return m_ctx;

}

}
