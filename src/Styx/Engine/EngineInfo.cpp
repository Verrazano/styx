#include "EngineInfo.h"
#include <SFML/Graphics.hpp>

namespace styx
{

EngineInfo::EngineInfo()
{
	frameRate = 60;
	antialiasingLevel = 2;
	hq2xOn = true;
	windowWidth = 854;
	windowHeight = 480;
	viewWidth = windowWidth;
	viewHeight = windowHeight;
	resourcePath = "res";
	debugLevel = 0;
	fontName = "consolas.ttf";
	maxTrackers = 2;
	fullScreen = false;
	consoleButton = sf::Keyboard::Home;
	trackerButton = sf::Keyboard::F1;
	gravityX = 0;
	gravityY = -19.6;
	volume = 100.0f;
	mute = false;

}

}
