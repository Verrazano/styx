#include "Engine.h"
#include "../Game/Entity/EntityType/EntityType.h"
#include "../Game/Entity/Entity.h"
#include "../Game/Entity/EntityBehavior.h"
#include "../Util/Util.h"
#include "../Util/convert/mousetoview.h"
#include "../Graphics/UI/EntityTracker.h"
#include "../Game/GameMode/GameMode.h"
#include <algorithm>
#include <iostream>
#include <time.h>

namespace styx
{

void Engine::tick()
{
	while(m_window.pollEvent(event))
	{
		console.update();
		desktop.HandleEvent(event);

		if(event.type == sf::Event::Closed || (m_info.fullScreen && sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)))
		{
			m_window.close();
			return;

		}

		if(sf::Keyboard::isKeyPressed((sf::Keyboard::Key)m_info.consoleButton))
		{
			if(!consolePressed)
			{
				consolePressed = true;
				console.show(!console.isShowing());

			}

		}
		else
		{
			consolePressed = false;

		}

		if(event.type == sf::Event::Resized)
		{
			Vec2 newSize = m_window.getSize();
			hq2xTexture.create(newSize.x, newSize.y);
			Vec2 viewSize = m_camera.getSize();
			hq2x.setParameter("screenSize", newSize.to<sf::Vector2f>());//sanity check
			hq2xRect.setTextureRect(sf::IntRect(0, 0, newSize.x, newSize.y));

		}

		if(m_info.debugLevel >= 1 && trackers.size() != m_info.maxTrackers)
		{
			if(sf::Keyboard::isKeyPressed((sf::Keyboard::Key)m_info.trackerButton))
			{
				sf::View view = m_camera.getView();
				sf::Vector2f mousePos = mouseToView(view, m_window);
				Entity* entity = entityAt(toWorld(mousePos));
				if(entity != NULL)
				{
					addTracker(entity);

				}

			}

		}

	}

	desktop.Update(1.0f/m_info.frameRate);

	float elapsedTime = frameTimer.restart().asSeconds();
	float fps = (int)(1.0f/elapsedTime);
	fpsAvg += fps;

	if(frameCount%m_info.frameRate == 0)
	{
		fpsAvg /= m_info.frameRate;
		fpsText.setString("fps:"+toString((int)fpsAvg));
		fpsAvg = 0;

	}

	if(!gameStarted && gameMode != NULL)
	{
		if(gameMode->onPreGame())
		{
			gameStarted = true;
			clearEntities();
			gameMode->onStart();

		}

	}

	world.step();

	if(gameStarted && gameMode != NULL)
	{
		gameMode->onTick();
		if(gameMode->isGameOver())
		{
			gameStarted = false;
			clearEntities();

		}

	}

	m_window.clear(sf::Color::White);

	m_camera.update();

	if(m_info.debugLevel >= 3)
	{
		m_window.setView(m_camera.getZoomedView());

	}
	else
	{
		m_window.setView(m_camera.getView());

	}

	sf::FloatRect viewRect = m_camera.getViewRect();

	defer = true;
	orderingRequested = false;
	std::vector<Entity*>::iterator it;
	for(it = entities.begin(); it != entities.end(); ++it)
	{
		Entity* entity = *it;

		if(entity->isDead())
		{
			it = entities.erase(it);
			it--;

			int index = it - entities.begin();
			int sizeBefore = entities.size();

			entity->onDeath();
			if(gameMode != NULL)
			{
				gameMode->onEntityDestroyed(entity);

			}

			int diff = sizeBefore - entities.size();

			if(diff != 0)
			{
				it = entities.begin();
				std::advance(it, index);

			}

			if(m_camera.getTargetEntity() == entity)
			{
				m_camera.disableTarget();

			}
			remTracker(entity);

			delete entity;

			continue;

		}

		entity->onTick();

		if(entity->getSprite().shouldDraw(m_camera))
		{
			entity->onDraw();
			entity->getSprite().draw(m_window);

		}

	}
	defer = false;
	if(orderingRequested)
	{
		orderingRequested = false;
		updateZ();

	}

	particles.update();
	particles.draw(m_window);

	if(m_info.debugLevel >= 2)
	{
		world.drawDebug();

	}

	if(m_info.debugLevel >= 3)
	{

		sf::RectangleShape viewShape(sf::Vector2f(viewRect.width, viewRect.height));
		viewShape.setPosition(viewRect.left, viewRect.top);
		viewShape.setFillColor(sf::Color(0, 0, 255, 1));
		viewShape.setOutlineColor(sf::Color::Red);
		viewShape.setOutlineThickness(5);
		m_window.draw(viewShape);

	}

	//GUI
	m_window.setView(m_camera.getGUIView());

	if(gameMode != NULL)
	{
		gameMode->onDraw();

	}

	if(m_info.hq2xOn)
	{
		hq2xTexture.update(m_window);
		m_window.draw(hq2xRect, &hq2x);

	}

	if(m_info.debugLevel >= 1)
	{
		std::vector<EntityTracker::Ptr>::iterator it2;
		for(it2 = trackers.begin(); it2 != trackers.end(); ++it2)
		{
			EntityTracker::Ptr tracker = *it2;
			if(!tracker->isOpen())
			{
				trackers.erase(it2);
				--it2;
				continue;

			}

			tracker->update();
			tracker->draw(m_window, sf::Vector2f(viewRect.left, viewRect.top));

		}

	}

	sfgui.Display(m_window);

	if(m_info.debugLevel >= 1)
	{
		m_window.draw(fpsText);

	}

	m_window.display();

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::P))
	{
		sf::Texture text;
		text.create(m_info.windowWidth, m_info.windowHeight);
		text.update(m_window);
		text.copyToImage().saveToFile("sn.png");

	}

	frameCount++;

}

}
