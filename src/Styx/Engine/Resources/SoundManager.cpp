#include "SoundManager.h"

namespace styx
{

SoundManager::SoundManager()
{
}

void SoundManager::addSound(std::string path, sf::SoundBuffer* buffer)
{
	m_sounds.push_back(std::pair<std::string, sf::Sound>(path, sf::Sound(*buffer)));

}

void SoundManager::play(std::string path, float pitch, float volume)
{
	sf::Sound* sound = getSound(path);
	if(sound != NULL)
	{
		sound->setVolume(volume);
		sound->setPitch(pitch);
		sound->play();

	}

}

bool SoundManager::isPlaying(std::string path)
{
	sf::Sound* sound = getSound(path);
	if(sound != NULL)
	{
		return sound->getStatus() == sf::Sound::Playing;

	}

	return false;

}

void SoundManager::stop(std::string path)
{
	sf::Sound* sound = getSound(path);
	if(sound != NULL)
	{
		sound->stop();

	}

}

sf::Sound* SoundManager::getSound(std::string path)
{
	for(unsigned int i = 0; i < m_sounds.size(); i++)
	{
		if(m_sounds[i].first == path)
		{
			return &m_sounds[i].second;

		}

	}

	return NULL;

}

}
