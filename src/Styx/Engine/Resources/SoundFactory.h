#ifndef SOUNDFACTORY_H_
#define SOUNDFACTORY_H_

#include "../../Util/ResourceManager/Resource.h"
#include "SoundManager.h"

class SoundFactory : public ResourceFactory
{
public:
	SoundFactory(std::string extension, styx::SoundManager* soundManager);
	~SoundFactory();

	static Resource* factory(std::string path);
	static void recycler(void* data);

	static styx::SoundManager* m_soundManager;

};



#endif /* SOUNDFACTORY_H_ */
