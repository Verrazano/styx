#ifndef FONTFACTORY_H_
#define FONTFACTORY_H_

#include "../../Util/ResourceManager/ResourceFactory.h"

class FontFactory : public ResourceFactory
{
public:
	FontFactory(std::string extension);
	~FontFactory();

	static Resource* factory(std::string path);
	static void recycler(void* data);

};

#endif /* FONTFACTORY_H_ */
