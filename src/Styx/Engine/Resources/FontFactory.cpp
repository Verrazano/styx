#include "FontFactory.h"
#include <SFML/Graphics.hpp>
#include "../../Util/ResourceManager/Resource.h"
#include <iostream>

FontFactory::FontFactory(std::string extension) : ResourceFactory(extension, FontFactory::factory, FontFactory::recycler)
{
}

FontFactory::~FontFactory()
{
}

Resource* FontFactory::factory(std::string path)
{
	sf::Font* t = new sf::Font;
	if(!t->loadFromFile(path))
	{
		delete t;
		return NULL;

	}

	Resource* r = new Resource(t, path, FontFactory::recycler);
	return r;

}

void FontFactory::recycler(void* data)
{
	delete (sf::Font*)data;

}
