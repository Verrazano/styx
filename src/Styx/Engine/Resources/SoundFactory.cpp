#include "SoundFactory.h"
#include <SFML/Audio.hpp>
#include "../../Util/ResourceManager/Resource.h"
#include <iostream>

styx::SoundManager* SoundFactory::m_soundManager;

SoundFactory::SoundFactory(std::string extension, styx::SoundManager* soundManager) : ResourceFactory(extension, SoundFactory::factory, SoundFactory::recycler)
{
	m_soundManager = soundManager;

}

SoundFactory::~SoundFactory()
{
}

Resource* SoundFactory::factory(std::string path)
{
	sf::SoundBuffer* t = new sf::SoundBuffer;
	if(!t->loadFromFile(path))
	{
		delete t;
		return NULL;

	}

	m_soundManager->addSound(path, t);

	Resource* r = new Resource(t, path, SoundFactory::recycler);
	return r;

}

void SoundFactory::recycler(void* data)
{
	delete (sf::SoundBuffer*)data;

}
