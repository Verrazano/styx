#ifndef SOUNDMANAGER_H_
#define SOUNDMANAGER_H_

#include <SFML/Audio.hpp>
#include <vector>

namespace styx
{

class SoundManager
{
public:
	SoundManager();

	void addSound(std::string path, sf::SoundBuffer* buffer);

	void play(std::string path, float pitch = 1.0f, float volume = 100.0f);
	bool isPlaying(std::string path);
	void stop(std::string path);

	sf::Sound* getSound(std::string path);

private:
	std::vector<std::pair<std::string, sf::Sound> > m_sounds;

};

}

#endif /* SOUNDMANAGER_H_ */
