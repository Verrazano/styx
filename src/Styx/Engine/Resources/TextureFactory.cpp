#include "TextureFactory.h"
#include <SFML/Graphics.hpp>
#include "../../Util/ResourceManager/Resource.h"
#include <iostream>

TextureFactory::TextureFactory(std::string extension) : ResourceFactory(extension, TextureFactory::factory, TextureFactory::recycler)
{
}

TextureFactory::~TextureFactory()
{
}

Resource* TextureFactory::factory(std::string path)
{
	sf::Texture* t = new sf::Texture;
	if(!t->loadFromFile(path))
	{
		delete t;
		return NULL;

	}

	/*sf::Image i = t->copyToImage();
	i.createMaskFromColor(sf::Color::White, 0);
	t->update(i);*/
	t->setSmooth(false);

	Resource* r = new Resource(t, path, TextureFactory::recycler);
	return r;

}

void TextureFactory::recycler(void* data)
{
	delete (sf::Texture*)data;

}
