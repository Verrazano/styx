#ifndef ENGINEINFO_H_
#define ENGINEINFO_H_

#include <string>
#include <SFML/System/Vector2.hpp>

namespace styx
{

class EngineInfo
{
public:
	EngineInfo();

	unsigned int frameRate;
	unsigned int antialiasingLevel;
	bool hq2xOn;
	unsigned int windowWidth;
	unsigned int windowHeight;
	unsigned int viewWidth;
	unsigned int viewHeight;
	std::string resourcePath;
	unsigned int debugLevel;
	std::string fontName;
	unsigned int maxTrackers;
	bool fullScreen;
	int consoleButton;
	int trackerButton;
	float gravityX;
	float gravityY;
	float volume;
	bool mute;

};

}

#endif /* ENGINEINFO_H_ */
