#ifndef ENGINERESOURCEMANAGER_H_
#define ENGINERESOURCEMANAGER_H_

#include "../Util/ResourceManager/ResourceManager.h"
#include "Resources/SoundManager.h"

namespace styx
{

class EngineResourceManager : public ResourceManager
{
public:
	EngineResourceManager(SoundManager* soundManager);

};

}

#endif /* ENGINERESOURCEMANAGER_H_ */
