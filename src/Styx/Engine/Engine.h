#ifndef ENGINE_H_
#define ENGINE_H_

#include <string>
#include <vector>
#include <SFML/Graphics.hpp>
#include "../Graphics/Graphics.h"
#include <SFGUI/SFGUI.hpp>
#include "../Graphics/UI/Console.h"
#include <Box2D/Box2D.h>
#include "../Graphics/Box2DDebugDrawer/Box2DDebugDrawer.h"
#include <unordered_set>
#include "EngineResourceManager.h"
#include <list>
#include <memory>
#include <assert.h>
#include "../Game/Entity/EntityBehavior.h"
#include "../Game/GameMode/GameModeBehavior.h"
#include "EngineInfo.h"
#include <cstdlib>
#include "../Graphics/Particles/ParticleManager.h"
#include "../Physics/Physics.h"
#include "../Util/Util.h"
#include "Resources/SoundManager.h"
#include <angelscript.h> //MOVE angelscript into its own file

namespace styx
{

class Entity;
class EntityType;
class EntityTracker;
class GameMode;

class Engine
{
public:
	Engine(EngineInfo info);

	~Engine();

	std::string addEntity(Entity* entity);
	Entity* retEntity(Entity* entity);
	Entity* getEntity(std::string id);
	bool hasEntity(std::string id);
	void remEntity(std::string id);
	//potential memory leak if this is used incorrectly be careful.
	Entity* takeEntity(std::string id);
	std::vector<Entity*>& getEntities();
	void getEntitiesWithName(std::string name, std::vector<Entity*>& list);

	void tick();
	bool isRunning();

	World& getWorld();
	EngineResourceManager& getFiles();

	void addBehaviorTo(Entity* entity, std::string title);

	template<class T, typename... Args>
	bool addBehavior(Args&&... args);

	void remBehavior(std::string title);
	EntityBehavior::Ptr getBehavior(std::string title);
	bool hasBehavior(std::string title);

	template<class T, typename... Args>
	bool addGameModeBehavior(Args&&... args);

	void remGameModeBehavior(std::string title);
	GameModeBehavior::Ptr getGameModeBehavior(std::string title);
	bool hasGameModeBehavior(std::string title);

	void addEntityType(std::string file);
	std::shared_ptr<EntityType> getEntityType(std::string name);
	bool hasEntityType(std::string name);

	std::string addEntity(std::string name, Vec2 pos = Vec2(0, 0));
	Entity* retEntity(std::string name, Vec2 pos = Vec2(0, 0));

	Entity* entityAt(Vec2 pos);
	bool isEntityAt(Vec2 pos);
	bool isOtherEntityAt(Vec2 pos, Entity* self);
	std::vector<Entity*> getEntitiesInBox(Vec2 pos, Vec2 size);
	std::vector<Entity*> getEntitiesInBoxWithName(Vec2 pos, Vec2 size, std::string name);
	std::vector<Entity*> getEntitiesInDist(Vec2 pos, float dist);
	std::vector<Entity*> getEntitiesInDistWithName(Vec2 pos, float dist, std::string name);

	void out(std::string msg);

	void setDebug(int d);
	int getDebug();

	void setHq2x(bool state);
	bool isHq2xOn();

	void updateZ();

	void addTracker(Entity* target);
	std::shared_ptr<EntityTracker> getTracker(Entity* target);
	bool hasTracker(Entity* target);
	void remTracker(Entity* target);

	unsigned int getMaxTrackers();
	void setMaxTrackers(unsigned int max);

	void setGameMode(std::string file);
	GameMode* getGameMode();

	void clearEntities();

	void draw(Vec2 pos, Vec2 size, sf::Color color);
	void draw(Vec2 pos, Vec2 size, std::string img);
	void draw(Vec2 pos, float radius, sf::Color color);
	void draw(Vec2 pos, float radius, std::string img);
	void draw(Vec2 pos, std::string text, sf::Color color, unsigned int size = 30);
	void draw(Vec2 pos, std::string text, std::string font, sf::Color color, unsigned int size = 30);

	unsigned int getFrameCount();

	//PARTICLE STUFF
	ParticleManager& getParticles();
	void addParticleType(std::string name, std::string imgName, Vec2 frameSize);
	void addParticleType(std::string name, std::string imgName, Vec2 framePos, Vec2 frameSize);
	void addParticleType(std::string name, sf::Color color, Vec2 size);

	//in meters
	void emit(std::string name, Vec2 pos, Vec2 vel, float lifetime, float scale = 1.0f);

	Camera& getCamera();

	void play(std::string soundName, float pitch = 1.0f, float volume = 100.0f);
	SoundManager* getSoundManager();

	asIScriptEngine* getScripts();
	asIScriptContext* getContext();

private:
	std::vector<Entity*> entities;
	sf::RenderWindow m_window;
	Camera m_camera;
	sf::Event event;
	World world;
	Box2DDebugDrawer debugDraw;
	EngineResourceManager files;
	std::unordered_set<EntityBehavior::Ptr> behaviors;
	std::vector<std::shared_ptr<EntityType> > types;
	sfg::SFGUI sfgui;
	sfg::Desktop desktop;
	bool consolePressed;
	Console console;
	sf::Shader hq2x;
	sf::Texture hq2xTexture;
	sf::RectangleShape hq2xRect;
	sf::Clock frameTimer;
	sf::Text fpsText;
	unsigned int frameCount;
	float fpsAvg;

	sf::Font* defaultFont;

 	std::vector<std::shared_ptr<EntityTracker> > trackers;
 	EngineInfo m_info;

 	std::unordered_set<GameModeBehavior::Ptr> gameModeBehaviors;
 	GameMode* gameMode;
 	bool gameStarted;
 	ParticleManager particles;

 	bool defer;
 	bool orderingRequested;

 	SoundManager soundManager;

 	asIScriptEngine* m_scripts;
 	asIScriptContext* m_ctx;

};

#include "Engine.inl"

}

#endif /* ENGINE_H_ */
