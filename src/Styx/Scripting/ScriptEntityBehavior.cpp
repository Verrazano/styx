#include "ScriptEntityBehavior.h"
#include "../Util/Config/Config.h"

namespace styx
{

ScriptEntityBehavior::ScriptEntityBehavior(Engine* engine, std::string path) :
				m_engine(engine),
				m_ctx(m_engine->getContext()),
				m_path(path),
				m_onInit(NULL),
				m_onTick(NULL),
				m_onDraw(NULL),
				m_onDeath(NULL),
				m_shouldCollide(NULL),
				m_onCollide(NULL),
				m_onEndCollide(NULL)
{
	EngineResourceManager& files = m_engine->getFiles();
	path = files.fullPath(m_path);
	m_file = Config::load(path, false, [&files](std::string path){return files.fullPath(path);});
	asIScriptEngine* script = m_engine->getScripts();
	asIScriptModule* mod = script->GetModule(path.c_str());

	if(mod == NULL)
	{
		mod = script->GetModule(m_path.c_str(), asGM_ALWAYS_CREATE);
		mod->AddScriptSection(path.c_str(), m_file.c_str(), m_file.size());
		mod->Build();

	}

	m_onInit = mod->GetFunctionByDecl("void onInit(Entity@)");
	m_onTick = mod->GetFunctionByDecl("void onTick(Entity@)");
	m_onDraw = mod->GetFunctionByDecl("void onDraw(Entity@)");
	m_onDeath = mod->GetFunctionByDecl("void onDeath(Entity@)");
	m_shouldCollide = mod->GetFunctionByDecl("bool shouldCollide(Entity@, Entity@)");
	m_onCollide = mod->GetFunctionByDecl("void onCollide(Entity@, Entity@)");
	m_onEndCollide = mod->GetFunctionByDecl("void onCollide(Entity@, Entity@)");
	m_fullPath = path;

}

std::string ScriptEntityBehavior::getTitle()
{
	return m_path;

}

std::string ScriptEntityBehavior::getFile()
{
	return m_file;

}

void ScriptEntityBehavior::onInit(Entity* self)
{
	/*we create a new context to run on init functions because they can be executed from within
	 * another script by calling something that creates a new entity.*/
	asIScriptContext* ctx  = m_engine->getScripts()->CreateContext();
	if(m_onInit != NULL && ctx->Prepare(m_onInit) >= 0)
	{
		ctx->SetArgAddress(0, self);
		ctx->Execute();
		ctx->Unprepare();

	}
	ctx->Release();

}

void ScriptEntityBehavior::onTick(Entity* self)
{
	if(m_onTick != NULL && m_ctx->Prepare(m_onTick) >= 0)
	{
		m_ctx->SetArgAddress(0, self);
		m_ctx->Execute();
		m_ctx->Unprepare();

	}

}

void ScriptEntityBehavior::onDraw(Entity* self)
{
	if(m_onDraw != NULL && m_ctx->Prepare(m_onDraw) >= 0)
	{
		m_ctx->SetArgAddress(0, self);
		m_ctx->Execute();
		m_ctx->Unprepare();

	}

}

void ScriptEntityBehavior::onDeath(Entity* self)
{
	if(m_onDeath != NULL && m_ctx->Prepare(m_onDeath) >= 0)
	{
		m_ctx->SetArgAddress(0, self);
		m_ctx->Execute();
		m_ctx->Unprepare();

	}

}

bool ScriptEntityBehavior::shouldCollide(Entity* self, Entity* other)
{
	if(m_shouldCollide != NULL && m_ctx->Prepare(m_shouldCollide) >= 0)
	{
		m_ctx->SetArgAddress(0, self);
		m_ctx->SetArgAddress(1, other);
		m_ctx->Execute();
		bool ret = m_ctx->GetReturnByte();
		m_ctx->Unprepare();
		return ret;

	}

	return true;

}

void ScriptEntityBehavior::onCollide(Entity* self, Entity* other)
{
	if(m_onCollide != NULL && m_ctx->Prepare(m_onCollide) >= 0)
	{
		m_ctx->SetArgAddress(0, self);
		m_ctx->SetArgAddress(1, other);
		m_ctx->Execute();
		m_ctx->Unprepare();

	}

}

void ScriptEntityBehavior::onEndCollide(Entity* self, Entity* other)
{
	if(m_onEndCollide != NULL && m_ctx->Prepare(m_onEndCollide) >= 0)
	{
		m_ctx->SetArgAddress(0, self);
		m_ctx->SetArgAddress(1, other);
		m_ctx->Execute();
		m_ctx->Unprepare();

	}

}

bool ScriptEntityBehavior::isValid()
{
	return m_onInit != NULL || m_onTick != NULL || m_onDraw != NULL || m_onDeath != NULL || m_shouldCollide != NULL || m_onCollide != NULL || m_onEndCollide != NULL;

}

}
