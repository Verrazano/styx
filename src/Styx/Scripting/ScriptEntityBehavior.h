#ifndef SCRIPTENTITYBEHAVIOR_H_
#define SCRIPTENTITYBEHAVIOR_H_

#include <angelscript.h>
#include "../Game/Entity/EntityBehavior.h"
#include "../Engine/Engine.h"

namespace styx
{

class ScriptEntityBehavior : public EntityBehavior
{
public:
	ScriptEntityBehavior(Engine* engine, std::string title);

	std::string getTitle();

	std::string getFile();

	void onInit(Entity* self);
	void onTick(Entity* self);
	void onDraw(Entity* self);
	void onDeath(Entity* self);

	bool shouldCollide(Entity* self, Entity* other);
	void onCollide(Entity* self, Entity* other);
	void onEndCollide(Entity* self, Entity* other);

	bool isValid();

private:
	Engine* m_engine;
	asIScriptContext* m_ctx;
	std::string m_path;
	std::string m_fullPath;
	std::string m_file;
	asIScriptFunction* m_onInit;
	asIScriptFunction* m_onTick;
	asIScriptFunction* m_onDraw;
	asIScriptFunction* m_onDeath;
	asIScriptFunction* m_shouldCollide;
	asIScriptFunction* m_onCollide;
	asIScriptFunction* m_onEndCollide;

};

}

#endif /* SCRIPTENTITYBEHAVIOR_H_ */
