#include "ScriptEngine.h"
#include "../Util/Strings/Strings.h"
#include <iostream>

#include "scriptarray/scriptarray.h"
#include "scriptmath/scriptmath.h"
#include "scriptstdstring/scriptstdstring.h"

#include "../Engine/Engine.h"
#include "../Game/Entity/Entity.h"
#include "../Game/GameMode/GameMode.h"

#include <angelscript.h>

namespace styx
{

void Vec2ParamConstructor(float x, float y, void* memory)
{
	new (memory)Vec2(x, y);

}

void registerVec2(asIScriptEngine* engine)
{
	engine->RegisterObjectType("Vec2", sizeof(Vec2), asOBJ_VALUE | asOBJ_POD | asOBJ_APP_CLASS | asOBJ_APP_CLASS_ALLFLOATS);
	engine->RegisterObjectBehaviour("Vec2", asBEHAVE_CONSTRUCT, "void f(float, float)", asFUNCTION(Vec2ParamConstructor), asCALL_CDECL_OBJLAST);
	engine->RegisterObjectProperty("Vec2", "float x", asOFFSET(Vec2, x));
	engine->RegisterObjectProperty("Vec2", "float y", asOFFSET(Vec2, y));

}

int randInt()
{
	return (rand()%101);

}

float randFloat()
{
	return ((float)randInt())/100.0f;

}

void registerEngine(asIScriptEngine* engine)
{
	engine->SetMessageCallback(asFUNCTION(styx::messageCallBack), 0, asCALL_CDECL);

	RegisterScriptMath(engine);
	RegisterScriptArray(engine, true);
	RegisterStdString(engine);
	registerVec2(engine);

	engine->RegisterGlobalFunction("float randFloat()", asFUNCTION(randFloat), asCALL_CDECL);
	engine->RegisterGlobalFunction("int rand()", asFUNCTION(randInt), asCALL_CDECL);

	engine->RegisterObjectType("Entity", sizeof(Entity), asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterObjectType("Engine", sizeof(Engine), asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterObjectType("Body", sizeof(Body), asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterObjectMethod("Entity", "Engine@ getEngine()",  asMETHOD(Entity, getEngine), asCALL_THISCALL);
	engine->RegisterObjectMethod("Engine", "void addParticleType(string, string, Vec2)", asMETHODPR(Engine, addParticleType, (std::string, std::string, Vec2), void), asCALL_THISCALL);
	engine->RegisterObjectMethod("Entity", "Body@ getBody()", asMETHOD(Entity, getBody), asCALL_THISCALL);
	engine->RegisterObjectMethod("Body", "Vec2 getPosition()", asMETHOD(Body, getPosition), asCALL_THISCALL);
	engine->RegisterObjectMethod("Entity", "void emit(string,Vec2,float life=1.0f,float scale=1.0,Vec2 offset=Vec2(0,0))", asMETHOD(Entity, emit), asCALL_THISCALL);
	//TODO: change the engine version to spawn because it sounds cooler
	engine->RegisterObjectMethod("Engine", "Entity@ spawn(string, Vec2)", asMETHODPR(Engine, retEntity, (std::string, Vec2), Entity*), asCALL_THISCALL);
	engine->RegisterObjectMethod("Body", "void applyForce(Vec2)", asMETHODPR(Body, applyForce, (Vec2), void), asCALL_THISCALL);
	engine->RegisterObjectMethod("Body", "void applyForce(float, float)", asMETHODPR(Body, applyForce, (float, float), void), asCALL_THISCALL);
	engine->RegisterObjectMethod("Body", "float getSpeed()", asMETHOD(Body, getSpeed), asCALL_THISCALL);
	engine->RegisterObjectMethod("Engine", "void play(string,float pitch=1.0f, float volume=100.0f)", asMETHOD(Engine, play), asCALL_THISCALL);
	engine->RegisterObjectMethod("Engine", "void print(string)", asMETHOD(Engine, out), asCALL_THISCALL);
	engine->RegisterObjectMethod("Entity", "void setBool(string,bool)", asMETHOD(Entity, setBool), asCALL_THISCALL);
	engine->RegisterObjectMethod("Entity", "bool getBool(string)", asMETHOD(Entity, getBool), asCALL_THISCALL);
	engine->RegisterObjectMethod("Entity", "void setInt(string,bool)", asMETHOD(Entity, setInt), asCALL_THISCALL);
	engine->RegisterObjectMethod("Entity", "int getInt(string)", asMETHOD(Entity, getInt), asCALL_THISCALL);
	engine->RegisterObjectMethod("Entity", "void setUInt(string,bool)", asMETHOD(Entity, setUInt), asCALL_THISCALL);
	engine->RegisterObjectMethod("Entity", "uint getUInt(string)", asMETHOD(Entity, getUInt), asCALL_THISCALL);
	engine->RegisterObjectMethod("Entity", "void setFloat(string,bool)", asMETHOD(Entity, setFloat), asCALL_THISCALL);
	engine->RegisterObjectMethod("Entity", "float getFloat(string)", asMETHOD(Entity, getFloat), asCALL_THISCALL);
	engine->RegisterObjectMethod("Entity", "void setString(string,bool)", asMETHOD(Entity, setString), asCALL_THISCALL);
	engine->RegisterObjectMethod("Entity", "string getString(string)", asMETHOD(Entity, getString), asCALL_THISCALL);
	engine->RegisterObjectMethod("Body", "Vec2 getVelocity()", asMETHOD(Body, getVelocity), asCALL_THISCALL);
	engine->RegisterObjectType("Sprite", sizeof(Sprite), asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterObjectMethod("Entity", "Sprite@ getSprite()", asMETHOD(Entity, getSprite), asCALL_THISCALL);
	engine->RegisterObjectType("SpriteLayer", sizeof(SpriteLayer), asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterObjectMethod("Sprite", "SpriteLayer@ getLayer(string)", asMETHOD(Sprite, getLayer), asCALL_THISCALL);
	engine->RegisterObjectMethod("SpriteLayer", "void flipHorizontal(bool flip=false)", asMETHOD(SpriteLayer, flipHorizontal), asCALL_THISCALL);
	engine->RegisterObjectMethod("Body", "void setVelocity(Vec2)", asMETHODPR(Body, setVelocity, (Vec2), void), asCALL_THISCALL);
	engine->RegisterObjectMethod("Body", "void setVelocity(float, float)", asMETHODPR(Body, setVelocity, (float, float), void), asCALL_THISCALL);
	//TODO: rename this, this is a terrible name for this function
	engine->RegisterObjectMethod("Engine", "bool isOtherEntityAt(Vec2, Entity@)", asMETHOD(Engine, isOtherEntityAt), asCALL_THISCALL);
	engine->RegisterObjectMethod("Entity", "void kill()", asMETHOD(Entity, kill), asCALL_THISCALL);
	engine->RegisterObjectMethod("SpriteLayer", "void setVisible(bool visible=true)", asMETHOD(SpriteLayer, setVisible), asCALL_THISCALL);
	engine->RegisterObjectType("Camera", sizeof(Camera), asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterObjectMethod("Engine", "Camera@ getCamera()", asMETHOD(Engine, getCamera), asCALL_THISCALL);
	engine->RegisterObjectMethod("Camera", "void setTarget(Entity@)", asMETHODPR(Camera, setTarget, (Entity*), void), asCALL_THISCALL);
	engine->RegisterObjectMethod("Camera", "void setLockToTarget(bool)", asMETHOD(Camera, setLockToTarget), asCALL_THISCALL);
	engine->RegisterObjectMethod("Body", "float getFriction()", asMETHOD(Body, getFriction), asCALL_THISCALL);
	engine->RegisterObjectMethod("Body", "void setFriciton(float)", asMETHOD(Body, setFriction), asCALL_THISCALL);
	engine->RegisterObjectType("Anim", sizeof(Anim), asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterObjectMethod("SpriteLayer", "Anim@ getActiveAnim()", asMETHOD(SpriteLayer, getActiveAnim), asCALL_THISCALL);
	engine->RegisterObjectMethod("Anim", "int getFrame()", asMETHOD(Anim, getFrame), asCALL_THISCALL);
	engine->RegisterObjectMethod("SpriteLayer", "bool isFlippedHorizontally()", asMETHOD(SpriteLayer, isFlippedHorizontally), asCALL_THISCALL);
	engine->RegisterObjectMethod("Entity", "string getName()", asMETHOD(Entity, getName), asCALL_THISCALL);
	engine->RegisterObjectMethod("Entity", "void setHealth(float)", asMETHOD(Entity, setHealth), asCALL_THISCALL);
	engine->RegisterObjectMethod("Entity", "float getHealth()", asMETHOD(Entity, getHealth), asCALL_THISCALL);
	engine->RegisterObjectMethod("Body", "void setAngle(float)", asMETHOD(Body, setAngle), asCALL_THISCALL);
	engine->RegisterObjectMethod("SpriteLayer", "void setFrame(int)", asMETHOD(SpriteLayer, setFrame), asCALL_THISCALL);
	engine->RegisterObjectType("GameMode", sizeof(GameMode), asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterObjectMethod("Engine", "GameMode@ getGameMode()", asMETHOD(Engine, getGameMode), asCALL_THISCALL);
	//TODO: move these out of VarManager so game mode doesn't crash everythig when you call get int
	engine->RegisterObjectMethod("GameMode", "int getInt(string)", asMETHOD(GameMode, getInt), asCALL_THISCALL);
	engine->RegisterObjectMethod("GameMode", "void setInt(string, int)", asMETHOD(GameMode, setInt), asCALL_THISCALL);



}

void messageCallBack(const asSMessageInfo* msg)
{

	bool info = false;
	std::string colorOpen = "\x1B[31m";
	if( msg->type == asMSGTYPE_WARNING )
	{
		colorOpen = "\x1B[33m";

	}
	else if( msg->type == asMSGTYPE_INFORMATION )
	{
		colorOpen = "\x1B[32m";
		info = true;

	}

	std::string message = msg->message;
	if(message.find("truncated") != std::string::npos || message.find("\'Prepare\'") != std::string::npos || (message.find("Compiling") != std::string::npos && info))
		return;

	std::string s = colorOpen + "an error occurred on line: " + toString(msg->row) + "\n\tof file: " + std::string(msg->section) + " . . . \n\t" + std::string(msg->message) +"\x1B[0m\n";
	std::cout << s << "\n";

}

}
