#ifndef SCRIPTENGINE_H_
#define SCRIPTENGINE_H_

#include <angelscript.h>

namespace styx
{

void registerEngine(asIScriptEngine* engine);

void messageCallBack(const asSMessageInfo* msg);


}

#endif /* SCRIPTENGINE_H_ */
