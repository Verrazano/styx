#ifndef CAMERA_H_
#define CAMERA_H_

#include <SFML/Graphics/View.hpp>

#include "../Util/Util.h"

namespace styx
{

class Entity;

class Camera
{
public:
	Camera();
	Camera(Vec2 size);

	Vec2 getCenter();
	void setCenter(Vec2 center);
	void setCenter(float x, float y);

	Vec2 getCenterMeters();
	void setCenterMeters(Vec2 center);
	void setCenterMeters(float x, float y);

	Vec2 getSize();
	void setSize(Vec2 size);
	void setSize(float width, float height);

	Vec2 getSizeMeters();
	void setSizeMeters(Vec2 size);
	void getSizeMeters(float width, float height);

	float getLagBehind();
	void setLagBehind(float lag);

	bool isLockedToTarget();
	void setLockToTarget(bool lock);

	void disableTarget();
	bool hasTarget();

	void setTarget(Vec2 pos);
	void setTarget(Entity* target);
	Entity* getTargetEntity();

	void update(float timeStep = 1.0f/60.0f);

	//screw converting from meters in the gui
	Vec2 getGUIViewSize();
	void setGUIViewSize(Vec2 size);
	void setGUIViewSize(float width, float height);

	sf::View getView();
	sf::View getZoomedView();
	sf::View getGUIView();

	sf::FloatRect getViewRect();
	sf::FloatRect getGUIRect();

private:
	sf::FloatRect m_viewRect;
	sf::FloatRect m_guiRect;

	float m_lag;
	bool m_lock;

	bool m_useTarget;
	Vec2 m_targetPos;
	Entity* m_target;

};

}

#endif /* CAMERA_H_ */
