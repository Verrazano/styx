#ifndef CONSOLE_H_
#define CONSOLE_H_

#include <SFML/Graphics.hpp>
#include <SFGUI/SFGUI.hpp>
#include <functional>
#include <vector>

namespace styx
{

class Console
{
public:
	Console();
	Console(sfg::Desktop& desktop);

	//use this if you created the console without a desktop object
	void update(const sf::Event& event);

	void update();

	void out(std::string msg);

	bool isShowing();
	void show(bool state = true);

	class Command
	{
	public:
		Command()
		{
			m_name = "";
			m_help = "";

		}

		Command(std::string name, std::function<bool(std::vector<std::string>)> func, std::string help)
		{
			m_name = name;
			m_func = func;
			m_help = help;

		}

		std::string m_name;
		std::function<bool(std::vector<std::string>)> m_func;
		std::string m_help;

	};

	void printHelp();
	void printHelp(std::string cmdname);
	void addCommand(Command cmd);
	Command getCommand(std::string cmdname);

private:
	void setup();

	sfg::Window::Ptr m_window;
	sfg::ScrolledWindow::Ptr m_scrollView;
	sfg::Label::Ptr m_label;
	sfg::Entry::Ptr m_entry;
	std::string m_prev;
	std::list<Command> m_commands;
	Command nosuch;

};

}

#endif /* CONSOLE_H_ */
