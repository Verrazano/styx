#include "EntityTracker.h"
#include "../../Util/Util.h"
#include "../../Util/convert/mousetoview.h"
#include "../../Game/Entity/Entity.h"
#include "../../Game/Entity/EntityType/EntityType.h"
#include "../../Game/Entity/EntityBehavior.h"
#include <iostream>
#include <algorithm>
#include <map>
#include <stdint.h>

namespace styx
{

EntityTracker::EntityTracker(sfg::Desktop& desktop, Entity* target) :
		m_desktop(&desktop),
		m_current(EntityInfo)
{
	m_open = true;
	m_target = target;
	m_window = sfg::Window::Create();
	m_window->SetTitle("Entity Tracker - " + target->getName());
	m_window->SetStyle(sfg::Window::TITLEBAR|sfg::Window::BACKGROUND|sfg::Window::SHADOW);

	auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	m_notebook = sfg::Notebook::Create();

	auto box1 = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto scrollbox1 = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto scroll1 = sfg::ScrolledWindow::Create();
	scroll1->SetScrollbarPolicy(sfg::ScrolledWindow::HORIZONTAL_AUTOMATIC|sfg::ScrolledWindow::VERTICAL_AUTOMATIC);
	scroll1->AddWithViewport(scrollbox1);
	scroll1->SetRequisition(sf::Vector2f(200.0f, 300.0f));
	m_entityInfo = sfg::Label::Create();
	m_entityInfo->SetAlignment(sf::Vector2f(0, 0));
	m_entityInfo->SetPosition(sf::Vector2f(0, 0));
	scrollbox1->Pack(m_entityInfo);
	box1->Pack(scroll1);

	auto box2 = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto scrollbox2 = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto scroll2 = sfg::ScrolledWindow::Create();
	scroll2->SetScrollbarPolicy(sfg::ScrolledWindow::HORIZONTAL_AUTOMATIC|sfg::ScrolledWindow::VERTICAL_AUTOMATIC);
	scroll2->AddWithViewport(scrollbox2);
	scroll2->SetRequisition(sf::Vector2f(200.0f, 300.0f));
	m_box2DInfo = sfg::Label::Create();
	m_box2DInfo->SetPosition(sf::Vector2f(0, 0));
	m_box2DInfo->SetAlignment(sf::Vector2f(0, 0));
	scrollbox2->Pack(m_box2DInfo);
	box2->Pack(scroll2);

	auto box3 = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto scrollbox3 = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto scroll3 = sfg::ScrolledWindow::Create();
	scroll3->SetScrollbarPolicy(sfg::ScrolledWindow::HORIZONTAL_AUTOMATIC|sfg::ScrolledWindow::VERTICAL_AUTOMATIC);
	scroll3->AddWithViewport(scrollbox3);
	scroll3->SetRequisition(sf::Vector2f(200.0f, 300.0f));
	m_varManagerInfo = sfg::Label::Create();
	m_varManagerInfo->SetPosition(sf::Vector2f(0, 0));
	m_varManagerInfo->SetAlignment(sf::Vector2f(0, 0));
	scrollbox3->Pack(m_varManagerInfo);
	box3->Pack(scroll3);

	m_notebook->AppendPage(box1, sfg::Label::Create("Entity"));
	m_notebook->AppendPage(box2, sfg::Label::Create("Box2D"));
	m_notebook->AppendPage(box3, sfg::Label::Create("VarManager"));
	m_notebook->SetRequisition(sf::Vector2f(200.0f, 300.0f));

	m_notebook->GetSignal(sfg::Widget::OnLeftClick).Connect(
			[this]()
			{
				int page = (int)m_notebook->GetCurrentPage();
				if(page == 2)
					m_current = VarInfo;
				else if(page == 1)
					m_current = Box2DInfo;
				else
					m_current = EntityInfo;

			});
	box->Pack(m_notebook, false);
	auto button = sfg::Button::Create("Close");
	button->GetSignal(sfg::Widget::OnLeftClick).Connect(
			[this]()
			{
				this->close();

			});

	box->Pack(button, false, false);
	m_window->Add(box);
	desktop.Add(m_window);

}

EntityTracker::Ptr EntityTracker::create(sfg::Desktop& desktop, Entity* target)
{
	return std::make_shared<EntityTracker>(desktop, target);

}

EntityTracker::~EntityTracker()
{
	m_desktop->Remove(m_window);

}

Entity* EntityTracker::getTarget()
{
	return m_target;

}

void EntityTracker::update()
{
	Body& body = m_target->getBody();
	float angle = body.getAngle();
	if(m_current == EntityInfo)
	{
		std::string info;
		info = "type: " + m_target->getName() + "\n";
		info += "id: " + m_target->getIDString() + "\n";
		info += "pos: " + body.getPosition().toString() + "\n";
		info += "angle: " + toString(angle) + "\n";
		info += "health: " + toString(m_target->getHealth()) + "\n";

		Sprite& sprite  = m_target->getSprite();
		info += "z: " + toString(sprite.getZ()) + "\n";

		info += "\nscripts:\n";
		std::vector<EntityBehavior::Ptr>& behaviors = m_target->getBehaviors();
		for(unsigned int i = 0; i < behaviors.size(); i++)
		{
			info += "  " + behaviors[i]->getTitle() + "\n";

		}

		if(cfgStr == "")
		{
			cfgStr = "\ncfg:\n";
			std::string cfg  = m_target->getType()->getFile();
			std::replace(cfg.begin(), cfg.end(), ';', '\n');
			cfgStr += cfg;

		}

		info += cfgStr;

		m_entityInfo->SetText(info);

	}
	else if(m_current == Box2DInfo)
	{
		std::string info;

		info = "pos: " + body.getPosition().toString() + "\n";
		info += "vel: " + body.getVelocity().toString() + "\n";
		info += "fixedRotation: " + toString(body.isFixedRotation()) + "\n";
		info += "angle: " + toString(angle) + "\n";
		info += "angularVel: " + toString(body.getAngularVelocity()) + "\n";
		info += "friction: " + toString(body.getFriction()) + "\n";
		info += "maxSpeed: " + toString(body.getMaxSpeed()) + "\n";
		info += "weight: " + toString(body.getMass()) + " kg\n";
		info += "bouncyness: " + toString(body.getBouncyness()) + "\n";
		info += "isDynamic: " + toString(body.isDynamic()) + "\n";
		info += "isBullet: " + toString(body.isBullet()) + "\n";
		info += "linearDamping: " + toString(body.getLinearDamping()) + "\n";
		info += "angularDamping: " + toString(body.getAngularDamping()) + "\n";
		m_box2DInfo->SetText(info);

	}
	else
	{
		std::string varInfo = "";
		std::vector<std::pair<std::string, unsigned int> >& uints = m_target->getUInts();
		std::vector<std::pair<std::string, int> >& ints = m_target->getInts();
		std::vector<std::pair<std::string, float> >& floats = m_target->getFloats();
		std::vector<std::pair<std::string, std::string> >& strings = m_target->getStrings();
		std::vector<std::pair<std::string, bool> >& bools = m_target->getBools();

		for(auto val : uints)
		{
			varInfo += val.first + " = " + toString(val.second) + "\n";

		}
		for(auto val : ints)
		{
			varInfo += val.first + " = " + toString(val.second) + "\n";

		}
		for(auto val : floats)
		{
			varInfo += val.first + " = " + toString(val.second) + "\n";

		}
		for(auto val : strings)
		{
			varInfo += val.first + " = " + val.second + "\n";

		}
		for(auto val : bools)
		{
			varInfo += val.first + " = " + toString(val.second) + "\n";

		}

		m_varManagerInfo->SetText(varInfo);

	}

}

bool EntityTracker::isOpen()
{
	return m_open;

}

void EntityTracker::close()
{
	m_open = false;

}

void EntityTracker::draw(sf::RenderWindow& window, sf::Vector2f offset)
{
	sf::VertexArray line(sf::Lines, 2);
	sf::FloatRect rect = m_window->GetAllocation();
	sf::Vector2f pos = m_window->GetAbsolutePosition();
	pos.x += rect.width/2;
	pos.y += rect.height/2;
	line[0] = sf::Vertex(pos, sf::Color::Green);
	sf::Vector2f p = toScreen(m_target->getBody().getPosition().to<sf::Vector2f>());
	p.x -= offset.x;
	p.y -= offset.y;
	line[1] = sf::Vertex(p, sf::Color::Green);
	sf::CircleShape shape(30);
	shape.setOutlineColor(sf::Color::Green);
	shape.setOutlineThickness(2);
	shape.setFillColor(sf::Color::Transparent);
	shape.setOrigin(30, 30);
	shape.setPosition(p);

	window.draw(line);
	window.draw(shape);
	window.draw(line);


}

}
