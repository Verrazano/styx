#include "Console.h"

namespace styx
{

Console::Console()
{
	setup();

}

Console::Console(sfg::Desktop& desktop)
{
	setup();
	desktop.Add(m_window);

}

void Console::update(const sf::Event& event)
{
	m_window->HandleEvent(event);
	m_window->Update(1.0f/60.0f);
	update();

}

void Console::update()
{
	if(isShowing())
	{
		if(m_entry->HasFocus())
		{
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
			{
				std::string msg = m_entry->GetText();
				if(msg != "")
				{
					out(msg);

					std::string tmsg;
					std::string cmdname;
					std::vector<std::string> params;
					tmsg = msg;
					cmdname = tmsg;
					if(tmsg.find(" ") != std::string::npos)
					{
						cmdname = tmsg.substr(0, tmsg.find(" "));
						tmsg = tmsg.substr(tmsg.find(" ")+1);

					}

					while(tmsg.find(" ") != std::string::npos)
					{
						params.push_back(tmsg.substr(0, tmsg.find(" ")));
						tmsg = tmsg.substr(tmsg.find(" ")+1);

					}

					if(tmsg != cmdname && tmsg != "" && tmsg != " ")
						params.push_back(tmsg);

					Command cmd = getCommand(cmdname);
					if(!cmd.m_func(params))
					{
						out("\t"+cmd.m_help);

					}

					m_entry->SetText("");
					m_entry->GrabFocus();
					m_prev = msg;

				}

			}
			else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{
				m_entry->SetText(m_prev);
				m_entry->GrabFocus();

			}

		}

	}

}

void Console::out(std::string msg)
{
	if(m_label->GetText() == "")
	{
		m_label->SetText(msg);

	}
	else
	{
		m_label->SetText(m_label->GetText() + "\n" + msg);
		m_scrollView->GetVerticalAdjustment()->SetValue(m_scrollView->GetVerticalAdjustment()->GetUpper());

	}

}

bool Console::isShowing()
{
	return m_window->IsLocallyVisible();

}

void Console::show(bool state)
{
	if(state)
	{
		m_window->Show();
		m_entry->GrabFocus();

	}
	else
	{
		m_window->Show(false);

	}

}

void Console::printHelp()
{
	out("Commands:");
	out("====================");
	for(auto cmd : m_commands)
	{
		out(cmd.m_name);
		out("\t"+cmd.m_help);

	}
	out("====================");

}

void Console::printHelp(std::string cmdname)
{
	for(auto cmd : m_commands)
	{
		if(cmd.m_name == cmdname)
		{
			out("\t"+cmd.m_help);
			return;

		}

	}

}

void Console::addCommand(Command cmd)
{
	m_commands.push_back(cmd);

}

Console::Command Console::getCommand(std::string cmdname)
{
	for(auto cmd : m_commands)
	{
		if(cmd.m_name == cmdname)
		{
			return cmd;

		}

	}

	return nosuch;


}

void Console::setup()
{
	m_window = sfg::Window::Create();
	m_window->SetTitle("Console");
	m_window->Show(false);
	m_window->SetStyle(sfg::Window::TOPLEVEL|sfg::Window::SHADOW);

	auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 10.f);
	auto scrolledBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	m_scrollView = sfg::ScrolledWindow::Create();
	m_scrollView->SetScrollbarPolicy(sfg::ScrolledWindow::HORIZONTAL_AUTOMATIC|sfg::ScrolledWindow::VERTICAL_ALWAYS);
	m_scrollView->AddWithViewport(scrolledBox);
	m_scrollView->SetRequisition(sf::Vector2f(80*7, (50*7)-25));

	m_entry = sfg::Entry::Create();
	m_entry->SetAllocation(sf::FloatRect(0, 0, 80*7, 25));
	m_label = sfg::Label::Create();
	m_label->SetAlignment(sf::Vector2f(0, 0));

	scrolledBox->Pack(m_label);
	box->Pack(m_scrollView);
	box->Pack(m_entry, false, true);

	m_window->Add(box);
	m_window->SetAllocation(sf::FloatRect(0, 0, 80*7, 50*7));

	addCommand(Command("help",
			[this](std::vector<std::string> params)
			{
				if(params.size() == 0)
				{
					this->printHelp();

				}
				else
				{
					for(unsigned int i = 0; i < params.size(); i++)
					{
						this->printHelp(params[i]);

					}

				}

				return true;

			},"help [*command]\t-\tPrints the help for a specific command."));

	nosuch = Command("",
			[this](std::vector<std::string> params)
			{
				if(params.size() != 0)
				{
					this->out("no such command, \"" + params[0] + "\".");
					return true;

				}

				this->out("no such command.");

				return true;

			}, "");

}

}
