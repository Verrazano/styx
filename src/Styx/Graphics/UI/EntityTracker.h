#ifndef ENTITYTRACKER_H_
#define ENTITYTRACKER_H_

#include <SFML/Graphics.hpp>
#include <SFGUI/SFGUI.hpp>

namespace styx
{

class Entity;

class EntityTracker
{
public:
	EntityTracker(sfg::Desktop& desktop, Entity* target);

	typedef std::shared_ptr<EntityTracker> Ptr;
	static Ptr create(sfg::Desktop& desktop, Entity* target);

	~EntityTracker();

	Entity* getTarget();

	void update();

	bool isOpen();
	void close();

	void draw(sf::RenderWindow& window, sf::Vector2f offset);

	typedef enum Active
	{
		EntityInfo = 0,
		Box2DInfo,
		VarInfo

	} ActivePanel;

private:
	Entity* m_target;
	sfg::Window::Ptr m_window;
	sfg::Label::Ptr m_entityInfo;
	sfg::Label::Ptr m_box2DInfo;
	sfg::Label::Ptr m_varManagerInfo;
	sfg::Notebook::Ptr m_notebook;
	bool m_open;
	sfg::Desktop* m_desktop;
	ActivePanel m_current;
	std::string cfgStr;

};

}

#endif /* ENTITYTRACKER_H_ */
