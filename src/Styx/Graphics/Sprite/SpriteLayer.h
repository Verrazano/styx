#ifndef SPRITELAYER_H_
#define SPRITELAYER_H_

#include <string>
#include <SFML/Graphics.hpp>

#include "Anim.h"
#include "../../Util/Util.h"

namespace styx
{

class Sprite;

class SpriteLayer : public Transformable
{
public:
	SpriteLayer(Sprite& parent, std::string name);
	SpriteLayer(Sprite& parent, std::string name, std::string textureName, sf::Texture* texture);
	SpriteLayer(Sprite& parent, std::string name, std::string textureName, sf::Texture* texture, Vec2 frameSize);

	std::string getName();
	std::string getTextureName();

	void setTexture(std::string textureName, bool resetSize = true, bool resetOrigin = true);

	Vec2 getSize();
	void setSize(Vec2 size);
	void setSize(float x, float y);

	float getRelativeZ();
	void setRelativeZ(float z);

	Vec2 getFrameSize();
	void setFrameSize(Vec2 frameSize, bool resetOrigin = true);
	void setFrameSize(int width, int height, bool resetOrigin = true);

	void setFrame(int frame);

	void draw(sf::RenderTarget& target, sf::RenderStates states);

	bool isFlippedVertically();
	void flipVertical(bool flip = false);

	bool isFlippedHorizontally();
	void flipHorizontal(bool flip = false);

	bool isVisible();
	void setVisible(bool visible = true);

	bool shouldDraw(sf::FloatRect rect);

	Anim& addAnim(std::string name, Vec2 frameSize);
	Anim& addAnim(std::string name, Vec2 frameSize, float speed, std::vector<int> frames);

	Anim* getAnim(std::string name);
	void remAnim(std::string name);
	bool hasAnim(std::string name);

	void setActiveAnim(std::string name);
	Anim& getActiveAnim();

	Vec2 getFrameOffset();
	void setFrameOffset(Vec2 offset);

private:
	Sprite* m_parent;
	std::string m_name;
	std::string m_textureName;
	sf::RectangleShape m_inter;
	float m_relativeZ;
	bool m_visible;
	Vec2 m_frameSize;

	std::vector<Anim> m_anims;
	Anim m_activeAnim;
	Vec2 m_offset;

	friend Sprite;

};

}

#endif /* SPRITELAYER_H_ */
