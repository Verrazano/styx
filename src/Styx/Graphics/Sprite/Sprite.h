#ifndef SPRITE_H_
#define SPRITE_H_

#include <string>
#include <vector>
#include <SFML/Graphics.hpp>
#include <functional>

#include "SpriteLayer.h"
#include "../../Util/Util.h"
#include "../../Util/ResourceManager/ResourceManager.h"

#include "../Camera.h"

namespace styx
{

class Sprite : public Transformable
{
public:
	Sprite(Sprite& copy);
	Sprite(ResourceManager& resourceManager);

	SpriteLayer& addLayer(std::string name);
	SpriteLayer& addLayer(std::string name, std::string textureName);
	SpriteLayer& addLayer(std::string name, std::string textureName, Vec2 frameSize);

	SpriteLayer* getLayer(std::string name);
	void remLayer(std::string name);
	bool hasLayer(std::string name);

	float getZ();
	void setZ(float z);
	void setZOrderCallback(std::function<void(void)> zOrderChange);

	bool shouldDraw(Camera& camera);

	void draw(sf::RenderTarget& target, sf::RenderStates states = sf::RenderStates::Default);

private:
	void order();

	std::vector<SpriteLayer> m_layers;
	float m_z;

	ResourceManager& m_resourceManager;

	std::function<void()> m_zOrderChange;

	friend SpriteLayer;

};

}

#endif /* SPRITE_H_ */
