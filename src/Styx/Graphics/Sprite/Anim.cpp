#include "Anim.h"
#include "SpriteLayer.h"

namespace styx
{

Anim::Anim() :
		m_speed(0.2),
		m_index(0),
		m_paused(true)
{
}

Anim::Anim(std::string name, Vec2 frameSize) :
		m_name(name),
		m_frameSize(frameSize),
		m_speed(0.2),
		m_index(0),
		m_paused(false)
{
}

Anim::Anim(std::string name, Vec2 frameSize, float speed, std::vector<int> frames) :
		m_name(name),
		m_frameSize(frameSize),
		m_speed(speed),
		m_frames(frames),
		m_index(0),
		m_paused(false)
{
}

std::string Anim::getName()
{
	return m_name;

}

Vec2 Anim::getFrameSize()
{
	return m_frameSize;

}

void Anim::setFrameSize(Vec2 frameSize)
{
	m_frameSize = frameSize;

}

void Anim::addFrame(int frame)
{
	m_frames.push_back(frame);

}

float Anim::getSpeed()
{
	return m_speed;

}

void Anim::setSpeed(float speed)
{
	m_speed = speed;

}

int Anim::getFrame()
{
	if(m_index >= m_frames.size())
	{
		return 0;

	}

	return m_frames[m_index];

}

int Anim::getIndex()
{
	return m_index;

}

void Anim::setFrame(int index)
{
	m_index = index;
	resetTimer();

}

void Anim::resetTimer()
{
	m_timer.restart();

}

void Anim::pause()
{
	m_paused = true;

}

void Anim::unpause()
{
	m_paused = false;

}

bool Anim::isPaused()
{
	return m_paused;

}

void Anim::update(SpriteLayer& layer)
{
	if(m_paused)
		return;

	if(m_timer.getElapsedTime().asSeconds() > m_speed)
	{
		layer.setFrameSize(m_frameSize);
		m_index++;
		m_index = m_index%m_frames.size();
		int frame = getFrame();
		layer.setFrame(frame);
		m_timer.restart();

	}

}

}
