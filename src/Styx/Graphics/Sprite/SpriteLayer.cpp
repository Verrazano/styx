#include "SpriteLayer.h"
#include "Sprite.h"

namespace styx
{

SpriteLayer::SpriteLayer(Sprite& parent, std::string name) :
		m_parent(&parent),
		m_name(name),
		m_relativeZ(0.0f),
		m_visible(true)
{
}

SpriteLayer::SpriteLayer(Sprite& parent, std::string name, std::string textureName, sf::Texture* texture) :
		m_parent(&parent),
		m_name(name),
		m_textureName(textureName),
		m_relativeZ(0.0f),
		m_visible(true)
{
	if(texture != NULL)
	{
		m_inter.setTexture(texture);
		setSize(texture->getSize());
		setFrameSize(getSize());
		setOrigin(getSize()/2);

	}

}

SpriteLayer::SpriteLayer(Sprite& parent, std::string name, std::string textureName, sf::Texture* texture, Vec2 frameSize) :
		m_parent(&parent),
		m_name(name),
		m_textureName(textureName),
		m_relativeZ(0.0f),
		m_visible(true)
{
	m_inter.setTexture(texture);
	setSize(frameSize);
	setFrameSize(frameSize);
	setOrigin(frameSize/2);

}

std::string SpriteLayer::getName()
{
	return m_name;

}

std::string SpriteLayer::getTextureName()
{
	return m_textureName;

}

void SpriteLayer::setTexture(std::string textureName, bool resetSize, bool resetOrigin)
{
	ResourceManager& resourceManager = m_parent->m_resourceManager;
	sf::Texture* texture = resourceManager.get<sf::Texture>(textureName);
	if(texture != NULL)
	{
		m_textureName = textureName;
		m_inter.setTexture(texture);

		if(resetSize)
		{
			setSize(texture->getSize());
			setFrameSize(getSize());

		}

		if(resetOrigin)
		{
			setOrigin((getSize()/2).to<sf::Vector2f>());

		}

	}

}

Vec2 SpriteLayer::getSize()
{
	return m_inter.getSize();

}

void SpriteLayer::setSize(Vec2 size)
{
	m_inter.setSize(size.to<sf::Vector2f>());

}

void SpriteLayer::setSize(float x, float y)
{
	setSize(Vec2(x, y));
	setOrigin(getSize()/2);

}

float SpriteLayer::getRelativeZ()
{
	return m_relativeZ;

}

void SpriteLayer::setRelativeZ(float z)
{
	if(m_relativeZ != z)
	{
		m_relativeZ = z;
		m_parent->order();

	}

}

Vec2 SpriteLayer::getFrameSize()
{
	return m_frameSize;

}

void SpriteLayer::setFrameSize(Vec2 frameSize, bool resetOrigin)
{
	m_frameSize = frameSize.truncate();
	setFrame(0);

	if(resetOrigin)
	{
		setOrigin(m_frameSize/2);

	}

}

void SpriteLayer::setFrameSize(int width, int height, bool resetOrigin)
{
	m_frameSize = Vec2(width, height);
	setFrame(0);

	if(resetOrigin)
	{
		setOrigin(m_frameSize/2);

	}

}

void SpriteLayer::setFrame(int frame)
{
	const sf::Texture* texture = m_inter.getTexture();
	if(texture == NULL)
	{
		return;

	}
	Vec2 textrueSize = texture->getSize();
	if(m_frameSize.x == 0 || m_frameSize.y == 0)
	{
		return;

	}

	unsigned int x = textrueSize.x/m_frameSize.x;
	unsigned int y = textrueSize.y/m_frameSize.y;

	if(x == 0 || y == 0)
	{
		return;

	}

	unsigned int frames = x*y;

	if(frame > frames)
	{
		sf::IntRect rect(0, 0, m_frameSize.x, m_frameSize.y);
		m_inter.setTextureRect(rect);
		return;

	}

	unsigned int fx = frame%x;
	unsigned int fy = frame/x;
	fx *= m_frameSize.x;
	fy *= m_frameSize.y;

	fx += m_offset.x;
	fy += m_offset.y;

	sf::IntRect rect(fx, fy, m_frameSize.x, m_frameSize.y);
	m_inter.setTextureRect(rect);

}

void SpriteLayer::draw(sf::RenderTarget& target, sf::RenderStates states)
{
	m_activeAnim.update((*this));
	states.transform *= getTransform();
	target.draw(m_inter, states);

}

bool SpriteLayer::isFlippedVertically()
{
	return getScale().y == -1;

}

void SpriteLayer::flipVertical(bool flip)
{
	float factor = flip ? -1 : 1;
	setScale(Vec2(getScale().x, factor));

}

bool SpriteLayer::isFlippedHorizontally()
{
	return getScale().x == -1;

}

void SpriteLayer::flipHorizontal(bool flip)
{
	float factor = flip ? -1 : 1;
	setScale(Vec2(factor, getScale().y));

}

bool SpriteLayer::isVisible()
{
	return m_visible;

}

void SpriteLayer::setVisible(bool visible)
{
	m_visible = visible;

}

bool SpriteLayer::shouldDraw(sf::FloatRect rect)
{
	if(!isVisible())
	{
		return false;

	}

	Vec2 offset = m_parent->getPosition();
	Vec2 parentOrigin = m_parent->getOrigin();
	Vec2 origin = getOrigin();
	sf::FloatRect interBounds = m_inter.getGlobalBounds();
	interBounds.left += offset.x - origin.x - parentOrigin.x;
	interBounds.top += offset.y - origin.y - parentOrigin.y;

	if(interBounds.intersects(rect))
	{
		return true;

	}

	return false;

}

Anim& SpriteLayer::addAnim(std::string name, Vec2 frameSize)
{
	Anim* anim = getAnim(name);
	if(anim == NULL)
	{
		m_anims.push_back(Anim(name, frameSize));
		return m_anims[m_anims.size()-1];

	}

	return (*anim);

}

Anim& SpriteLayer::addAnim(std::string name, Vec2 frameSize, float speed, std::vector<int> frames)
{
	Anim* anim = getAnim(name);
	if(anim == NULL)
	{
		m_anims.push_back(Anim(name, frameSize, speed, frames));
		return m_anims[m_anims.size()-1];

	}

	return (*anim);

}

Anim* SpriteLayer::getAnim(std::string name)
{
	for(unsigned int i = 0; i < m_anims.size(); i++)
	{
		if(m_anims[i].getName() == name)
		{
			return &m_anims[i];

		}

	}

	return NULL;

}

void SpriteLayer::remAnim(std::string name)
{
	std::vector<Anim>::iterator it;
	for(it = m_anims.begin(); it != m_anims.end(); ++it)
	{
		if(it->getName() == name)
		{
			m_anims.erase(it);
			return;

		}

	}

}

bool SpriteLayer::hasAnim(std::string name)
{
	return getAnim(name) != NULL;

}

void SpriteLayer::setActiveAnim(std::string name)
{
	Anim* anim = getAnim(name);
	if(anim != NULL)
	{
		Anim temp = Anim((*anim));
		for(unsigned int i = 0; i < m_anims.size(); i++)
		{
			if(m_anims[i].getName() == name)
			{
				m_anims[i] = m_activeAnim;

			}

		}
		m_activeAnim = temp;
		return;

	}

	m_activeAnim = Anim();

}

Anim& SpriteLayer::getActiveAnim()
{
	return m_activeAnim;

}

Vec2 SpriteLayer::getFrameOffset()
{
	return m_offset;

}

void SpriteLayer::setFrameOffset(Vec2 offset)
{
	m_offset = offset;

}

}
