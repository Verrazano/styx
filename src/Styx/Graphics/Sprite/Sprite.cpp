#include "Sprite.h"
#include <algorithm>

namespace styx
{

Sprite::Sprite(Sprite& copy) :
	m_z(copy.m_z),
	m_resourceManager(copy.m_resourceManager),
	m_zOrderChange([](){}) //#worstlambdaever
{
	m_layers.reserve(10);
	m_layers = copy.m_layers;

	for(unsigned int i = 0; i < m_layers.size(); i++)
	{
		m_layers[i].m_parent = this;

	}

}

Sprite::Sprite(ResourceManager& resourceManager) :
		m_z(0),
		m_resourceManager(resourceManager),
		m_zOrderChange([](){}) //#worstlambdaever
{
	m_layers.reserve(10);

}

SpriteLayer& Sprite::addLayer(std::string name)
{
	SpriteLayer* layer = getLayer(name);
	if(layer == NULL)
	{
		m_layers.push_back(SpriteLayer((*this), name));
		return m_layers[m_layers.size()-1];

	}

	return (*layer);

}

SpriteLayer& Sprite::addLayer(std::string name, std::string textureName)
{
	SpriteLayer* layer = getLayer(name);
	if(layer == NULL)
	{
		sf::Texture* texture = m_resourceManager.get<sf::Texture>(textureName);
		if(texture != NULL)
		{
			m_layers.push_back(SpriteLayer((*this), name, textureName, texture));
			return m_layers[m_layers.size()-1];

		}

	}

	return (*layer);

}

SpriteLayer& Sprite::addLayer(std::string name, std::string textureName, Vec2 frameSize)
{
	SpriteLayer* layer = getLayer(name);
	if(layer == NULL)
	{
		sf::Texture* texture = m_resourceManager.get<sf::Texture>(textureName);
		if(texture != NULL)
		{
			m_layers.push_back(SpriteLayer((*this), name, textureName, texture, frameSize));
			return m_layers[m_layers.size()-1];

		}

	}

	return (*layer);

}

SpriteLayer* Sprite::getLayer(std::string name)
{
	for(unsigned int i = 0; i < m_layers.size(); i++)
	{
		if(m_layers[i].getName() == name)
		{
			return &m_layers[i];

		}

	}

	return NULL;

}

void Sprite::remLayer(std::string name)
{
	std::vector<SpriteLayer>::iterator it;
	for(it = m_layers.begin(); it != m_layers.end(); ++it)
	{
		if(it->getName() == name)
		{
			m_layers.erase(it);
			return;

		}

	}

}

bool Sprite::hasLayer(std::string name)
{
	return getLayer(name) != NULL;

}

float Sprite::getZ()
{
	return m_z;

}

void Sprite::setZ(float z)
{
	if(m_z != z)
	{
		m_z = z;
		m_zOrderChange();

	}

}

void Sprite::setZOrderCallback(std::function<void(void)> zOrderChange)
{
	m_zOrderChange = zOrderChange;

}

bool Sprite::shouldDraw(Camera& camera)
{
	for(unsigned int i = 0; i < m_layers.size(); i++)
	{
		bool state = m_layers[i].shouldDraw(camera.getViewRect());
		if(state)
		{
			return true;

		}

	}

	return false;

}

void Sprite::draw(sf::RenderTarget& target, sf::RenderStates states)
{
	states.transform *= getTransform();
	for(unsigned int i = 0; i < m_layers.size(); i++)
	{
		if(m_layers[i].isVisible())
		{
			m_layers[i].draw(target, states);

		}

	}

}

void Sprite::order()
{
	std::stable_sort(m_layers.begin(), m_layers.end(), [](SpriteLayer lhs, SpriteLayer rhs){return lhs.m_relativeZ < rhs.m_relativeZ;});

}

}
