#ifndef ANIM_H_
#define ANIM_H_

#include <SFML/System/Clock.hpp>

#include "../../Util/Util.h"

namespace styx
{

class SpriteLayer;

class Anim
{
public:
	Anim();
	Anim(std::string name, Vec2 frameSize);
	Anim(std::string name, Vec2 frameSize, float speed, std::vector<int> frames);

	std::string getName();

	Vec2 getFrameSize();
	void setFrameSize(Vec2 frameSize);

	void addFrame(int frame);

	float getSpeed();
	void setSpeed(float speed);

	int getFrame();
	int getIndex();
	void setFrame(int index);

	void resetTimer();

	void pause();
	void unpause();

	bool isPaused();

	void update(SpriteLayer& layer);

private:
	std::string m_name;
	Vec2 m_frameSize;
	sf::Clock m_timer;
	float m_speed;
	std::vector<int> m_frames;
	int m_index;
	bool m_paused;

};

}

#endif /* ANIM_H_ */
