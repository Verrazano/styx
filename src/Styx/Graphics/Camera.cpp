#include "Camera.h"
#include "../Game/Entity/Entity.h"

namespace styx
{

Camera::Camera() :
		m_lag(10.0),
		m_lock(0),
		m_useTarget(false),
		m_target(NULL)
{
	setSize(800, 600);
	setGUIViewSize(800, 600);

}

Camera::Camera(Vec2 size) :
		m_lag(10.0),
		m_lock(0),
		m_useTarget(false),
		m_target(NULL)
{
	setSize(size);
	setGUIViewSize(size);

}

Vec2 Camera::getCenter()
{
	return Vec2(m_viewRect.left + m_viewRect.width/2,
			m_viewRect.top + m_viewRect.height/2);

}

void Camera::setCenter(Vec2 center)
{
	m_viewRect.left = center.x - m_viewRect.width/2;
	m_viewRect.top = center.y - m_viewRect.height/2;

}

void Camera::setCenter(float x, float y)
{
	setCenter(Vec2(x, y));

}

Vec2 Camera::getCenterMeters()
{
	return toWorld(getCenter());

}

void Camera::setCenterMeters(Vec2 center)
{
	setCenter(toScreen(center));

}
void Camera::setCenterMeters(float x, float y)
{
	setCenterMeters(Vec2(x, y));

}

Vec2 Camera::getSize()
{
	return Vec2(m_viewRect.width, m_viewRect.height);

}

void Camera::setSize(Vec2 size)
{
	m_viewRect.width = size.x;
	m_viewRect.height = size.y;

}

void Camera::setSize(float width, float height)
{
	setSize(Vec2(width, height));

}

Vec2 Camera::getSizeMeters()
{
	return toWorld(getSize());

}

void Camera::setSizeMeters(Vec2 size)
{
	setSize(toScreen(size));

}

void Camera::getSizeMeters(float width, float height)
{
	setSizeMeters(Vec2(width, height));

}

float Camera::getLagBehind()
{
	return m_lag;

}

void Camera::setLagBehind(float lag)
{
	if(lag == 0.0f)
	{
		lag = 0.1f;

	}

	m_lag = lag;

}

bool Camera::isLockedToTarget()
{
	return m_lock;

}

void Camera::setLockToTarget(bool lock)
{
	m_lock = lock;

}

bool Camera::hasTarget()
{
	return m_useTarget;

}

void Camera::disableTarget()
{
	m_useTarget = false;
	m_target = NULL;
	m_targetPos = Vec2(0, 0);

}

void Camera::setTarget(Vec2 pos)
{
	m_targetPos = pos;
	m_useTarget = true;
	m_target = NULL;

}

void Camera::setTarget(Entity* target)
{
	m_target = target;
	m_useTarget = true;

}

Entity* Camera::getTargetEntity()
{
	return m_target;

}

void Camera::update(float timeStep)
{
	if(!m_useTarget)
	{
		return;

	}

	if(m_target != NULL)
	{
		m_targetPos = toScreen(m_target->getBody().getPosition());

	}

	if(m_lock)
	{
		setCenter(m_targetPos);

	}
	else
	{
		Vec2 diff = m_targetPos - getCenter();
		if(diff.length() <= 10.0)
		{
			setCenter(m_targetPos);

		}
		diff.x *= timeStep * m_lag;
		diff.y *= timeStep * m_lag;
		m_viewRect.left += diff.x;
		m_viewRect.top += diff.y;

	}


}

Vec2 Camera::getGUIViewSize()
{
	return Vec2(m_guiRect.width, m_guiRect.height);

}

void Camera::setGUIViewSize(Vec2 size)
{
	m_guiRect.width = size.x;
	m_guiRect.height = size.y;

}

void Camera::setGUIViewSize(float width, float height)
{
	setGUIViewSize(Vec2(width, height));

}

sf::View Camera::getView()
{
	sf::View view(m_viewRect);
	return sf::View(m_viewRect);

}

sf::View Camera::getZoomedView()
{
	sf::FloatRect rect = m_viewRect;
	rect.left -= 100;
	rect.top -= 100;
	rect.width += 200;
	rect.height += 200;
	return sf::View(rect);

}

sf::View Camera::getGUIView()
{
	return sf::View(m_guiRect);

}

sf::FloatRect Camera::getViewRect()
{
	return m_viewRect;

}

sf::FloatRect Camera::getGUIRect()
{
	return m_guiRect;

}

}
