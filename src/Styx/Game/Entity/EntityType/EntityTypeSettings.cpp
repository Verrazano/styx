#include "EntityTypeSettings.h"

namespace styx
{

EntityTypeSettings::EntityTypeSettings(World& world) :
		bodyDef(world)
{
	name = "";
	maxSpeed = 1000;
	health = 100;
	z = 1;
	mass = -1.0f;

	add("z", offsetof(EntityTypeSettings, z), z);
	add("health", offsetof(EntityTypeSettings, health), health);
	add("name", offsetof(EntityTypeSettings, name), name);
	add("scripts", offsetof(EntityTypeSettings, scripts), scripts);
	add("maxSpeed", offsetof(EntityTypeSettings, maxSpeed), maxSpeed);

	//BODYDEF stuff
	add("isDynamic", offsetof(EntityTypeSettings, bodyDef.isDynamic), bodyDef.isDynamic);
	add("linearDamping", offsetof(EntityTypeSettings, bodyDef.linearDamping), bodyDef.linearDamping);
	add("angularDamping", offsetof(EntityTypeSettings, bodyDef.angularDamping), bodyDef.angularDamping);
	add("isBullet", offsetof(EntityTypeSettings, bodyDef.isBullet), bodyDef.isBullet);
	add("fixedRotation", offsetof(EntityTypeSettings, bodyDef.fixedRotation), bodyDef.fixedRotation);
	add("sensor", offsetof(EntityTypeSettings, bodyDef.sensor), bodyDef.sensor);
	add("bouncyness", offsetof(EntityTypeSettings, bodyDef.bouncyness), bodyDef.bouncyness);
	add("friction", offsetof(EntityTypeSettings, bodyDef.friction), bodyDef.friction);
	add("gravityScale", offsetof(EntityTypeSettings, bodyDef.gravityScale), bodyDef.gravityScale);
	add("mass", offsetof(EntityTypeSettings, mass), mass);

}

}
