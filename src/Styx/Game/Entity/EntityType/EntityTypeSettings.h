#ifndef ENTITYTYPESETTINGS_H_
#define ENTITYTYPESETTINGS_H_

#include "../../../Physics/Physics.h"
#include "../../../Util/Settings/Settings.h"

namespace styx
{

class EntityTypeSettings : public Settings
{
public:
	EntityTypeSettings(World& world);

	std::string name;
	BodyDef bodyDef;
	std::vector<std::string> scripts;
	float maxSpeed;
	float mass;
	float health;
	float z;

};

}

#endif /* ENTITYTYPESETTINGS_H_ */
