#ifndef ENTITYTYPE_H_
#define ENTITYTYPE_H_

#include "../../../Physics/Physics.h"
#include "EntityTypeSettings.h"
#include "../../../Graphics/Graphics.h"
#include <memory>

namespace styx
{

class Engine;
class EntityBehavior;

class EntityType
{
public:
	EntityType(Engine* engine,
			std::string file);

	typedef std::shared_ptr<EntityType> Ptr;
	static Ptr create(Engine* engine, std::string file);

	~EntityType();

	Engine* getEngine();
	std::string& getFile();
	std::string getName();
	Config& getConfig();
	EntityTypeSettings& getSettings();
	BodyDef& getBodyDef();
	Sprite& getSprite();
	std::vector<std::string>& getBehaviorList();
	std::vector<std::shared_ptr<EntityBehavior> >& getBehaviors();
	float getHealth();

private:
	void read();

	Engine* m_engine;
	std::string m_file;
	Config m_config;
	EntityTypeSettings m_settings;
	Sprite m_sprite;
	std::vector<std::shared_ptr<EntityBehavior> > m_behaviors;

};

}

#endif /* ENTITYTYPE_H_ */
