#include "EntityType.h"
#include "../EntityBehavior.h"
#include "../../../Engine/Engine.h"
#include "../../../Util/Util.h"

namespace styx
{

EntityType::EntityType(Engine* engine,
		std::string file) :
				m_engine(engine),
				m_file(file),
				m_settings(engine->getWorld()),
				m_sprite(engine->getFiles())
{
	read();

}

EntityType::Ptr EntityType::create(Engine* engine, std::string file)
{
	return std::make_shared<EntityType>(engine, file);

}

EntityType::~EntityType()
{
}

Engine* EntityType::getEngine()
{
	return m_engine;

}

std::string& EntityType::getFile()
{
	return m_file;

}

std::string EntityType::getName()
{
	return m_settings.name;

}

Config& EntityType::getConfig()
{
	return m_config;

}

EntityTypeSettings& EntityType::getSettings()
{
	return m_settings;

}

BodyDef& EntityType::getBodyDef()
{
	return m_settings.bodyDef;

}

Sprite& EntityType::getSprite()
{
	return m_sprite;

}

void EntityType::read()
{
	m_config.parse(m_file);
	m_settings.read(m_config);

	if(m_settings.mass != -1.0f)
	{
		m_settings.bodyDef.setMass(m_settings.mass);

	}

	for(unsigned int i = 0; i < m_config.getNumOfItems(); i++)
	{
		ConfigItem item = m_config.getItem(i);

		if(item.getType() == "circ@")
		{
			if(item.getNumOfParams() < 1)
				continue;

			float radius = toMeters(item.paramAsFloat(0));
			float x = 0;
			float y = 0;

			if(item.getNumOfParams() > 2)
			{
				x = toMeters(item.paramAsFloat(1));
				y = -toMeters(item.paramAsFloat(2));

			}

			m_settings.bodyDef.addCirc(radius, x, y);

		}
		else if(item.getType() == "rect@")
		{
			if(item.getNumOfParams() < 2)
				continue;

			float width = toMeters(item.paramAsFloat(0));
			float height = toMeters(item.paramAsFloat(1));

			float x = 0;
			float y = 0;
			float a = 0;

			if(item.getNumOfParams() > 3)
			{
				x = toMeters(item.paramAsFloat(2));
				y = -toMeters(item.paramAsFloat(3));


			}

			if(item.getNumOfParams() > 4)
			{
				a = toRadians(item.paramAsFloat(4));

			}

			m_settings.bodyDef.addRect(width, height, x, y, a);

		}
		else if(item.getType() == "poly@")
		{
			if(item.getNumOfParams() < 8)
				continue;

			float x = toMeters(item.paramAsFloat(0));
			float y = -toMeters(item.paramAsFloat(1));

			int numpoints = (item.getNumOfParams()-2)/2;
			std::vector<Vec2> points(numpoints);
			for(int i = 2, x = 0; i < item.getNumOfParams(); i+=2, x++)
			{
				points[x].x = toMeters(item.paramAsFloat(i));
				points[x].y = -toMeters(item.paramAsFloat(i+1));

			}

			m_settings.bodyDef.addPoly(points);

		}
		else if(item.getType() == "sprite@")
		{
			if(item.getNumOfParams() < 3)
				continue;

			std::string text = item.getParam(0);

			float x = item.paramAsFloat(1);
			float y = item.paramAsFloat(2);

			float a = 0;
			if(item.getNumOfParams() > 5)
			{
				a = item.paramAsFloat(5);

			}


			SpriteLayer& layer = m_sprite.addLayer(item.getKey(), text);
			layer.setPosition(x, y);
			layer.setRotation(a);

			if(item.getNumOfParams() > 4)
			{
				float sizex = item.paramAsFloat(3);
				float sizey = item.paramAsFloat(4);
				layer.setSize(sizex, sizey);

			}

			if(item.getNumOfParams() > 7)
			{
				float framex = item.paramAsInt(6);
				float framey = item.paramAsInt(7);
				layer.setFrameSize(Vec2(framex, framey));

			}

			if(item.getNumOfParams() > 9)
			{
				float offsetx = item.paramAsInt(8);
				float offsety = item.paramAsInt(9);
				layer.setFrameOffset(Vec2(offsetx, offsety));

			}

		}
		else if(item.getType() == "anim@")
		{
			if(item.getNumOfParams() < 5)
				break;

			std::string spritename = item.getParam(0);
			float framex = item.paramAsFloat(1);
			float framey = item.paramAsFloat(2);
			float speed = item.paramAsFloat(3);

			std::vector<int> frames;
			for(unsigned int i = 4; i < item.getNumOfParams(); i++)
			{
				frames.push_back(item.paramAsInt(i));

			}

			SpriteLayer* layer = m_sprite.getLayer(spritename);
			if(layer != NULL)
			{
				layer->addAnim(item.getKey(), Vec2(framex, framey), speed, frames);
				layer->setActiveAnim(item.getKey());

			}

		}

	}

	for(unsigned int i = 0; i < m_settings.scripts.size(); i++)
	{
		EntityBehavior::Ptr behavior = m_engine->getBehavior(m_settings.scripts[i]);
		if(behavior != NULL)
		{
			m_behaviors.push_back(behavior);

		}

	}

	m_sprite.setZ(m_settings.z);

}

std::vector<std::string>& EntityType::getBehaviorList()
{
	return m_settings.scripts;

}

std::vector<EntityBehavior::Ptr>& EntityType::getBehaviors()
{
	return m_behaviors;

}

float EntityType::getHealth()
{
	return m_settings.health;

}

}
