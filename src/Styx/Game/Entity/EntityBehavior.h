#ifndef ENTITYBEHAVIOR_H_
#define ENTITYBEHAVIOR_H_

#include <string>
#include <stdint.h>
#include <memory>

namespace styx
{

class Entity;

class EntityBehavior
{
public:
	EntityBehavior();

	typedef std::shared_ptr<EntityBehavior> Ptr;

	virtual ~EntityBehavior();

	virtual std::string getTitle() = 0;

	bool operator==(EntityBehavior& rhs);

	virtual void onInit(Entity* self);
	virtual void onTick(Entity* self);
	virtual void onDraw(Entity* self);
	virtual void onDeath(Entity* self);

	virtual bool shouldCollide(Entity* self, Entity* other);
	virtual void onCollide(Entity* self, Entity* other);
	virtual void onEndCollide(Entity* self, Entity* other);

};

}

#endif /* ENTITYBEHAVIOR_H_ */
