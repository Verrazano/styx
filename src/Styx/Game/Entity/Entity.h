#ifndef ENTITY_H_
#define ENTITY_H_

#include "../../Physics/Physics.h"
#include "../../Graphics/Graphics.h"
#include "../../Util/Util.h"
#include "../../Util/ID/ID.h"
#include "../Controller.h"
#include <memory>
#include "../../Graphics/Particles/ParticleManager.h"

namespace styx
{

class Engine;
class EntityType;
class EntityBehavior;

class GameMode;

class Entity : public ID
{
public:
	Entity(std::shared_ptr<EntityType> type);
	Entity(std::shared_ptr<EntityType> type, Vec2 pos);

	~Entity();

	std::string getName();

	//user is responsible for deleting behaviors
	void addBehavior(std::string title);
	void addBehavior(std::shared_ptr<EntityBehavior> behavior);
	void remBehavior(std::string title);
	std::shared_ptr<EntityBehavior> getBehavior(std::string title);
	bool hasBehavior(std::string title);
	std::vector<std::shared_ptr<EntityBehavior> >& getBehaviors();

	void onInit();
	void onTick();
	void onDraw();
	void onDeath();

	float getHealth();
	void setHealth(float health);
	void kill();
	bool isDead();

	//Shortcuts to body distanceTo
	float distanceTo(Entity* other);
	float distanceTo(Vec2 pos);

	Sprite& getSprite();

	Body& getBody();

	std::shared_ptr<EntityType> getType();
	Engine* getEngine();

	Controller& getController();
	bool areControlsDisabled();
	void setControlsDisabled(bool disableControls);

	GameMode* getGameMode();
	World* getWorld();

	void emit(std::string name, Vec2 vel, float lifetime = 1.0, float scale = 1.0, Vec2 offset = Vec2(0, 0));

	void setInt(std::string name, int val);
	int getInt(std::string name);

	void setUInt(std::string name, unsigned int val);
	unsigned int getUInt(std::string name);

	void setFloat(std::string name, float val);
	float getFloat(std::string name);

	void setString(std::string name, std::string val);
	std::string getString(std::string name);

	void setBool(std::string name, bool val);
	bool getBool(std::string name);

	std::vector<std::pair<std::string, int> >& getInts();
	std::vector<std::pair<std::string, unsigned int> >& getUInts();
	std::vector<std::pair<std::string, float> >& getFloats();
	std::vector<std::pair<std::string, std::string> >& getStrings();
	std::vector<std::pair<std::string, bool> >& getBools();

private:
	bool shouldCollide(Body& other);
	void onCollide(Body& other);
	void onEndCollide(Body& other);

	std::vector<std::shared_ptr<EntityBehavior> > m_behaviors;
	Sprite m_sprite;
	Body m_body;
	std::shared_ptr<EntityType> m_type;
	float m_health;
	Engine* m_engine;
	Controller m_controller;
	bool m_disableControls;
	GameMode* m_gameMode;
	ParticleManager* m_particles;
	World* m_world;

	std::vector<std::pair<std::string, int> > m_ints;
	std::vector<std::pair<std::string, unsigned int> > m_uints;
	std::vector<std::pair<std::string, float> > m_floats;
	std::vector<std::pair<std::string, std::string> > m_strings;
	std::vector<std::pair<std::string, bool> > m_bools;

};

}

#endif /* ENTITY_H_ */
