#include "Entity.h"
#include "../../Engine/Engine.h"
#include "EntityBehavior.h"
#include <iostream>
#include "EntityType/EntityType.h"
#include "../../Util/Util.h"
#include "../GameMode/GameMode.h"

namespace styx
{

Entity::Entity(EntityType::Ptr type) :
		m_sprite(type->getSprite()),
		m_body(type->getBodyDef()),
		m_type(type)
{
	m_disableControls = false;
	Engine* engine = m_type->getEngine();
	m_engine = engine;
	m_health = m_type->getHealth();
	m_body.setData(this);

	m_behaviors = m_type->getBehaviors();

	//TODO: change these to references
	m_gameMode = m_engine->getGameMode();
	m_particles = &m_engine->getParticles();
	m_world = &m_engine->getWorld();

	m_sprite.setZOrderCallback(
			[engine]()
			{
				engine->updateZ();

			});

	m_body.setShouldCollide(
			[this](Body& other)
			{
				return this->shouldCollide(other);

			});

	m_body.setOnCollide(
			[this](Body& other)
			{
				this->onCollide(other);

			});

	m_body.setOnEndCollide(
			[this](Body& other)
			{
				this->onEndCollide(other);

			});

	engine->updateZ();

}

Entity::Entity(EntityType::Ptr type, Vec2 pos) :
		m_sprite(type->getSprite()),
		m_body(type->getBodyDef()),
		m_type(type)
{
	m_disableControls = false;
	Engine* engine = m_type->getEngine();
	m_engine = engine;
	m_health = m_type->getHealth();
	m_body.setData(this);

	m_behaviors = m_type->getBehaviors();

	//TODO: change these to references
	m_gameMode = m_engine->getGameMode();
	m_particles = &m_engine->getParticles();
	m_world = &m_engine->getWorld();

	m_sprite.setZOrderCallback(
			[engine]()
			{
				engine->updateZ();

			});

	m_body.setPosition(pos);

	m_body.setShouldCollide(
			[this](Body& other)
			{
				return this->shouldCollide(other);

			});

	m_body.setOnCollide(
			[this](Body& other)
			{
				this->onCollide(other);

			});

	m_body.setOnEndCollide(
			[this](Body& other)
			{
				this->onEndCollide(other);

			});

	engine->updateZ();

}

Entity::~Entity()
{
}

std::string Entity::getName()
{
	return m_type->getName();

}

void Entity::addBehavior(std::string title)
{
	m_type->getEngine()->addBehaviorTo(this, title);

}

void Entity::addBehavior(EntityBehavior::Ptr behavior)
{
	if(behavior == NULL)
	{
		return;

	}

	m_behaviors.push_back(behavior);

}

void Entity::remBehavior(std::string title)
{
	std::vector<EntityBehavior::Ptr>::iterator it;
	for(it = m_behaviors.begin(); it != m_behaviors.end(); ++it)
	{
		EntityBehavior::Ptr behavior = *it;
		if(behavior->getTitle() == title)
		{
			m_behaviors.erase(it);
			return;

		}

	}

}

EntityBehavior::Ptr Entity::getBehavior(std::string title)
{
	for(auto behavior : m_behaviors)
	{
		if(behavior->getTitle() == title)
		{
			return behavior;

		}

	}

	return NULL;

}

bool Entity::hasBehavior(std::string title)
{
	return getBehavior(title) != NULL;

}

std::vector<EntityBehavior::Ptr>& Entity::getBehaviors()
{
	return m_behaviors;

}

void Entity::onInit()
{
	for(auto behavior : m_behaviors)
	{
		behavior->onInit(this);

	}

}

sf::Vector2f toScreenInt(Vec2 pos){ return sf::Vector2f((int)toPixels(pos.x), -(int)toPixels(pos.y)); }

void Entity::onTick()
{
	m_sprite.setPositionMeters(m_body.getPosition());
	m_sprite.setRotation(m_body.getAngle());

	m_body.updateSpeed();

	if(!m_disableControls)
	{
		m_controller.update();

	}

	for(auto behavior : m_behaviors)
	{
		behavior->onTick(this);

	}

}

void Entity::onDraw()
{
	for(auto behavior : m_behaviors)
	{
		behavior->onDraw(this);

	}

}

void Entity::onDeath()
{
	for(auto behavior : m_behaviors)
	{
		behavior->onDeath(this);

	}

}

bool Entity::shouldCollide(Body& body)
{
	Entity* other = body.getData<Entity>();
	for(auto behavior : m_behaviors)
	{
		bool result = behavior->shouldCollide(this, other);
		if(!result)
		{
			return false;

		}

	}

	return true;

}

void Entity::onCollide(Body& body)
{
	Entity* other = body.getData<Entity>();
	for(auto behavior : m_behaviors)
	{
		behavior->onCollide(this, other);

	}

}

void Entity::onEndCollide(Body& body)
{
	Entity* other = body.getData<Entity>();
	for(auto behavior : m_behaviors)
	{
		behavior->onEndCollide(this, other);

	}

}

float Entity::getHealth()
{
	return m_health;

}

void Entity::setHealth(float health)
{
	m_health = health;

}

void Entity::kill()
{
	m_health = -1;

}

bool Entity::isDead()
{
	return m_health <= 0;

}

float Entity::distanceTo(Entity* other)
{
	return m_body.getPosition().dist(other->m_body.getPosition());
}

float Entity::distanceTo(Vec2 pos)
{
	return m_body.getPosition().dist(pos);

}

Sprite& Entity::getSprite()
{
	return m_sprite;

}

Body& Entity::getBody()
{
	return m_body;

}

EntityType::Ptr Entity::getType()
{
	return m_type;

}

Engine* Entity::getEngine()
{
	return m_engine;

}

Controller& Entity::getController()
{
	return m_controller;

}

bool Entity::areControlsDisabled()
{
	return m_disableControls;

}

void Entity::setControlsDisabled(bool disableControls)
{
	m_disableControls = disableControls;
	m_controller.flush();

}

GameMode* Entity::getGameMode()
{
	return m_gameMode;

}

//TODO: fix this from messing up...
/*
ParticleManager* Entity::getParticleManager();
{
	return m_particles;

}*/

World* Entity::getWorld()
{
	return m_world;

}

void Entity::emit(std::string name, Vec2 vel, float lifetime, float scale, Vec2 offset)
{
	m_engine->emit(name, m_body.getPosition() + offset, vel, lifetime, scale);

}

void Entity::setInt(std::string name, int val)
{
	for(auto& v : m_ints)
	{
		if(v.first == name)
		{
			v.second = val;
			return;

		}

	}

	m_ints.push_back(std::pair<std::string, int>(name, val));

}

int Entity::getInt(std::string name)
{
	for(auto& v : m_ints)
	{
		if(v.first == name)
		{
			return v.second;

		}

	}

	return 0;

}

void Entity::setUInt(std::string name, unsigned int val)
{
	for(auto& v : m_uints)
	{
		if(v.first == name)
		{
			v.second = val;
			return;

		}

	}

	m_uints.push_back(std::pair<std::string, unsigned int>(name, val));

}

unsigned int Entity::getUInt(std::string name)
{
	for(auto& v : m_uints)
	{
		if(v.first == name)
		{
			return v.second;

		}

	}

	return 0;

}

void Entity::setFloat(std::string name, float val)
{
	for(auto& v : m_floats)
	{
		if(v.first == name)
		{
			v.second = val;
			return;

		}

	}

	m_floats.push_back(std::pair<std::string, float>(name, val));

}

float Entity::getFloat(std::string name)
{
	for(auto& v : m_floats)
	{
		if(v.first == name)
		{
			return v.second;

		}

	}

	return 0.0f;

}

void Entity::setString(std::string name, std::string val)
{
	for(auto& v : m_strings)
	{
		if(v.first == name)
		{
			v.second = val;
			return;

		}

	}

	m_strings.push_back(std::pair<std::string, std::string>(name, val));

}

std::string Entity::getString(std::string name)
{
	for(auto& v : m_strings)
	{
		if(v.first == name)
		{
			return v.second;

		}

	}

	return "";

}

void Entity::setBool(std::string name, bool val)
{
	for(auto& v : m_bools)
	{
		if(v.first == name)
		{
			v.second = val;
			return;

		}

	}

	m_bools.push_back(std::pair<std::string, bool>(name, val));

}

bool Entity::getBool(std::string name)
{
	for(auto& v : m_bools)
	{
		if(v.first == name)
		{
			return v.second;

		}

	}

	return false;

}

std::vector<std::pair<std::string, int> >& Entity::getInts()
{
	return m_ints;

}

std::vector<std::pair<std::string, unsigned int> >& Entity::getUInts()
{
	return m_uints;

}

std::vector<std::pair<std::string, float> >& Entity::getFloats()
{
	return m_floats;

}

std::vector<std::pair<std::string, std::string> >& Entity::getStrings()
{
	return m_strings;

}

std::vector<std::pair<std::string, bool> >& Entity::getBools()
{
	return m_bools;

}

}
