#include "EntityBehavior.h"

namespace styx
{

EntityBehavior::EntityBehavior()
{
}

EntityBehavior::~EntityBehavior()
{
}

bool EntityBehavior::operator==(EntityBehavior& rhs)
{
	return this->getTitle() == rhs.getTitle();

}

void EntityBehavior::onInit(Entity* self){}
void EntityBehavior::onTick(Entity* self){}
void EntityBehavior::onDraw(Entity* self){}
void EntityBehavior::onDeath(Entity* self){}
bool EntityBehavior::shouldCollide(Entity* self, Entity* other){return true;}
void EntityBehavior::onCollide(Entity* self, Entity* other){}
void EntityBehavior::onEndCollide(Entity* self, Entity* other){}

}
