#ifndef GAMEMODESETTINGS_H_
#define GAMEMODESETTINGS_H_

#include "../../Util/Settings/Settings.h"

namespace styx
{

class GameModeSettings : public Settings
{
public:
	GameModeSettings();

	std::string title;
	std::vector<std::string> scripts;

};

}

#endif /* GAMEMODESETTINGS_H_ */
