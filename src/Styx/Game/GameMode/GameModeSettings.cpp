#include "GameModeSettings.h"

namespace styx
{

GameModeSettings::GameModeSettings()
{
	title = "gamemode";

	add("title", offsetof(GameModeSettings, title), title);
	add("scripts", offsetof(GameModeSettings, scripts), scripts);

}

}
