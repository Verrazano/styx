#include "GameMode.h"
#include "GameModeBehavior.h"
#include "../Entity/Entity.h"
#include "../../Engine/Engine.h"

namespace styx
{

GameMode::GameMode(Engine* engine, std::string file) :
		m_engine(engine),
		m_file(file)
{
	read();
	onInit();

}

std::string GameMode::getTitle()
{
	return m_settings.title;

}

void GameMode::addBehavior(std::string title)
{
	GameModeBehavior::Ptr behavior = m_engine->getGameModeBehavior(title);
	addBehavior(behavior);

}

void GameMode::addBehavior(GameModeBehavior::Ptr behavior)
{
	if(behavior == NULL)
	{
		return;

	}

	m_behaviors.push_back(behavior);

}

void GameMode::remBehavior(std::string title)
{
	std::vector<GameModeBehavior::Ptr>::iterator it;
	for(it = m_behaviors.begin(); it != m_behaviors.end(); ++it)
	{
		GameModeBehavior::Ptr behavior = *it;
		if(behavior->getTitle() == title)
		{
			m_behaviors.erase(it);
			return;

		}

	}

}

GameModeBehavior::Ptr GameMode::getBehavior(std::string title)
{
	for(auto behavior : m_behaviors)
	{
		if(behavior->getTitle() == title)
		{
			return behavior;


		}

	}

	return NULL;

}

bool GameMode::hasBehavior(std::string title)
{
	return getBehavior(title) != NULL;

}

std::vector<GameModeBehavior::Ptr>& GameMode::getBehaviors()
{
	return m_behaviors;

}

void GameMode::onInit()
{
	for(auto behavior : m_behaviors)
	{
		behavior->onInit(this);

	}

}

void GameMode::onStart()
{
	for(auto behavior : m_behaviors)
	{
		behavior->onStart(this);

	}

}

bool GameMode::onPreGame()
{
	for(auto behavior : m_behaviors)
	{
		bool ret = behavior->onPreGame(this);
		if(ret)
		{
			return true;

		}

	}

	return false;

}

void GameMode::onTick()
{
	for(auto behavior : m_behaviors)
	{
		behavior->onTick(this);

	}

}

bool GameMode::isGameOver()
{
	for(auto behavior : m_behaviors)
	{
		bool ret = behavior->isGameOver(this);
		if(ret)
		{
			return true;

		}

	}

	return false;

}

void GameMode::onDraw()
{
	for(auto behavior : m_behaviors)
	{
		behavior->onDraw(this);

	}

}

void GameMode::onEntityCreated(Entity* entity)
{
	for(auto behavior : m_behaviors)
	{
		behavior->onEntityCreated(this, entity);

	}

}

void GameMode::onEntityDestroyed(Entity* entity)
{
	for(auto behavior : m_behaviors)
	{
		behavior->onEntityDestroyed(this, entity);

	}

}

Engine* GameMode::getEngine()
{
	return m_engine;

}

std::string& GameMode::getFile()
{
	return m_file;

}

std::vector<std::string>& GameMode::getBehaviorList()
{
	return m_settings.scripts;

}

Config& GameMode::getConifg()
{
	return m_config;

}

GameModeSettings& GameMode::getSettings()
{
	return m_settings;

}

void GameMode::read()
{
	m_config.parse(m_file);
	m_settings.read(m_config);

	for(unsigned int i = 0; i < m_settings.scripts.size(); i++)
	{
		addBehavior(m_settings.scripts[i]);

	}

}

}
