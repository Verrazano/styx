#ifndef GAMEMODE_H_
#define GAMEMODE_H_

#include <memory>
#include <string>
#include <vector>
#include "GameModeSettings.h"
#include "../../Util/VarManager/VarManager.h"

namespace styx
{

class Engine;
class Entity;
class GameModeBehavior;

class GameMode : public VarManager
{
public:
	GameMode(Engine* engine, std::string file);

	std::string getTitle();

	void addBehavior(std::string title);
	void addBehavior(std::shared_ptr<GameModeBehavior> behavior);
	void remBehavior(std::string title);
	std::shared_ptr<GameModeBehavior> getBehavior(std::string title);
	bool hasBehavior(std::string title);
	std::vector<std::shared_ptr<GameModeBehavior> >& getBehaviors();

	void onInit();
	void onStart();
	bool onPreGame();
	void onTick();
	bool isGameOver();
	void onDraw();
	void onEntityCreated(Entity* entity);
	void onEntityDestroyed(Entity* entity);

	Engine* getEngine();
	std::string& getFile();
	std::vector<std::string>& getBehaviorList();
	Config& getConifg();
	GameModeSettings& getSettings();

private:
	void read();

	std::vector<std::shared_ptr<GameModeBehavior> > m_behaviors;
	std::string m_file;
	Config m_config;
	GameModeSettings m_settings;
	Engine* m_engine;

};

}

#endif /* GAMEMODE_H_ */
