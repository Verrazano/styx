#include "GameModeBehavior.h"

namespace styx
{

GameModeBehavior::GameModeBehavior()
{
}

GameModeBehavior::~GameModeBehavior()
{
}

void GameModeBehavior::onInit(GameMode* self)
{
}

void GameModeBehavior::onStart(GameMode* self)
{
}

bool GameModeBehavior::onPreGame(GameMode* self)
{
	return true;

}

void GameModeBehavior::onTick(GameMode* self)
{
}

bool GameModeBehavior::isGameOver(GameMode* self)
{
	return false;

}

void GameModeBehavior::onDraw(GameMode* self)
{
}

void GameModeBehavior::onEntityCreated(GameMode* self, Entity* entity)
{
}

void GameModeBehavior::onEntityDestroyed(GameMode* self, Entity* entity)
{
}

}


