#ifndef GAMEMODEBEHAVIOR_H_
#define GAMEMODEBEHAVIOR_H_

#include <string>
#include <memory>

namespace styx
{

class Entity;
class GameMode;

class GameModeBehavior
{
public:
	GameModeBehavior();

	typedef std::shared_ptr<GameModeBehavior> Ptr;

	virtual ~GameModeBehavior();

	virtual std::string getTitle() = 0;

	bool operator==(GameModeBehavior& rhs);

	virtual void onInit(GameMode* self);
	virtual void onStart(GameMode* self);
	virtual bool onPreGame(GameMode* self);
	virtual void onTick(GameMode* self);
	virtual bool isGameOver(GameMode* self);
	virtual void onDraw(GameMode* self);
	virtual void onEntityCreated(GameMode* self, Entity* entity);
	virtual void onEntityDestroyed(GameMode* self, Entity* entity);

};

}

#endif /* GAMEMODEBEHAVIOR_H_ */
