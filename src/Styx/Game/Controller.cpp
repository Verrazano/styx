#include "Controller.h"
#include <SFML/Graphics.hpp>

namespace styx
{


Controller::Controller(bool useKeyboard) :
		m_useKeyboard(useKeyboard)
{
	flush();

}

void Controller::update()
{
	for(unsigned int i = 0; i < 101; i++)
	{
		m_oldPresses[i] = m_newPresses[i];
		if(m_useKeyboard)
		{
			m_newPresses[i] = sf::Keyboard::isKeyPressed((sf::Keyboard::Key)i);

		}
		else
		{
			m_newPresses[i] = false;

		}

	}

}

void Controller::press(int index)
{
	m_newPresses[index] = true;

}

bool Controller::isPressing(int index)
{
	return m_newPresses[index];

}

bool Controller::justPressed(int index)
{
	return m_newPresses[index] && !m_oldPresses[index];

}

bool Controller::justUnpressed(int index)
{
	return !m_newPresses[index] && m_oldPresses[index];

}

bool Controller::isUsingKeyboard()
{
	return m_useKeyboard;

}

void Controller::setUsingKeyboard(bool useKeyboard)
{
	m_useKeyboard = useKeyboard;

}

void Controller::setNamedKey(std::string name, int index)
{

	for(unsigned int i = 0; i < m_named.size(); i++)
	{
		if(m_named[i].first == name)
		{
			m_named[i].second = index; //re-assign the key
			return;


		}

	}

	m_named.push_back(std::pair<std::string, int>(name, index));

}

bool Controller::hasNamedKey(std::string name)
{
	for(unsigned int i = 0; i < m_named.size(); i++)
	{
		if(m_named[i].first == name)
		{
			return true;

		}

	}

	return false;

}

bool Controller::isPressing(std::string name)
{
	for(unsigned int i = 0; i < m_named.size(); i++)
	{
		if(m_named[i].first == name)
		{
			return isPressing(m_named[i].second);

		}

	}

	return false;

}

bool Controller::justPressed(std::string name)
{
	for(unsigned int i = 0; i < m_named.size(); i++)
	{
		if(m_named[i].first == name)
		{
			return justPressed(m_named[i].second);

		}

	}

	return false;

}

bool Controller::justUnpressed(std::string name)
{
	for(unsigned int i = 0; i < m_named.size(); i++)
	{
		if(m_named[i].first == name)
		{
			return justUnpressed(m_named[i].second);

		}

	}

	return false;

}

void Controller::flush()
{
	for(unsigned int i = 0; i < 101; i++)
	{
		m_newPresses[i] = false;
		m_oldPresses[i] = false;

	}

}

}
