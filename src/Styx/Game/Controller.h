#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include <string>
#include <vector>

namespace styx
{

class Controller
{
public:
	Controller(bool useKeyboard = false);

	void update();

	void press(int index);

	bool isPressing(int index);
	bool justPressed(int index);
	bool justUnpressed(int index);

	bool isUsingKeyboard();
	void setUsingKeyboard(bool useKeyboard);

	void setNamedKey(std::string name, int index);
	bool hasNamedKey(std::string name);

	bool isPressing(std::string name);
	bool justPressed(std::string name);
	bool justUnpressed(std::string name);

	void flush();

private:
	bool m_useKeyboard;
	bool m_newPresses[101];
	bool m_oldPresses[101];
	std::vector<std::pair<std::string, int> > m_named;

};

}

#endif /* CONTROLLER_H_ */
