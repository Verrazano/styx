#include <SFML/Audio.hpp>
#include "Styx/Styx.h"
#include <iostream>
#include <math.h>
#include "PlayerLogic.h"

class SuperNinja : public styx::GameModeBehavior
{
public:
	SuperNinja()
	{
	}

	sf::Sound bitninja;

	std::string getTitle(){return "SuperNinja";}

	unsigned int lives;

	void onStart(styx::GameMode* self)
	{
		lives = 3;
		styx::Engine* engine = self->getEngine();
		engine->addEntity("background", sf::Vector2f(0, 0));
		engine->addEntity("player", sf::Vector2f(0, 0));
		engine->addEntity("platform", sf::Vector2f(0, -15.625));
		engine->addEntity("platform", sf::Vector2f(4, -23.43));
		engine->addEntity("platform", sf::Vector2f(-4, -23.43));
		engine->addEntity("platform", sf::Vector2f(4*2, -20.3125));
		engine->addEntity("platform", sf::Vector2f(-4*2, -20.3125));
		engine->addEntity("platform", sf::Vector2f(4*3, -20.3125));
		engine->addEntity("platform", sf::Vector2f(-4*3, -20.3125));
		engine->addEntity("platform", sf::Vector2f(4*5, -15.625));
		engine->addEntity("platform", sf::Vector2f(-4*5, -15.625));
		engine->addEntity("platform", sf::Vector2f(4*6, -15.625));
		engine->addEntity("platform", sf::Vector2f(-4*6, -15.625));
		engine->addEntity("platform", sf::Vector2f(4*2.5, -10.937));
		engine->addEntity("platform", sf::Vector2f(-4*2.5, -10.937));
		engine->addEntity("platform", sf::Vector2f(0, 200));
		engine->addEntity("boundry", sf::Vector2f(100000/2/32, -1000/32));

		std::vector<styx::Entity*> platforms;
				self->getEngine()->getEntitiesWithName("platform", platforms);
				std::cout << "platforms: " << platforms.size() << "\n";


		unsigned int crates = 10;
		for(unsigned int i = 0; i < crates; i++)
		{
			spawnCrate(self);

		}

		/*bitninja.setBuffer(*engine->getFiles().get<sf::SoundBuffer>("8bitninja.wav"));
		bitninja.setVolume(50.0);
		bitninja.play();*/

	}

	void spawnRandomCrate(styx::GameMode* self)
	{
		//self->getEngine()->addEntity("crate", sf::Vector2f(400 + (rand()%600)-300, 300));

	}

	void spawnCrate(styx::GameMode* self)
	{
		std::vector<styx::Entity*> platforms;
		self->getEngine()->getEntitiesWithName("platform", platforms);
		int spawnat = rand()%platforms.size();
		styx::Vec2 pos = platforms[rand()%platforms.size()]->getBody().getPosition();
		pos.y += 2.0;
		self->getEngine()->addEntity("crate", pos);

	}

	void onDraw(styx::GameMode* self)
	{
		styx::Engine* engine = self->getEngine();
		for(unsigned int i = 0; i < lives; i++)
		{
			engine->draw(sf::Vector2f(20 + (64*i), 20), sf::Vector2f(64, 64), "ninjahead.png");

		}

		int mushrooms = self->getInt("mushrooms");
		engine->draw(sf::Vector2f(20, 100), sf::Vector2f(50, 41), "orangemushroom.png");
		engine->draw(sf::Vector2f(75, 100), " x " + styx::toString(mushrooms), sf::Color::Black);

	}

	void onEntityDestroyed(styx::GameMode* self, styx::Entity* entity)
	{
		if(entity->getName() == "player")
		{
			if(lives > 0)
			{
				self->getEngine()->addEntity("player", sf::Vector2f(0, 0));
				lives--;

			}

		}
		else if(entity->getName() == "crate")
		{
			spawnCrate(self);

		}

	}

};

int main(int argc, char** argv)
{
	styx::EngineInfo info;
	info.hq2xOn = true;
	styx::Engine engine(info);
	engine.addBehavior<PlayerLogic>();
	engine.addGameModeBehavior<SuperNinja>();

	engine.addEntityType(playerCFG);

	engine.setGameMode(
			"string title = \"SuperNinja\";"
			"string@ scripts = \"SuperNinja\";");

	while(engine.isRunning())
	{
		engine.tick();

	}

	return 0;

}
