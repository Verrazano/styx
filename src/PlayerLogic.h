#include "Styx/Styx.h"

static std::string playerCFG =
		"string name = \"player\";"
		"string@ scripts = \"PlayerLogic\";"
		"bool isDynamic = true;"
		"bool isBullet = true;"
		"bool fixedRotation = true;"
		"float restitution = 0.2;"
		"rect@ feet = 50, 100, 0, 4;"
		"float gravityScale = 2;"
		"float friction = 5;"
		"float z = 2;"
		"sprite@ idle = \"characteridle.png\", 0, 0, 58, 109, 0, 58, 109;"
		"anim@ idleanim = \"idle\", 58, 109, 0.1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9;"
		"sprite@ run = \"characterrun.png\", 0, 2, 90, 114, 0, 90, 114;"
		"sprite@ jump = \"characterjump.png\", 0, 0, 90, 120, 0, 90, 120;"
		"anim@ jumpanim = \"jump\", 90, 120, 0.06, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 6, 7, 8, 9, 8, 7, 6, 5, 6, 7, 8, 9;"
		"sprite@ slide = \"characterslide.png\", 0, 18, 93, 87, 90, 93, 87;"
		"sprite@ attack = \"attack.png\", 0, 13, 126, 130, 0, 126, 130;"
		"anim@ attackanim = \"attack\", 126, 130, 0.06, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9;";

class PlayerLogic : public styx::EntityBehavior
{
public:
	PlayerLogic()
	{
		countup = true;
		pressLeft = false;
		pressRight = false;
		primed = false;
		canSlide = true;
		slide = 0;
		leftClick = false;
		rightClick = false;

	}

	sf::Clock animTimer;
	sf::Clock doubleTap;
	bool pressLeft;
	bool primed;
	bool pressRight;
	float slide;
	bool canSlide;
	bool leftClick;
	bool rightClick;

	std::string getTitle(){return "PlayerLogic";}

	std::string anim = "idle";
	bool countup;

	void onInit(styx::Entity* self)
	{
		static bool loaded = false;
		if(!loaded)
		{
			styx::Engine* engine = self->getEngine();
			loaded = true;
			engine->addParticleType("dust", "smoke.png", styx::Vec2(62, 58));

		}

		countup = true;
		self->getSprite().getLayer("run")->setVisible(false);
		self->getSprite().getLayer("jump")->setVisible(false);
		self->getSprite().getLayer("slide")->setVisible(false);
		self->getSprite().getLayer("attack")->setVisible(false);
		self->setInt("frame", 0);

		styx::Camera& camera = self->getEngine()->getCamera();
		camera.setTarget(self);
		camera.setLockToTarget(true);
		anim = "idle";

	}

	void onTick(styx::Entity* self)
	{
		styx::Body& body = self->getBody();
		int frame = self->getInt("frame");

		if(body.getFriction() == 0.0f)
		{
			sf::Vector2f pos = body.getPosition().to<sf::Vector2f>();
			pos.y -= 1.687;
			styx::Engine* engine = self->getEngine();
			if(engine->isOtherEntityAt(pos, self))
			{
				body.setFriction(5);
				body.setVelocity(0, 0);


			}

		}

		if((anim == "run" || anim == "jump") && animTimer.getElapsedTime().asSeconds() > 0.25)
		{
			self->getSprite().getLayer("run")->setVisible(false);
			self->getSprite().getLayer("idle")->setVisible(true);
			self->getSprite().getLayer("jump")->setVisible(false);
			anim = "idle";
			animTimer.restart();

		}


		styx::SpriteLayer* sprite = self->getSprite().getLayer(anim);

		if(anim == "attack")
		{
			int ff = sprite->getActiveAnim().getFrame();
			if(ff == 5)
			{
				float dir = 1.562;
				if(sprite->isFlippedHorizontally())
					dir = -1.562;
				std::vector<styx::Entity*> damaged =
						self->getEngine()->getEntitiesInBox(body.getPosition().to<sf::Vector2f>(), sf::Vector2f(dir, -2.5));
				for(auto entity : damaged)
				{
					if(entity == self)
						continue;

					if(entity->getName() != "platform")
					{
						entity->setHealth(entity->getHealth()-20);

					}

				}

			}

			if(ff == 9)
			{
				anim = "idle";
				self->getSprite().getLayer("idle")->setVisible(true);
				sprite->setVisible(false);

			}

			return;

		}

		//sprite->setRotation(a*10);
		/*if(anim == "idle" && animTimer.getElapsedTime().asSeconds() > 0.1)
		{
			frame++;
			frame = frame%10;
			sprite->setFrame(frame);
			self->setInt("frame", frame);
			animTimer.restart();
		}*/

		if(anim == "jump")
		{
			if(!(body.getVelocity().y > 0.5))
			{
				sf::Vector2f pos = body.getPosition().to<sf::Vector2f>();
				pos.y -= 1.687;
				styx::Engine* engine = self->getEngine();
				if(engine->isOtherEntityAt(pos, self))
				{
					anim = "run";
					self->getSprite().getLayer("run")->setVisible(true);
					self->getSprite().getLayer("idle")->setVisible(false);
					self->getSprite().getLayer("jump")->setVisible(false);

				}

			}

			animTimer.restart();

		}

		if(canSlide && sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			pressRight = false;
			bool dothing = true;
			if(canSlide && !pressLeft)
			{
				if(primed)
				{
					if(body.getFriction() != 0 && doubleTap.getElapsedTime().asSeconds() <= 0.2)
					{
						self->setInt("frame", 0);
						slide = -21.875;
						body.setVelocity(sf::Vector2f(slide, body.getVelocity().y));
						body.applyForce(sf::Vector2f(0, -18.75));
						primed = false;
						dothing = false;
						canSlide = false;
						body.setFriction(0.1);
						doubleTap.restart();
						self->getSprite().getLayer("slide")->flipHorizontal(true);
						self->getSprite().getLayer("run")->setVisible(false);
						self->getSprite().getLayer("idle")->setVisible(false);
						self->getSprite().getLayer("jump")->setVisible(false);
						self->getSprite().getLayer("slide")->setVisible(true);
						body.setAngle(90);
						anim = "slide";
						return;

					}
					else
					{
						primed = false;
						pressLeft = true;
						doubleTap.restart();

					}

				}
				else
				{

					pressLeft = true;
					primed = false;
					doubleTap.restart();

				}

			}

			if(dothing)
			{
				sprite->flipHorizontal(true);
				body.setVelocity(sf::Vector2f((-15), body.getVelocity().y));
				if(body.getFriction() != 0.0f)
					body.setFriction(0.2f);

				if(anim == "idle")
				{
					self->getSprite().getLayer("run")->setVisible(true);
					self->getSprite().getLayer("idle")->setVisible(false);
					self->getSprite().getLayer("jump")->setVisible(false);
					anim = "run";

				}

				if(anim == "run" && animTimer.getElapsedTime().asSeconds() > 0.1)
				{
					frame++;
					frame = frame%10;
					sprite->setFrame(frame);
					self->setInt("frame", frame);
					animTimer.restart();

				}

			}

		}
		else if(canSlide && sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			pressLeft = false;
			bool dothing = true;
			if(canSlide && !pressRight)
			{
				if(primed)
				{
					if(body.getFriction() != 0 && doubleTap.getElapsedTime().asSeconds() <= 0.2)
					{
						self->setInt("frame", 0);
						slide = 21.875;
						body.setVelocity(sf::Vector2f(slide, body.getVelocity().y));
						body.applyForce(sf::Vector2f(0, -18.75));
						primed = false;
						dothing = false;
						canSlide = false;
						body.setFriction(0.1);
						doubleTap.restart();
						self->getSprite().getLayer("slide")->flipHorizontal(false);
						self->getSprite().getLayer("run")->setVisible(false);
						self->getSprite().getLayer("idle")->setVisible(false);
						self->getSprite().getLayer("jump")->setVisible(false);
						self->getSprite().getLayer("slide")->setVisible(true);
						body.setAngle(90);
						anim = "slide";
						return;

					}
					else
					{
						primed = false;
						pressRight = true;
						doubleTap.restart();

					}

				}
				else
				{
					pressRight = true;
					primed = false;
					doubleTap.restart();

				}

			}

			if(dothing)
			{
				sprite->flipHorizontal(false);
				body.setVelocity(sf::Vector2f((10), body.getVelocity().y));
				if(body.getFriction() != 0.0f)
					body.setFriction(0.2f);

				if(anim == "idle")
				{
					self->getSprite().getLayer("run")->setVisible(true);
					self->getSprite().getLayer("idle")->setVisible(false);
					self->getSprite().getLayer("jump")->setVisible(false);
					anim = "run";

				}

				if(anim == "run" && animTimer.getElapsedTime().asSeconds() > 0.1)
				{
					frame++;
					frame = frame%10;
					sprite->setFrame(frame);
					self->setInt("frame", frame);
					animTimer.restart();

				}

			}

		}
		else
		{
			if(body.getFriction() == 0.2f)
			{
				body.setFriction(5.0f);
				body.setVelocity(0, 0);

			}

			if(pressLeft || pressRight)
			{
				primed = true;
				pressLeft = false;
				pressRight = false;

			}


		}

		if(!canSlide)
		{
			if(doubleTap.getElapsedTime().asSeconds() >= 0.5)
			{
				body.setFriction(5.0f);
				body.setVelocity(0, 0);
				anim = "idle";
				self->getSprite().getLayer("idle")->setVisible(true);
				self->getSprite().getLayer("slide")->setVisible(false);
				body.setAngle(0);
				canSlide = true;
				primed = false;
				doubleTap.restart();

			}
			else
			{

				body.setVelocity(sf::Vector2f(slide, body.getVelocity().y));
				frame++;
				frame = frame%10;
				sprite->setFrame(frame);
				self->setInt("frame", frame);

				/*int d = (rand()%2) + 1;
				for(int i = 0; i < d; i++)
					self->getEngine()->getParticles().get("dust")->emit(self->getPosition(), sf::Vector2f(-slide/25, (rand()%50)-100), (rand()%2)+0.5, 0.6);
*/
			}

		}

		if(sf::Keyboard::isKeyPressed(sf::Keyboard::I))
		{
			if(!leftClick)
			{
				leftClick = true;
				if(!canSlide)
				{
					body.setAngle(0);
					canSlide = true;
					primed = false;
					self->getSprite().getLayer("slide")->setVisible(false);

				}

				anim = "attack";
				self->setInt("frame", 0);
				sprite->setVisible(false);
				self->getSprite().getLayer("attack")->setVisible(true);
				self->getSprite().getLayer("attack")->setFrame(0);
				self->getSprite().getLayer("attack")->flipHorizontal(sprite->isFlippedHorizontally());
				self->getSprite().getLayer("attack")->getActiveAnim().setFrame(0);

				sf::Vector2f vel = body.getVelocity().to<sf::Vector2f>();
				if(sprite->isFlippedHorizontally())
				{
					body.setVelocity(sf::Vector2f(vel.x + -6.25, vel.y + 6.25));

				}
				else
				{
					body.setVelocity(sf::Vector2f(vel.x + 6.25, vel.y + 6.25));

				}

				self->getEngine()->play("swoosh.wav");

			}

		}
		else
		{
			leftClick = false;

		}

		if(sf::Mouse::isButtonPressed(sf::Mouse::Right))
		{
			if(!rightClick)
			{
				rightClick = true;

			}

		}
		else
		{
			rightClick = false;

		}

		if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			if(!canSlide)
			{
				body.setAngle(0);
				canSlide = true;
				primed = false;
				self->getSprite().getLayer("slide")->setVisible(false);

			}

			sf::Vector2f pos = body.getPosition().to<sf::Vector2f>();
			styx::Engine* engine = self->getEngine();
			bool nojump = false;
			sf::Vector2f posleft = pos;
			posleft.x -= 1.125;
			sf::Vector2f posleft2 = posleft;
			posleft.y += 0.65;
			sf::Vector2f posleft3 = posleft;
			posleft.y -= 0.65;
			sf::Vector2f posright = pos;
			posright.x += 1.125;
			sf::Vector2f posright2 = posright;
			posright.y += 0.65;
			sf::Vector2f posright3 = posright;
			posright.y -= 0.65;
			if(anim == "jump" && sf::Keyboard::isKeyPressed(sf::Keyboard::D) && (engine->isOtherEntityAt(posleft, self) || engine->isOtherEntityAt(posleft2, self) || engine->isOtherEntityAt(posleft3, self)))
			{
				self->setInt("frame", 0);
				body.applyForce(sf::Vector2f(60, 0));
				body.setVelocity(sf::Vector2f(body.getVelocity().x, 18.75));
				self->getSprite().getLayer("jump")->getActiveAnim().setFrame(0);
				countup = true;
				nojump = true;
				body.setFriction(0.0);
				int d = (rand()%3) + 3;
				for(int i = 0; i < d; i++)
					self->emit("dust", styx::Vec2((rand()%4)-2, (rand()%4)-2), (rand()%2)+0.5, 0.75);

			}
			else if(anim == "jump" && sf::Keyboard::isKeyPressed(sf::Keyboard::A) && (engine->isOtherEntityAt(posright, self) || engine->isOtherEntityAt(posright2, self) || engine->isOtherEntityAt(posright3, self)))
			{
				self->setInt("frame", 0);
				body.applyForce(sf::Vector2f(-60, 0));
				body.setVelocity(sf::Vector2f(body.getVelocity().x, 18.75));
				self->getSprite().getLayer("jump")->getActiveAnim().setFrame(0);
				countup = true;
				nojump = true;
				body.setFriction(0.0);
				int d = (rand()%3) + 3;
				for(int i = 0; i < d; i++)
					self->emit("dust", styx::Vec2((rand()%4)-2, (rand()%4)-2), (rand()%2)+0.5, 0.75);


			}

			if(!nojump && body.getVelocity().y < 1)
			{
				pos.y -= 1.687;
				if(engine->isOtherEntityAt(pos, self))
				{
					self->setInt("frame", 0);
					body.changeVelocity(0, 18.75);
					self->getSprite().getLayer("jump")->setVisible(true);
					self->getSprite().getLayer("idle")->setVisible(false);
					self->getSprite().getLayer("run")->setVisible(false);
					anim = "jump";
					self->getSprite().getLayer("jump")->getActiveAnim().setFrame(0);
					self->getSprite().getLayer("jump")->flipHorizontal(sprite->isFlippedHorizontally());
					countup = true;
					body.setFriction(0);


				}

			}
		}

	}

	void onCollide(styx::Entity* self, styx::Entity* other)
	{
		styx::Body& body = self->getBody();
		if(other->getName() == "mushroom")
		{
			other->setHealth(0);
			int mushrooms = self->getEngine()->getGameMode()->getInt("mushrooms");
			self->getEngine()->getGameMode()->setInt("mushrooms", mushrooms+1);

		}

		if(anim == "attack")
			return;

		sf::Vector2f pos = body.getPosition().to<sf::Vector2f>();
		pos.y -= 1.687;
		styx::Engine* engine = self->getEngine();
		if(engine->isOtherEntityAt(pos, self))
		{

				if(anim == "jump")
				{
					anim = "idle";
					self->getSprite().getLayer("jump")->setVisible(false);
					self->getSprite().getLayer("idle")->setVisible(true);
					self->getSprite().getLayer("run")->setVisible(false);

				}

		}

	}

};
